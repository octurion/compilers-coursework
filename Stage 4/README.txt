Compilers - Project 4
---------------------

Alexandros Tasos (sdi1100085)

Compilation instructions:
1) Unzip & descend into the src directory
2) Run the following command:
   $ find -name "*.java" -print | xargs javac

Alternatively, you can create a new project in Eclipse and add the
source files.

Running instructions:
1) While inside the src directory, run:
   $ java Main /path/to/piglet/files/*.pg

If you are using Eclipse, you will have to set the command line arguments
by going to Run > Run Configurations... > Arguments.
If for some reason or another the Piglet files cannot be found by Eclipse,
just cd into the Piglet folder and run the following, then copy & paste
the output:
$ find `pwd` -name "*.pg" | sort

Each *.spg file generated will be located in the same directory its
corresponding Piglet source file lives in.
