package spiglet;

/**
 * Represents a temporary variable in a Spiglet program
 */
public final class TempVar implements SpigletPrintable {
	private int varID;
	public TempVar(int varID){
		this.varID = varID;
	}
	
	public int getID(){
		return varID;
	}

	@Override
	public String toSpigletExpression() {
		return String.format("TEMP %d", varID);
	}
}
