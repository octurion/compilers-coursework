package spiglet;
/**
 * A class that represents either an integer or a label constant
 */
public final class ConstantValue implements SpigletPrintable {
	private enum ConstantKind {
		INTEGER, LABEL
	}
	
	private ConstantKind kind;
	private int constant;
	private String labelName;
	
	public ConstantValue(int constant){
		this.kind = ConstantKind.INTEGER;
		this.constant = constant;
	}
	
	public ConstantValue(String labelName){
		if (labelName == null || labelName.isEmpty()){
			throw new IllegalArgumentException();
		}
		
		this.kind = ConstantKind.LABEL;
		this.labelName = labelName;
	}

	@Override
	public String toSpigletExpression() {
		if (kind == ConstantKind.INTEGER){
			return Integer.toString(constant);
		}
		
		return labelName;
	}
}
