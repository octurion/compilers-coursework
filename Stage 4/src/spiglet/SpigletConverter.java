package spiglet;

import syntaxtree.Goal;
import syntaxtree.Procedure;
import syntaxtree.StmtList;
import visitor.GJVoidDepthFirst;
import visitor.GJVoidVisitor;

public class SpigletConverter {
	private SpigletConverter(){}

	public static String convertPigletToSpiglet(Goal goal){
		StringBuilder builder = new StringBuilder();
		METHODS_VISITOR.visit(goal, builder);
		
		return builder.toString();
	}
	
	private static final GJVoidVisitor<StringBuilder> METHODS_VISITOR =
			new GJVoidDepthFirst<StringBuilder>() {
		public void visit(Procedure proc, StringBuilder builder){
			String methodName = proc.f0.f0.toString();
			int argnum = Integer.parseInt(proc.f2.f0.toString());
			
			String methodBody = MethodConverter.emitMethodBody(proc.f4);
			builder.append(String.format("%s [%d]%nBEGIN%n%sEND%n%n",
					methodName,
					argnum,
					methodBody));
		}
		
		public void visit(StmtList main, StringBuilder builder){
			String methodBody = MethodConverter.emitMethodBody(main);
			builder.append(String.format("MAIN%n%sEND%n%n",	methodBody));
		}
	};
}
