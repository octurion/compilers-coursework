package spiglet;

import java.util.Deque;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import syntaxtree.Node;
import syntaxtree.Temp;
import visitor.GJVoidDepthFirst;

public final class TempVarAllocator {
	private static final class InclusiveInterval {
		private int min;
		private int max;
		
		public InclusiveInterval(int min, int max){
			if (max < min){
				throw new IllegalArgumentException();
			}
						
			this.min = min;
			this.max = max;
		}
		
		/**
		 * @return The minimum number this interval can contain
		 */
		public int getMin(){
			return min;
		}
		
		/**
		 * @return The maximum number this interval can contain
		 */
		public int getMax(){
			return max;
		}
		
		/**
		 * @return {@code true} if the interval can only contain a single integer.
		 */
		public boolean isSingular(){
			return min == max;
		}
	}
	
	// A list that contains all inclusive intervals of unallocated temporary
	// variable numbers
	private Deque<InclusiveInterval> tempVarPool;
	
	TempVarAllocator(Node methodBody){
		constructTempVarPool(methodBody);
	}

	private void constructTempVarPool(Node methodBody) {
		Set<Integer> usedTemps = new TreeSet<Integer>();
		methodBody.accept(TEMP_VAR_ACCUMULATOR, usedTemps);

		tempVarPool = new LinkedList<InclusiveInterval>();
		int minUnused = 0;
		for(Integer elem : usedTemps){
			if (elem == minUnused){
				minUnused++;
				continue;
			}
			
			tempVarPool.addLast(new InclusiveInterval(minUnused, elem - 1));
			minUnused = elem + 1;
		}
		
		// If during the loop we encounter an Integer.MAX_VALUE, it will be
		// the last element of the iteration (we can't have bigger ints than that)
		// and minUnused will be Integer.MIN_VALUE
		if (minUnused != Integer.MIN_VALUE){
			tempVarPool.addLast(
					new InclusiveInterval(minUnused, Integer.MAX_VALUE));
		}
	}
	
	/**
	 * Allocates a new temporary variable from the pool of unallocated temporary
	 * variables available.
	 */
	public TempVar allocateNewTempVar(){
		if (tempVarPool.isEmpty()){
			throw new NoSuchElementException("Too many temporart variables have been allocated");
		}
		
		InclusiveInterval lowestInterval = tempVarPool.pollFirst();
		if (lowestInterval.isSingular()){
			return new TempVar(lowestInterval.getMin());
		}
		
		TempVar returnValue = new TempVar(lowestInterval.getMin());
		InclusiveInterval newLowestInterval = removeLowestInteger(lowestInterval);
		tempVarPool.addFirst(newLowestInterval);
		
		return returnValue;
	}
	
	/**
	 * Creates a new interval from an existing one that doesn't contain the
	 * lowest integer the original one contains. For example, if this function
	 * were passed the interval {@code [5, 30]}, it would return {@code [6, 30]}.
	 */
	private static InclusiveInterval removeLowestInteger(InclusiveInterval interval){
		assert(!interval.isSingular());
		return new InclusiveInterval(interval.getMin() + 1, interval.getMax());
	}
	
	/**
	 * A visitor that accumulates all the temporary variable IDs used in a
	 * function into a set. Because it accepts a set, the IDs will be unique.
	 */
	private static final GJVoidDepthFirst<Set<Integer>> TEMP_VAR_ACCUMULATOR = 
			new GJVoidDepthFirst<Set<Integer>>() {
		public void visit(Temp tempVar, Set<Integer> usedTemps){
			usedTemps.add(Integer.parseInt(tempVar.f1.f0.toString()));
		}
	};
}
