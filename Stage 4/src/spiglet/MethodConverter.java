package spiglet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import syntaxtree.BinOp;
import syntaxtree.CJumpStmt;
import syntaxtree.Call;
import syntaxtree.ErrorStmt;
import syntaxtree.Exp;
import syntaxtree.HAllocate;
import syntaxtree.HLoadStmt;
import syntaxtree.HStoreStmt;
import syntaxtree.IntegerLiteral;
import syntaxtree.JumpStmt;
import syntaxtree.Label;
import syntaxtree.MoveStmt;
import syntaxtree.NoOpStmt;
import syntaxtree.Node;
import syntaxtree.PrintStmt;
import syntaxtree.Stmt;
import syntaxtree.StmtExp;
import syntaxtree.StmtList;
import syntaxtree.Temp;
import visitor.GJDepthFirst;
import visitor.GJVoidDepthFirst;
import visitor.GJVoidVisitor;

final class MethodConverter {
	enum BinaryOp implements SpigletPrintable{
		LT("LT"), PLUS("PLUS"), MINUS("MINUS"), TIMES("TIMES");

		private String expressionKeyword;
		private BinaryOp(String expressionKeyword){
			this.expressionKeyword = expressionKeyword;
		}
		
		@Override
		public String toSpigletExpression() {
			return expressionKeyword;
		}
	}
	
	private StringBuilder methodBuilder;
	private TempVarAllocator tempVarAllocator;
	
	// Because a constant that is a label and a label before a statement are
	// the same thing to a parser, we need to know whether we should treat
	// labels as constants or not.
	private boolean treatLabelsAsConstants;
	
	private MethodConverter(Node node){
		methodBuilder = new StringBuilder();
		tempVarAllocator = new TempVarAllocator(node);
		treatLabelsAsConstants = true;
	}
	
	private String emitCode(StmtList stmts){
		new PigletVisitor().visit(stmts, this);
		
		return methodBuilder.toString();
	}
	
	private String emitCode(StmtExp stmts){
		PigletVisitor v = new PigletVisitor();
		v.visit(stmts.f1, this);
		
		SpigletPrintable returnExpr = v.visit(stmts.f3, this);
		emitReturnStatement(returnExpr);
		
		return methodBuilder.toString();
	}
	
	/**
	 * Converts a sequence of statements into their equivalent Spiglet code
	 * 
	 * @param stmts The parse tree node that represents the sequence of
	 * statements
	 * @return An equivalent version of the statements sequence in Spiglet as
	 * a string
	 */
	public static String emitMethodBody(StmtExp stmts){
		MethodConverter converter = new MethodConverter(stmts);
		return converter.emitCode(stmts);
	}
	/**
	 * Converts a sequence of statements, along with its return expresiion,
	 * into their equivalent Spiglet code
	 * 
	 * @param stmts The parse tree node that represents the sequence of
	 * statements and the final return statement
	 * @return An equivalent version of the statements sequence in Spiglet as
	 * a string
	 */
	public static String emitMethodBody(StmtList stmts){
		MethodConverter converter = new MethodConverter(stmts);
		return converter.emitCode(stmts);
	}
	
	public TempVar emitTempFromExpression(
			BinaryOp operation,
			SpigletPrintable lhs,
			SpigletPrintable rhs){
		TempVar result = tempVarAllocator.allocateNewTempVar();
		
		appendFmtln("MOVE %s %s %s %s",
				result.toSpigletExpression(),
				operation.toSpigletExpression(),
				lhs.toSpigletExpression(),
				rhs.toSpigletExpression());
		return result;
	}
	
	public TempVar emitTempFromFunctionCall(SpigletPrintable function,
			List<TempVar> args){
		List<String> expressionArgs = new ArrayList<String>();
		for (TempVar elem : args){
			expressionArgs.add(elem.toSpigletExpression());
		}
		
		TempVar result = tempVarAllocator.allocateNewTempVar();
		appendFmtln("MOVE %s CALL %s (%s)",
				result.toSpigletExpression(),
				function.toSpigletExpression(),
				joinWithDelimiter(expressionArgs, " "));
		
		return result;
	}
	
	public void emitMemoryStore(TempVar ptr, int offset, TempVar expression){
		appendFmtln("HSTORE %s %d %s",
				ptr.toSpigletExpression(),
				offset,
				expression.toSpigletExpression());
	}
	
	public void emitMemoryLoad(TempVar result, TempVar ptr, int offset){
		appendFmtln("HLOAD %s %s %d",
				result.toSpigletExpression(),
				ptr.toSpigletExpression(),
				offset);
	}
	
	public void setTreatLabelsAsConstants(){
		treatLabelsAsConstants = true;
	}
	
	public void unsetTreatLabelsAsConstants(){
		treatLabelsAsConstants = false;
	}
	
	public boolean mustTreatLabelsAsConstants(){
		return treatLabelsAsConstants;
	}
	
	public void emitReturnStatement(SpigletPrintable expr){
		appendFmtln("RETURN %s", expr.toSpigletExpression());
	}
	
	public TempVar emitExpressionToTempVar(SpigletPrintable expr){
		if (expr instanceof TempVar){
			return (TempVar) expr;
		}
		
		TempVar result = tempVarAllocator.allocateNewTempVar();
		appendFmtln("MOVE %s %s",
				result.toSpigletExpression(),
				expr.toSpigletExpression());
		
		return result;
	}
	
	public void emitMove(TempVar dest, SpigletPrintable src){
		appendFmtln("MOVE %s %s",
				dest.toSpigletExpression(),
				src.toSpigletExpression());

	}

	public TempVar emitAllocationToTempVar(SpigletPrintable expr){
		TempVar result = tempVarAllocator.allocateNewTempVar();
		appendFmtln("MOVE %s HALLOCATE %s",
				result.toSpigletExpression(),
				expr.toSpigletExpression());
		
		return result;
	}
	
	public void emitNoop(){
		appendFmtln("NOOP");
	}
	
	public void emitError(){
		appendFmtln("ERROR");
	}
	
	public SpigletPrintable emitLabel(String labelName){
		if (treatLabelsAsConstants) {
			return new ConstantValue(labelName);
		}
		
		appendFmtln("%s NOOP", labelName);
		return null;
	}
	
	public void emitJump(String labelName){
		appendFmtln("JUMP %s", labelName);
	}
	
	public void emitCjump(SpigletPrintable expr, String labelName){
		appendFmtln("CJUMP %s %s", expr.toSpigletExpression(), labelName);
	}
	
	public void emitPrint(SpigletPrintable expr){
		appendFmtln("PRINT %s", expr.toSpigletExpression());
	}

	/**
	 * Constructs a formatted string using the format specified in
	 * {@link java.lang.String#format}
	 */
	private void appendFmtln(String fmt, Object... args){
		methodBuilder.append(String.format(fmt, args))
			.append(System.getProperty("line.separator"));
	}
	
	/**
	 * Joins together a list of strings which are separated by a delimiter
	 * string (e.g. a comma followed by a space, a single space etc).
	 */
	private static String joinWithDelimiter(List<String> list, String delimeter){
		StringBuilder builder = new StringBuilder();
		Iterator<String> iter = list.iterator();
		
		if(iter.hasNext()){
			builder.append(iter.next());
		}
		while(iter.hasNext()){
			builder.append(delimeter).append(iter.next());
		}
		
		return builder.toString();
	}
	
	private static class PigletVisitor extends GJDepthFirst<SpigletPrintable, MethodConverter> {
		public PigletVisitor(){}
		
		@Override
		public SpigletPrintable visit(StmtList list, MethodConverter converter){
			converter.unsetTreatLabelsAsConstants();
			list.f0.accept(this, converter);
			converter.setTreatLabelsAsConstants();
			
			return null;
		}
		
		@Override
		public SpigletPrintable visit(Stmt stmt, MethodConverter converter){
			converter.setTreatLabelsAsConstants();
			stmt.f0.accept(this, converter);
			converter.unsetTreatLabelsAsConstants();
			
			return null;
		}
		
		public SpigletPrintable visit(StmtExp stmtExp, MethodConverter converter){
			visit(stmtExp.f1, converter);

			SpigletPrintable returnValue = visit(stmtExp.f3, converter);
			return returnValue;
		}

		@Override
		public SpigletPrintable visit(PrintStmt statement, MethodConverter converter){
			converter.emitPrint(visit(statement.f1, converter));
			return null;
		}
		
		@Override
		public SpigletPrintable visit(NoOpStmt unused, MethodConverter converter){
			converter.emitNoop();
			return null;
		}
		
		@Override
		public SpigletPrintable visit(ErrorStmt unused, MethodConverter converter){
			converter.emitError();
			return null;
		}
		
		@Override
		public SpigletPrintable visit(Label label, MethodConverter converter){
			String labelName = label.f0.toString();
			return converter.emitLabel(labelName);
		}
		
		@Override
		public SpigletPrintable visit(CJumpStmt stmt, MethodConverter converter){
			TempVar condition = converter.emitExpressionToTempVar(visit(stmt.f1, converter));
			converter.emitCjump(condition, stmt.f2.f0.toString());
			return null;
		}
		
		@Override
		public SpigletPrintable visit(JumpStmt stmt, MethodConverter converter){
			converter.emitJump(stmt.f1.f0.toString());
			return null;
		}
		
		@Override
		public SpigletPrintable visit(HStoreStmt stmt, MethodConverter converter){
			TempVar ptr = converter.emitExpressionToTempVar(visit(stmt.f1, converter));
			TempVar value = converter.emitExpressionToTempVar(visit(stmt.f3, converter));
			int offset = Integer.parseInt(stmt.f2.f0.toString());
			
			converter.emitMemoryStore(ptr, offset, value);
			return null;
		}
		
		@Override
		public SpigletPrintable visit(HLoadStmt stmt, MethodConverter converter){
			TempVar ptr = converter.emitExpressionToTempVar(visit(stmt.f2, converter));
			int offset = Integer.parseInt(stmt.f3.f0.toString());
			
			TempVar result = (TempVar) visit(stmt.f1, converter);
			
			converter.emitMemoryLoad(result, ptr, offset);
			return null;
		}
		
		@Override
		public SpigletPrintable visit(MoveStmt stmt, MethodConverter converter){
			TempVar result = (TempVar) visit(stmt.f1, converter);
			SpigletPrintable expr = visit(stmt.f2, converter);
			
			converter.emitMove(result, expr);
			return null;
		}
		
		@Override
		public SpigletPrintable visit(BinOp expr, MethodConverter converter){
			TempVar lhs = converter.emitExpressionToTempVar(visit(expr.f1, converter));
			SpigletPrintable rhs = visit(expr.f2, converter);
			
			String operatorToken = expr.f0.f0.choice.toString();
			BinaryOp op = BinaryOp.valueOf(operatorToken);
			
			return converter.emitTempFromExpression(op, lhs, rhs);
		}

		@Override
		public SpigletPrintable visit(Call expr, MethodConverter converter){
			SpigletPrintable calledMethod = visit(expr.f1, converter);
			
			List<Exp> parameters = new ArrayList<Exp>(); 
			GJVoidVisitor<List<Exp>> PARAMETER_VISITOR = new GJVoidDepthFirst<List<Exp>>() {
				@Override
				public void visit(Exp expression, List<Exp> pigletExpressions){
					pigletExpressions.add(expression);
				}
			};
			PARAMETER_VISITOR.visit(expr.f3, parameters);
			
			List<TempVar> temps = new ArrayList<TempVar>();
			for (Exp param : parameters){
				temps.add(converter.emitExpressionToTempVar(visit(param, converter)));
			}
			
			return converter.emitTempFromFunctionCall(calledMethod, temps);
		}
		
		@Override
		public SpigletPrintable visit(HAllocate expr, MethodConverter converter){
			return converter.emitAllocationToTempVar(visit(expr.f1, converter));
		}
		
		@Override
		public SpigletPrintable visit(Temp expr, MethodConverter converter){
			return new TempVar(Integer.parseInt(expr.f1.f0.toString()));
		}
		
		public SpigletPrintable visit(IntegerLiteral expr, MethodConverter converter){
			return new ConstantValue(Integer.parseInt(expr.f0.toString()));
		}
	}
}