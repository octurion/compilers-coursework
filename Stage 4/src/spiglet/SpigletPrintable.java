package spiglet;

/**
 * An interface that provides a string representation of any simple Spiglet
 * expression (temporary variable, integer constant or label).
 */
public interface SpigletPrintable {
	public String toSpigletExpression();
}
