import java.io.*;

public final class Parser {
	private LookaheadReader reader;

	public Parser(InputStream stream) throws IOException {
		if (stream == null) {
			throw new NullPointerException();
		}
		this.reader = new LookaheadReader(stream);
	}

	public ExprToken parseFormula() throws IOException, ParseException {
		return ExprToken.parseExprToken(reader);
	}
}
