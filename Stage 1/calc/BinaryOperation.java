public final class BinaryOperation implements Expression {
	public enum Operation {
		PLUS('+'), MINUS('-'), TIMES('*'), OVER('/');

		private final char symbol;

		private Operation(char symbol) {
			this.symbol = symbol;
		}

		public char getSymbol() {
			return symbol;
		}

		public String toString() {
			return String.valueOf(symbol);
		}
	}

	private Expression lhs;
	private Operation op;
	private Expression rhs;

	public BinaryOperation(Expression lhs, Operation op, Expression rhs) {
		this.lhs = lhs;
		this.op = op;
		this.rhs = rhs;
	}

	public String toLispString() {
		return String.format("(%s %s %s)",
				op.toString(),
				lhs.toLispString(),
				rhs.toLispString());
	}
}
