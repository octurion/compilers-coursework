import java.io.*;

public final class TermToken {
	private FactorToken head;

	// We allow tail to be null, in which case the expression is
	// essentially the head
	private TermTailToken tail;

	private TermToken(FactorToken head, TermTailToken tail) {
		if (head == null) {
			throw new NullPointerException();
		}

		this.head = head;
		this.tail = tail;
	}

	public static TermToken parseTermToken(LookaheadReader reader) throws IOException, ParseException {
		FactorToken head = FactorToken.parseFactorToken(reader);
		TermTailToken tail = TermTailToken.parseTermTailToken(reader);

		return new TermToken(head, tail);
	}

	public Expression toExpression() {
		Expression treeRoot = head.toExpression();
		
		for (TermTailToken iter = tail; iter != null; iter = iter.getTail()){
			treeRoot = new BinaryOperation(treeRoot,
					iter.getOperation(),
					iter.getFactorToken().toExpression());
		}
		
		return treeRoot;
	}

	public FactorToken getFactor() {
		return head;
	}

	public TermTailToken getTail() {
		return tail;
	}
}
