import java.io.*;

public final class ExprTailToken {
	private BinaryOperation.Operation op;
	private TermToken term;
	private ExprTailToken tail;

	private ExprTailToken(BinaryOperation.Operation op, TermToken term, ExprTailToken tail) {
		this.op = op;
		this.term = term;
		this.tail = tail;
	}

	public BinaryOperation.Operation getOperation() {
		return op;
	}

	public TermToken getTerm() {
		return term;
	}

	public ExprTailToken getTail() {
		return tail;
	}

	public static ExprTailToken parseExprTailToken(LookaheadReader reader) throws IOException, ParseException {
		BinaryOperation.Operation op;

		switch (reader.peek()){
			case ')':
			case '\n':
			case -1:
				return null;
				
			case '+':
				op = BinaryOperation.Operation.PLUS;
				break;

			case '-':
				op = BinaryOperation.Operation.MINUS;
				break;

			default:
				throw new ParseException();
		}
		reader.advance();

		TermToken term = TermToken.parseTermToken(reader);
		ExprTailToken tail = ExprTailToken.parseExprTailToken(reader);

		return new ExprTailToken(op, term, tail);
	}
}
