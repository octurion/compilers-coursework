import java.io.*;

public final class LookaheadReader {
	private InputStream stream;
	private int lookahead;

	public LookaheadReader(InputStream stream) throws IOException {
		if (stream == null) {
			throw new NullPointerException();
		}

		this.stream = stream;
		this.lookahead = stream.read();
	}

	public int peek() {
		return lookahead;
	}

	public void consume(int char_val) throws IOException, ParseException {
		if (lookahead != char_val) {
			throw new ParseException();
		}

		lookahead = stream.read();
	}

	public void advance() throws IOException, ParseException {
		lookahead = stream.read();
	}
}
