import java.io.*;

public final class ExprToken {
	private TermToken head;

	// We allow tail to be null, in which case the expression is
	// essentially the head
	private ExprTailToken tail;

	private ExprToken(TermToken head, ExprTailToken tail) {
		if (head == null) {
			throw new NullPointerException();
		}

		this.head = head;
		this.tail = tail;
	}

	public static ExprToken parseExprToken(LookaheadReader reader) throws IOException, ParseException {
		TermToken head = TermToken.parseTermToken(reader);
		ExprTailToken tail = ExprTailToken.parseExprTailToken(reader);

		return new ExprToken(head, tail);
	}

	public Expression toExpression() {
		Expression treeRoot = head.toExpression();
		
		for (ExprTailToken iter = tail; iter != null; iter = iter.getTail()){
			treeRoot = new BinaryOperation(treeRoot,
					iter.getOperation(),
					iter.getTerm().toExpression());
		}
		
		return treeRoot;
	}
}
