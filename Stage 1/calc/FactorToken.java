import java.io.*;

public final class FactorToken {
	private NumToken number;
	// Do not use two classes for an algebraic data type. And, yes, such a
	// thing IS ugly.
	private boolean isNumber;
	private ExprToken exprToken;

	public Expression toExpression() {
		if (isNumber) {
			return number;
		}

		return exprToken.toExpression();
	}

	private FactorToken(int digit) {
		isNumber = true;
		number = new NumToken(digit);
	}

	private FactorToken(ExprToken token) {
		isNumber = false;
		exprToken = token;
	}

	public static FactorToken parseFactorToken(LookaheadReader reader) throws IOException, ParseException {
		int top_ch = reader.peek();
		if ('0' <= top_ch && top_ch <= '9') {
			reader.advance();
			return new FactorToken(top_ch - '0');
		}

		if (top_ch == '(') {
			reader.advance();
			ExprToken token = ExprToken.parseExprToken(reader);
			reader.consume(')');
			return new FactorToken(token);
		}

		throw new ParseException();
	}
}
