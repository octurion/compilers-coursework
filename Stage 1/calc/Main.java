import java.io.*;

public final class Main {
	public static void main(String[] args) throws IOException, ParseException{
		try {
			Parser parser = new Parser(System.in);
			
			ExprToken token = parser.parseFormula();
			System.out.println(token.toExpression().toLispString());
		}
		catch (IOException e){
			System.err.printf("An I/O error occurred: %s\n", e.getMessage());
		}
		catch (ParseException e){
			System.err.println("Parse error");
		}

	}
}
