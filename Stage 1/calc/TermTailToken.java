import java.io.*;

public final class TermTailToken {
	private BinaryOperation.Operation op;
	private FactorToken factor;
	private TermTailToken tail;

	private TermTailToken(BinaryOperation.Operation op, FactorToken factor, TermTailToken tail) {
		this.op = op;
		this.factor = factor;
		this.tail = tail;
	}

	public BinaryOperation.Operation getOperation() {
		return op;
	}

	public FactorToken getFactorToken() {
		return factor;
	}

	public TermTailToken getTail() {
		return tail;
	}

	public static TermTailToken parseTermTailToken(LookaheadReader reader) throws IOException, ParseException {
		BinaryOperation.Operation op;

		switch (reader.peek()){
			case '+':
			case '-':
			case ')':
			case '\n':
			case -1:
				return null;
				
			case '*':
				op = BinaryOperation.Operation.TIMES;
				break;

			case '/':
				op = BinaryOperation.Operation.OVER;
				break;

			default:
				throw new ParseException();
		}
		reader.advance();

		FactorToken factor = FactorToken.parseFactorToken(reader);
		TermTailToken tail = TermTailToken.parseTermTailToken(reader);

		return new TermTailToken(op, factor, tail);
	}
}

