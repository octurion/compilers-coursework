public final class NumToken implements Expression {
	private int digit;

	public NumToken(int digit) {
		this.digit = digit;
	}

	public String toLispString() {
		return String.valueOf(digit);
	}
}
