import parser.*;
import lexer.*;
import node.*;

import java.io.*;

public class Main{

    public static void main(String[] args){
        try{
            Parser p = new Parser(new Lexer(new PushbackReader(
                            new InputStreamReader(System.in), 1024)));

            p.parse();

            System.err.println("Successfully parsed the expression!");
        }
        catch(ParserException ex){
            System.err.printf("Parser error: %s\n", ex.getMessage());
        }
        catch(LexerException ex){
            System.err.printf("Lexer error: %s\n", ex.getMessage());
        }
        catch(IOException ex){
            System.err.printf("I/O error: %s\n", ex.getMessage());
        }
        catch(Exception ex){
            System.err.printf("Unknown error: %s\n", ex.getMessage());
        }
    }

}
