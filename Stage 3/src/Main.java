import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import piglet.CodeEmitter;
import program.ClassHierarchy;
import program.SemanticException;
import syntaxtree.Goal;

public class Main {
	public static void main(String[] args) {
		for (String filename : args) {
			if (!filename.endsWith(".java")){
				System.out.printf(
					"Skipping file %s because it does not have a .java extension%n",
					filename);
				continue;
			}
			String outFilename = replaceSuffix(filename, ".java", ".pg");
			System.out.printf("Now parsing file %s...%n", filename);
			parseFile(filename, outFilename);
		}
	}
	
	private static void parseFile(String filename, String outFilename){
		InputStream in = null;
		FileWriter out = null;
		try {
			in = new FileInputStream(filename);
			out = new FileWriter(outFilename);
			MiniJavaParser parser = new MiniJavaParser(in);
			Goal goal = parser.Goal();
			
			ClassHierarchy ch = ClassHierarchy.generateDeclarations(goal);
			out.write(CodeEmitter.emitPigletCode(ch));

			System.out.println("File parsed successfully!");
		}
		catch (IOException e) {
			System.err.printf("An I/O error occurred while parsing file \"%s\": %s%n",
				filename, e.getMessage());
		}
		catch (ParseException e) {
			System.err.println(e.getMessage());
		}
		catch (SemanticException e){
			System.err.println(e.getMessage());
		}
		finally {
			try {
				if (in != null) in.close();
				if (out != null) out.close();
			}
			catch(IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
	private static String replaceSuffix(String target, String suffix, String replacement) {
	    if (!target.endsWith(suffix)) {
	        return target;
	    }

	    String prefix = target.substring(0, target.length() - suffix.length());
	    return prefix + replacement;
	}
}
