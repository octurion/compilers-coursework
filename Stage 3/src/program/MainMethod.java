package program;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import syntaxtree.MainClass;
import syntaxtree.NodeListOptional;

public final class MainMethod {
	private Map<String, Variable> locals;
	private NodeListOptional statements;
	
	public Map<String, Variable> getLocals() {
		return Collections.unmodifiableMap(locals);
	}

	public NodeListOptional getStatements() {
		return statements;
	}

	MainMethod(ClassHierarchy ch, MainClass mainClass) {
		this.locals = checkAndGetNonDuplicateLocals(SyntaxTreeUtils.acceptAndCollect(mainClass.f14,
				new Variable.VariableExtractor(), ch));
		String argsParameter = mainClass.f1.f0.toString();
		if (this.locals.containsKey(argsParameter)){
			throw new SemanticException(
					String.format("Parameter %s redefined as local",
							argsParameter));
		}
		
		this.statements = mainClass.f15;
	}
	
	private static Map<String, Variable> checkAndGetNonDuplicateLocals(List<Variable> localsList) {
		Map<String, Variable> localsMap = new LinkedHashMap<String, Variable>();
		for (Variable v : localsList){
			if (localsMap.put(v.getName(), v) != null){
				throw new SemanticException(
						String.format("Local %s redefined twice",
								v.getName()));
			}
		}
		return localsMap;
	}
}