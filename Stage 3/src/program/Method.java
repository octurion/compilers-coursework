package program;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import syntaxtree.FormalParameterList;
import syntaxtree.MethodDeclaration;
import visitor.GJDepthFirst;

public final class Method {
	private String methodName;
	private Map<String, Variable> parameters;
	private Map<String, Variable> locals;
	private VarType returnType;

	private MethodDeclaration methodNode;
	
	public String getMethodName() {
		return methodName;
	}
	
	public Map<String, Variable> getParameters() {
		return Collections.unmodifiableMap(parameters);
	}

	public Map<String, Variable> getLocals() {
		return Collections.unmodifiableMap(locals);
	}
	
	public Map<String, Variable> getAllLocals(){
		Map<String, Variable> retval = new LinkedHashMap<String, Variable>(parameters);
		retval.putAll(locals);
		
		return retval;
	}

	public VarType getReturnType() {
		return returnType;
	}
	
	@Override
	public String toString(){
		return String.format("%s %s%s",
				returnType, methodName, parameters.values());
	}
	
	public MethodDeclaration getMethodNode(){
		return methodNode;
	}

	Method(VarType returnType,
			String methodName,
			Map<String, Variable> parameters,
			Map<String, Variable> locals,
			MethodDeclaration node) {
		this.methodName = methodName;
		this.parameters = new LinkedHashMap<String, Variable>(parameters);
		this.locals = new LinkedHashMap<String, Variable>(locals);
		this.returnType = returnType;

		this.methodNode = node;
	}

	public boolean isCompatibleWith(Method m){
		if (m == null){
			throw new NullPointerException();
		}
		
		if (!returnType.equals(m.returnType)){
			return false;
		}
		
		if (parameters.size() != m.parameters.size()){
			return false;
		}
		
		Iterator<Variable> selfParams = parameters.values().iterator();
		Iterator<Variable> otherParams = m.parameters.values().iterator();
		
		while(selfParams.hasNext() && otherParams.hasNext()){
			VarType selfParamType = selfParams.next().getType();
			VarType otherParamType = otherParams.next().getType();
			if (!selfParamType.equals(otherParamType)){
				return false;
			}
		}

		return true;
	}

	static class MethodExtractor extends GJDepthFirst<Method, ClassHierarchy>{
		public MethodExtractor(){}

		public Method visit(MethodDeclaration d, ClassHierarchy ch){
			List<Variable> paramsList = new FormalParameterExtractor().visit(d.f4, ch);
			if (paramsList == null){
				paramsList = Collections.emptyList();
			}
			Map<String, Variable> paramsMap = checkNonDuplicate(paramsList);
			
			List<Variable> localsList = SyntaxTreeUtils.acceptAndCollect(d.f7,
					new Variable.VariableExtractor(), ch);
			Map<String, Variable> localsMap = checkNonDuplicate(localsList);
			
			Set<String> localsParamsCommon = new HashSet<String>(paramsMap.keySet());
			localsParamsCommon.retainAll(localsMap.keySet());
			if (!localsParamsCommon.isEmpty()){
				throw new SemanticException(String.format(
						"The following parameters are redefined as locals: %s",
						localsParamsCommon));
			}

			VarType returnType = VarType.extractType(d.f1, ch);
			return new Method(returnType,
					d.f2.f0.toString(),
					paramsMap,
					localsMap,
					d);
		}

		private static Map<String, Variable> checkNonDuplicate(List<Variable> paramsList) {
			Map<String, Variable> paramsMap = new LinkedHashMap<String, Variable>();
			for (Variable v : paramsList){
				if (paramsMap.put(v.getName(), v) != null){
					throw new SemanticException(
							String.format("Parameter or local %s redefined twice",
									v.getName()));
				}
			}
			return paramsMap;
		}
	}

	private static class FormalParameterExtractor extends GJDepthFirst<List<Variable>, ClassHierarchy>{
		public FormalParameterExtractor(){}
		
		public List<Variable> visit(FormalParameterList p, ClassHierarchy ch){
			List<Variable> result = new ArrayList<Variable>();
			result.add(Variable.extractVariable(p.f0, ch));
			
			result.addAll(SyntaxTreeUtils.acceptAndCollect(p.f1.f0,
					new Variable.VariableExtractor(), ch));
			return result;
		}
	}
}
