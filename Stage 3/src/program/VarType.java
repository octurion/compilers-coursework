package program;
import syntaxtree.ArrayType;
import syntaxtree.BooleanType;
import syntaxtree.Identifier;
import syntaxtree.IntegerType;
import syntaxtree.Type;
import visitor.GJDepthFirst;

public final class VarType {
	public static enum Primitive {
		INTEGER("int"), BOOLEAN("boolean"), INTEGER_ARRAY("int[]");

		private String typeString;
		private Primitive(String typeString){
			this.typeString = typeString;
		}

		@Override public String toString(){
			return typeString;
		}
	}

	public static enum VarKind {
		CLASS, PRIMITIVE
	}

	private MiniJavaClass classType;
	private Primitive primitiveType;
	private VarKind which;

	public MiniJavaClass getClassType() {
		return classType;
	}

	public Primitive getPrimitiveType() {
		return primitiveType;
	}

	public VarKind getWhich() {
		return which;
	}

	private VarType(Primitive p){
		if (p == null) {
			throw new NullPointerException();
		}
		primitiveType = p;
		which = VarKind.PRIMITIVE;
	}

	public VarType(MiniJavaClass c){
		if (c == null) {
			throw new NullPointerException();
		}

		classType = c;
		which = VarKind.CLASS;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof VarType)){
			return false;
		}
		
		VarType other = (VarType) o;
		if (which != other.which){
			return false;
		}
		
		if (which == VarKind.PRIMITIVE){
			return primitiveType == other.primitiveType;
		}
		
		return classType.equals(other.classType);
	}
	
	public boolean isConvertibleTo(VarType type){
		if (type == null){
			throw new NullPointerException();
		}
		
		if (which != type.which){
			return false;
		}
		
		if (which == VarKind.PRIMITIVE){
			return primitiveType == type.primitiveType; 
		}
		
		return classType.isSubclassOf(type.classType);
	}
	
	@Override
	public int hashCode(){
		if (which.equals(VarKind.CLASS)){
			return classType.hashCode();
		}
		
		return primitiveType.hashCode();
	}
	
	@Override
	public String toString(){
		if (which == VarKind.PRIMITIVE){
			return primitiveType.toString();
		}
		
		return classType.toString();
	}

	static VarType extractType(Type t, ClassHierarchy ch){
		return new TypeExtractor().visit(t, ch);
	}
	
	private static class TypeExtractor extends GJDepthFirst<VarType, ClassHierarchy> {
		public TypeExtractor(){}
		public VarType visit(IntegerType t, ClassHierarchy ch){
			return VarType.INTEGER_TYPE;
		}
		
		public VarType visit(BooleanType t, ClassHierarchy ch){
			return VarType.BOOLEAN_TYPE;
		}
		
		public VarType visit(ArrayType t, ClassHierarchy ch){
			return VarType.INTEGER_ARRAY_TYPE;
		}

		public VarType visit(Identifier classType, ClassHierarchy ch){
			String className = classType.f0.toString();
			MiniJavaClass clazz = ch.getClassesByName().get(className);
			if (clazz == null){
				throw new SemanticException(String.format("No class named %s has been declared", className));
			}
			
			return new VarType(clazz);
		}
	}

	public static VarType INTEGER_TYPE = new VarType(Primitive.INTEGER);
	public static VarType BOOLEAN_TYPE = new VarType(Primitive.BOOLEAN);
	public static VarType INTEGER_ARRAY_TYPE = new VarType(Primitive.INTEGER_ARRAY);
}
