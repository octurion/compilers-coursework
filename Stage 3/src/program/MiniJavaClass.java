package program;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import piglet.VTable;

import syntaxtree.NodeListOptional;

public final class MiniJavaClass {
	private String className;
	private MiniJavaClass superClass;
	
	private Map<String, Method> methods;
	private Map<String, Variable> members;

	private NodeListOptional membersTree;
	private NodeListOptional methodsTree;

	private VTable vtable;
	
	MiniJavaClass(String className, NodeListOptional members, NodeListOptional methods) {
		this.className = className;
		this.superClass = null;
		this.methods = null;
		
		this.membersTree = members;
		this.methodsTree = methods;
	}
	
	MiniJavaClass(String className, MiniJavaClass superClass,
			NodeListOptional members, NodeListOptional methods) {
		this(className, members, methods);
		this.superClass = superClass;
	}

	@Override
	public String toString(){
		return className;
	}

	public MiniJavaClass getSuperClass(){
		return superClass;
	}
	
	public Map<String, Method> getClassMethods(){
		return Collections.unmodifiableMap(methods);
	}
	
	public Map<String, Variable> getClassMembers(){
		return Collections.unmodifiableMap(members);
	}
	
	public VTable getVtable(){
		return vtable;
	}

	public boolean isSubclassOf(MiniJavaClass parent){
		for (MiniJavaClass iter = this; iter != null; iter = iter.getSuperClass()){
			if (iter == parent){
				return true;
			}
		}
		return false;
	}
	
	public boolean isSuperclassOf(MiniJavaClass clazz){
		if (clazz == null){
			return false;
		}
		
		return clazz.isSubclassOf(this);
	}

	void populateClassMembersAndMethods(ClassHierarchy ch){
		if (methods != null || members != null){
			throw new IllegalStateException("Class members and methods have been already populated");
		}
		
		populateClassMembers(ch);
		populateClassMethods(ch);
	}
	
	void ensureInheritanceRules(){
		MiniJavaClass parent;
		for(parent = superClass; parent != null; parent = parent.getSuperClass()){
			ensureCompatibleCommonMethodsWith(parent);
		}
	}

	private void ensureCompatibleCommonMethodsWith(MiniJavaClass parent) {
		Set<String> commonMethods = new HashSet<String>(methods.keySet());
		commonMethods.retainAll(parent.methods.keySet());
		
		for (String s : commonMethods){
			Method subclassMethod = methods.get(s);
			Method superclassMethod = parent.methods.get(s);
			
			if (!subclassMethod.isCompatibleWith(superclassMethod)){
				throw new SemanticException(String.format(
						"Incompatible method %s defined in superclass %s and subclass %s",
						s, parent, this));
			}
		}
	}

	private void populateClassMembers(ClassHierarchy ch){
		members = new LinkedHashMap<String, Variable>();
		for (Variable var : SyntaxTreeUtils.acceptAndCollect(membersTree,
				new Variable.VariableExtractor(),
				ch)){
			if (members.put(var.getName(), var) != null){
				throw new SemanticException(String.format("Member %s of class %s redefined",
						var.getName(), className));
			}
		}
	}

	private void populateClassMethods(ClassHierarchy ch){
		methods = new LinkedHashMap<String, Method>();
		for (Method m : SyntaxTreeUtils.acceptAndCollect(methodsTree,
				new Method.MethodExtractor(),
				ch)){
			if (methods.put(m.getMethodName(), m) != null){
				throw new SemanticException(String.format("Method %s of class %s redefined",
						m.getMethodName(), className));
			}
		}
	}
	
	public VarType getTypeOfMember(String memberName){
		for (MiniJavaClass iter = this; iter != null; iter = iter.superClass){
			Variable result = iter.members.get(memberName);
			if (result == null){
				continue;
			}

			return result.getType();
		}
		
		return null;
	}
	
	void setVtable(VTable vtable){
		this.vtable = vtable;
	}
}
