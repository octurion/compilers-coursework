package program;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import piglet.VTable;

import syntaxtree.ClassDeclaration;
import syntaxtree.ClassExtendsDeclaration;
import syntaxtree.Goal;
import syntaxtree.MainClass;
import syntaxtree.NodeListOptional;
import visitor.*;

public final class ClassHierarchy {
	private MainMethod mainMethod;
	private Map<String, MiniJavaClass> classesByName;

	public Map<String, MiniJavaClass> getClassesByName() {
		return Collections.unmodifiableMap(classesByName);
	}

	public MainMethod getMainMethod(){
		return mainMethod;
	}

	private ClassHierarchy() {
		classesByName = new LinkedHashMap<String, MiniJavaClass>();
	}

	private void addNewClass(String className, String superclassName,
			NodeListOptional members, NodeListOptional methods) {
		if (classesByName.containsKey(className)){
			throw new SemanticException(String.format("Class %s has been redefined", className));
		}

		MiniJavaClass superClass = classesByName.get(superclassName);
		if (superclassName != null && superClass == null){
			throw new SemanticException(String.format("Class %s must be defined before class %s",
					superclassName, className));
		}
		classesByName.put(className, new MiniJavaClass(className, superClass, members, methods));
	}

	void populateAllMembersAndMethods(){
		for (MiniJavaClass c : classesByName.values()) {
			c.populateClassMembersAndMethods(this);
		}
		
		for (MiniJavaClass c : classesByName.values()) {
			c.ensureInheritanceRules();
		}
	}

	public static ClassHierarchy generateDeclarations(Goal goal){
		ClassHierarchy decls = new ClassHierarchy();

		new HierarchyExtractor().visit(goal, decls);
		decls.mainMethod = new MainMethod(decls, goal.f0);
		decls.populateAllMembersAndMethods();

		MethodChecker.checkMainMethod(decls);
		for (MiniJavaClass clazz : decls.getClassesByName().values()){
			MiniJavaClass superclass = clazz.getSuperClass();
			if (superclass == null){
				clazz.setVtable(new VTable(clazz));
			}
			else {
				clazz.setVtable(new VTable(superclass.getVtable(), clazz));
			}
			
			for (Method method : clazz.getClassMethods().values()){
				MethodChecker.checkMethod(method, decls, clazz);
			}
		}

		return decls;
	}

	private static class HierarchyExtractor extends GJVoidDepthFirst<ClassHierarchy> {
		public HierarchyExtractor(){}

		public void visit(ClassDeclaration clazz, ClassHierarchy decls) {
			String className = clazz.f1.f0.toString();
			decls.addNewClass(className, null, clazz.f3, clazz.f4);
		}

		public void visit(MainClass clazz, ClassHierarchy decls){
			String mainClassName = clazz.f1.f0.toString();
			decls.addNewClass(mainClassName, null, new NodeListOptional(), new NodeListOptional());
		}

		public void visit(ClassExtendsDeclaration clazz, ClassHierarchy decls){
			String className = clazz.f1.f0.toString();
			decls.addNewClass(className, clazz.f3.f0.toString(), clazz.f5, clazz.f6);
		}
	}
}