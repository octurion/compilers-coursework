package program;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import syntaxtree.Node;
import syntaxtree.NodeListOptional;

public class SyntaxTreeUtils {
	private SyntaxTreeUtils(){}

	public static <R, A> List<R> acceptAndCollect(NodeListOptional list,
			visitor.GJVisitor<R,A> v,
			A argu) {
		List<R> result = new ArrayList<R>();
		for (Node node : Collections.list(list.elements())){
			result.add(node.accept(v, argu));
		}
	
		return result;
	}

	public static <R> List<R> acceptAndCollect(NodeListOptional list,
			visitor.GJNoArguVisitor<R> v) {
		List<R> result = new ArrayList<R>();
		for (Node node : Collections.list(list.elements())){
			result.add(node.accept(v));
		}
 
		return result;
	}
}
