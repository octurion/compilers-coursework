package piglet;

import program.VarType;

public class Expression {
	public static enum ExprType {
		CONSTANT, TEMP_VAR
	}

	public static class TempVar {
		private int varID;
		private VarType type;
		public TempVar(int varID, VarType type) {
			this.varID = varID;
			this.type = type;
		}

		public int getVarID() {
			return varID;
		}
		
		public VarType getType(){
			return type;
		}
		
		@Override
		public String toString(){
			return String.format("TEMP %d", varID);
		}
	}

	private ExprType type;
	private TempVar temp;
	private int constant;
	
	Expression(int constant){
		this.type = ExprType.CONSTANT;
		this.constant = constant;
	}
	
	Expression(TempVar var){
		this.type = ExprType.TEMP_VAR;
		this.temp = var;
	}
	
	public ExprType getType() {
		return type;
	}

	public TempVar getTemp() {
		if (type != ExprType.TEMP_VAR){
			throw new IllegalStateException();
		}
		return temp;
	}

	public int getConstant() {
		if (type != ExprType.CONSTANT){
			throw new IllegalStateException();
		}
		return constant;
	}
	
	@Override
	public String toString(){
		if (type == ExprType.CONSTANT){
			return String.valueOf(constant);
		}
		else {
			return temp.toString();
		}
	}
	
	public static final Expression ONE = new Expression(1);
	public static final Expression ZERO = new Expression(0);

}