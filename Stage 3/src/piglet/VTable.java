package piglet;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import program.Method;
import program.MiniJavaClass;

public class VTable {
	public static class MethodInfo {
		private Method method;
		private int vtableOffset;
		private String methodPigletLabel;
		
		public int getVtableOffset() {
			return vtableOffset;
		}

		public String getMethodPigletLabel() {
			return methodPigletLabel;
		}
		
		public Method getMethod(){
			return method;
		}

		MethodInfo(Method method, int vtableOffset, String methodPigletLabel) {
			this.method = method;
			this.vtableOffset = vtableOffset;
			this.methodPigletLabel = methodPigletLabel;
		}
		
		@Override
		public String toString(){
			return String.format("%s(offset: %s)", methodPigletLabel,
					vtableOffset);
		}
	}
	
	public Map<String, Integer> getMemberOffsets() {
		return Collections.unmodifiableMap(memberOffsets);
	}

	public Map<String, MethodInfo> getMethodInfo() {
		return Collections.unmodifiableMap(methodInfo);
	}

	public int getTableSize() {
		return tableSize;
	}
	
	public int getTableSlots() {
		return tableSize / 4;
	}

	private Map<String, Integer> memberOffsets;
	private Map<String, MethodInfo> methodInfo;
	private int tableSize;
	
	private static final int PRIMITIVE_SIZE = 4;
	
	public VTable(MiniJavaClass clazz){
		memberOffsets = new HashMap<String, Integer>();
		methodInfo = new HashMap<String, MethodInfo>();
		
		int currentOffset = 0;
		for (Map.Entry<String, Method> method: clazz.getClassMethods().entrySet()){
			methodInfo.put(method.getKey(), 
					new MethodInfo(method.getValue(),
							currentOffset,
							classMethodLabel(clazz.toString(), method.getKey())));
			currentOffset += PRIMITIVE_SIZE;
		}

		for (String memberName : clazz.getClassMembers().keySet()){
			memberOffsets.put(memberName, currentOffset);
			currentOffset += PRIMITIVE_SIZE;
		}

		tableSize = currentOffset;
	}
	
	public VTable(VTable superVtable, MiniJavaClass clazz){
		if (superVtable == null){
			throw new NullPointerException();
		}
		
		memberOffsets = new LinkedHashMap<String, Integer>(superVtable.memberOffsets);
		methodInfo = new LinkedHashMap<String, MethodInfo>(superVtable.methodInfo);

		int currentOffset = superVtable.tableSize;
		for (Map.Entry<String, Method> method: clazz.getClassMethods().entrySet()){
			MethodInfo superDefinition = methodInfo.get(method.getKey());
			int methodOffset = superDefinition == null
					? currentOffset
					: superDefinition.getVtableOffset();
			
			methodInfo.put(method.getKey(), new MethodInfo(method.getValue(),
					methodOffset,
					classMethodLabel(clazz.toString(), method.getKey())));
			if (superDefinition == null){
				currentOffset += PRIMITIVE_SIZE;
			}
		}
		
		for (String memberName : clazz.getClassMembers().keySet()){
			memberOffsets.put(memberName, currentOffset);
			currentOffset += PRIMITIVE_SIZE;
		}
		
		tableSize = currentOffset;
	}
	
	@Override
	public String toString(){
		return String.format("<%s, %s>", memberOffsets, methodInfo);
	}
	
	public static String classMethodLabel(String className, String methodName){
		return String.format("_Z%s_%s_%s", className.length(), className, methodName);
	}
	
	public static String classConstructorLabel(String className){
		return String.format("_Z%d_%s_new", className.length(), className);
	}
}