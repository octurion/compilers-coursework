package piglet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import piglet.Expression.TempVar;
import piglet.VTable.MethodInfo;
import program.ClassHierarchy;
import program.MainMethod;
import program.Method;
import program.MiniJavaClass;
import program.SyntaxTreeUtils;
import program.VarType;
import program.Variable;
import syntaxtree.*;
import visitor.GJDepthFirst;
import visitor.GJNoArguDepthFirst;

public class PigletMethodEmitter {
	
	public enum BinOp {
		LESS_THAN("LT"), PLUS("PLUS"), MINUS("MINUS"), TIMES("TIMES");
		
		private String pigletName;
		private BinOp(String pigletName){
			this.pigletName = pigletName;
		}
		public String getPigletName() {
			return pigletName;
		}
	}
	
	public static String emitMethod(MiniJavaClass clazz, Method m,
			ClassHierarchy ch){
		String code = new PigletMethodEmitter(clazz, m, ch).emitMethodCode(m);
			return String.format("%s[%d]%nBEGIN%n%sEND%n",
					VTable.classMethodLabel(clazz.toString(), m.getMethodName()),
					m.getParameters().size() + 1,
					code);
	}
	
	public static String emitMain(MainMethod m, ClassHierarchy ch){
		String code = new PigletMethodEmitter(m, ch).emitMain(m);
		return String.format("MAIN%n%sEND%n", code);
	}

	private Map<String, TempVar> locals;
	private ClassHierarchy hierarchy;
	
	private StringBuilder emittedStatements;
	
	private int nextTempID;
	private int nextLabelID;
	
	private TempVar newUniqueTemp(VarType type){
		return new TempVar(nextTempID++, type);
	}
	
	private String newUniqueLabel(){
		return String.format("L%s", nextLabelID++);
	}
	
	Map<String, TempVar> getLocals() {
		return locals;
	}
	
	ClassHierarchy getHierarchy() {
		return hierarchy;
	}
	
	PigletMethodEmitter(MiniJavaClass thisClass, Method m, ClassHierarchy ch){
		nextTempID = 0;
		nextLabelID = 0;
		
		emittedStatements = new StringBuilder();
		
		hierarchy = ch;
		locals = new HashMap<String, TempVar>();
		locals.put("this", newUniqueTemp(new VarType(thisClass)));
		
		for(Map.Entry<String, Variable> methodVar : m.getAllLocals().entrySet()){
			locals.put(methodVar.getKey(), 
					newUniqueTemp(methodVar.getValue().getType()));
		}
	}
	
	PigletMethodEmitter(MainMethod m, ClassHierarchy ch){
		nextTempID = 0;
		nextLabelID = 0;
	
		emittedStatements = new StringBuilder();
		
		hierarchy = ch;
		locals = new HashMap<String, TempVar>();
		for(Map.Entry<String, Variable> methodVar : m.getLocals().entrySet()){
			locals.put(methodVar.getKey(), 
					newUniqueTemp(methodVar.getValue().getType()));
		}
	}
	
	private String emitMethodCode(Method m){
		MethodEmissionVisitor v = new MethodEmissionVisitor();
		v.visit(m.getMethodNode().f8, this);
		Expression returnExpr = v.visit(m.getMethodNode().f10, this);
		emitReturn(returnExpr);
		return emittedStatements.toString();
	}
	
	private String emitMain(MainMethod m){
		new MethodEmissionVisitor().visit(m.getStatements(), this);
		return emittedStatements.toString();
	}
			
	TempVar emitBinOp(Expression lhs, BinOp op, Expression rhs){
		TempVar returnVar = newUniqueTemp(op == BinOp.LESS_THAN
				? VarType.BOOLEAN_TYPE
				: VarType.INTEGER_TYPE);
		emittedStatements.append(String.format("MOVE %s %s %s %s%n",
				returnVar, op.getPigletName(), lhs, rhs));
		return returnVar;
	}
	
	TempVar emitLoadAt(Expression expr, int offset, VarType type){
		TempVar retVar = newUniqueTemp(type);
		
		emittedStatements.append(String.format("HLOAD %s %s %d%n",
				retVar, expr, offset));
		return retVar;
	}
	
	TempVar emitClassConstruction(MiniJavaClass clazz){
		TempVar retVar = newUniqueTemp(new VarType(clazz));
		
		emittedStatements.append(String.format("MOVE %s CALL %s()%n",
				retVar, VTable.classConstructorLabel(clazz.toString())));
		
		return retVar;
	}
	
	void emitMove(Expression lvalue, Expression rvalue){
		emittedStatements.append(String.format("MOVE %s %s%n", lvalue, rvalue));
	}
	
	void emitStoreAt(Expression lvalue, int offset, Expression rvalue){
		emittedStatements.append(String.format("HSTORE %s %d %s%n",
				lvalue, offset, rvalue));
	}
	
	void emitLabel(String label){
		emittedStatements.append(String.format("%s NOOP%n", label));
	}
	
	void emitPrint(Expression expr){
		emittedStatements.append(String.format("PRINT %s%n", expr));
	}
	
	void emitReturn(Expression expr){
		emittedStatements.append(String.format("RETURN %s%n", expr));
	}
	
	void emitJumpIfFalse(Expression expr, String jumpLabelOnFalse){
		emittedStatements.append(String.format("CJUMP %s %s%n", expr, jumpLabelOnFalse));
	}
	
	void emitJump(String jumpLabelOnTrue){
		emittedStatements.append(String.format("JUMP %s%n", jumpLabelOnTrue));
	}
	
	void emitNotNullCheck(Expression expr){
		String normalFlowLabel = newUniqueLabel();
		emittedStatements.append(String.format("CJUMP LT %s 1 %s%n", expr, normalFlowLabel));
		emitError();
		emitLabel(normalFlowLabel);
	}
	
	void emitInsideBoundsCheck(Expression array, Expression idx){
		Expression length = new Expression(emitLoadAt(array, 0, VarType.INTEGER_TYPE));
		String checkPassedTestOneLabel = newUniqueLabel();
		String checkPassedTestTwoLabel = newUniqueLabel();
		
		emittedStatements.append(String.format("CJUMP LT %s 0 %s%n",
				idx, checkPassedTestOneLabel));
		emitError();
		emitLabel(checkPassedTestOneLabel);
		emittedStatements.append(String.format("CJUMP LT MINUS %s 1 %s %s%n",
				length, idx, checkPassedTestTwoLabel));
		emitError();
		emitLabel(checkPassedTestTwoLabel);
	}
	
	void emitNonNegative(Expression expr){
		String checkPassedTest = newUniqueLabel();
		emittedStatements.append(String.format("CJUMP LT %s 0 %s%n",
				expr, checkPassedTest));
		emitError();
		emitLabel(checkPassedTest);
	}
	
	void emitError(){
		emittedStatements.append("ERROR\n");
	}
	
	TempVar emitCalloc(Expression expr, VarType type){
		TempVar retVar = newUniqueTemp(type);
		
		emittedStatements.append(String.format("MOVE %s CALL CALLOC(%s)%n", retVar, expr));
		return retVar;
	}
	
	TempVar emitArrayLoad(Expression array, Expression idx){
		TempVar var = newUniqueTemp(VarType.INTEGER_TYPE);
		
		emittedStatements.append(String.format("HLOAD %s PLUS %s TIMES 4 %s 4\n",
				var, array, idx));
		return var;
	}
	
	void emitArrayStore(Expression array, Expression idx, Expression value){
		emittedStatements.append(String.format("HSTORE PLUS %s TIMES 4 %s 4 %s\n",
				array, idx, value));
	}
	

	public TempVar emitMethodCall(Expression thisExpression,
			int offset,
			List<Expression> argsAsTemps,
			VarType returnType) {
		TempVar retVal = newUniqueTemp(returnType);
		TempVar funcPtr = newUniqueTemp(VarType.INTEGER_TYPE);

		StringBuilder command = new StringBuilder();
		command.append(String.format("HLOAD %s %s %d%n",
				funcPtr, thisExpression, offset));
		command.append(String.format("MOVE %s CALL %s(",
				retVal, funcPtr));
		
		Iterator<Expression> iter = argsAsTemps.iterator();
		if (iter.hasNext()){
			command.append(iter.next());
		}
		while (iter.hasNext()){
			command.append(" " + iter.next().toString());
		}
		command.append(")\n");
		emittedStatements.append(command.toString());
		
		return retVal;
	}

	private static class MethodEmissionVisitor extends GJDepthFirst<Expression, PigletMethodEmitter> {
		public MethodEmissionVisitor(){}
		
		public Expression visit(AndExpression expr, PigletMethodEmitter data){
			Expression lhs = visit(expr.f0, data);
			String ifFalseLabel = data.newUniqueLabel();
			
			data.emitJumpIfFalse(lhs, ifFalseLabel);
			Expression rhs = visit(expr.f2, data);
			data.emitMove(lhs, rhs);
			
			data.emitLabel(ifFalseLabel);
			return lhs;
		}
		
		public Expression visit(CompareExpression expr, PigletMethodEmitter data){
			Expression lhs = visit(expr.f0, data);
			Expression rhs = visit(expr.f2, data);

			return new Expression(data.emitBinOp(lhs, BinOp.LESS_THAN, rhs));
		}
		
		public Expression visit(PlusExpression expr, PigletMethodEmitter data){
			Expression lhs = visit(expr.f0, data);
			Expression rhs = visit(expr.f2, data);

			return new Expression(data.emitBinOp(lhs, BinOp.PLUS, rhs));
		}
		
		public Expression visit(MinusExpression expr, PigletMethodEmitter data){
			Expression lhs = visit(expr.f0, data);
			Expression rhs = visit(expr.f2, data);

			return new Expression(data.emitBinOp(lhs, BinOp.MINUS, rhs));
		}
		
		public Expression visit(TimesExpression expr, PigletMethodEmitter data){
			Expression lhs = visit(expr.f0, data);
			Expression rhs = visit(expr.f2, data);

			return new Expression(data.emitBinOp(lhs, BinOp.TIMES, rhs));
		}
		
		public Expression visit(ArrayLookup expr, PigletMethodEmitter data){
			Expression array = visit(expr.f0, data);
			Expression idx = visit(expr.f2, data);
			data.emitNotNullCheck(array);
			data.emitInsideBoundsCheck(array, idx);
			
			TempVar retVal = data.emitArrayLoad(array, idx);
			
			return new Expression(retVal);
		}
		
		public Expression visit(ArrayLength expr, PigletMethodEmitter data){
			Expression array = visit(expr.f0, data);
			data.emitNotNullCheck(array);
			TempVar retval = data.emitLoadAt(array, 0, VarType.INTEGER_TYPE);

			return new Expression(retval);
		}
		
		public Expression visit(MessageSend expr, PigletMethodEmitter data){
			Expression thisParameter = visit(expr.f0, data);
			data.emitNotNullCheck(thisParameter);
			
			MiniJavaClass classType = thisParameter.getTemp().getType().getClassType();
			MethodInfo info = classType.getVtable().getMethodInfo().get(expr.f2.f0.toString());
			VarType returnType = info.getMethod().getReturnType();
			int offset = info.getVtableOffset();

			List<syntaxtree.Expression> arguments = new ArgumentsExtractor().visit(expr.f4);
			if (arguments == null){
				arguments = Collections.emptyList();
			}
						
			List<Expression> argsAsTemps = new ArrayList<Expression>();
			argsAsTemps.add(thisParameter);
			for (syntaxtree.Expression arg : arguments){
				Expression emittedArg = visit(arg, data);
				argsAsTemps.add(emittedArg);
			}
			
			return new Expression(data.emitMethodCall(
					thisParameter,
					offset,
					argsAsTemps,
					returnType));
		}
				
		public Expression visit(IntegerLiteral expr, PigletMethodEmitter data){
			return new Expression(Integer.parseInt(expr.f0.toString()));
		}
		
		public Expression visit(TrueLiteral expr, PigletMethodEmitter data){
			return Expression.ONE;
		}
		
		public Expression visit(FalseLiteral expr, PigletMethodEmitter data){
			return Expression.ZERO;
		}
		
		public Expression visit(Identifier expr, PigletMethodEmitter data){
			String identifier = expr.f0.toString();
			TempVar localVar = data.getLocals().get(identifier);
			
			if (localVar != null){
				return new Expression(localVar);
			}
			
			TempVar thisTemp = data.getLocals().get("this");
			MiniJavaClass thisClass = thisTemp.getType().getClassType();
			int offset = thisClass.getVtable().getMemberOffsets().get(identifier);
			return new Expression(data.emitLoadAt(
					new Expression(thisTemp), offset,
					thisClass.getTypeOfMember(identifier)));
		}
		
		public Expression visit(ThisExpression expr, PigletMethodEmitter data){
			return new Expression(data.getLocals().get("this"));
		}
		
		public Expression visit(ArrayAllocationExpression expr, PigletMethodEmitter data){
			Expression length = visit(expr.f3, data);
			data.emitNonNegative(length);
			TempVar arrayAndLength = data.emitBinOp(length, BinOp.PLUS, Expression.ONE);
			TempVar array = data.emitCalloc(new Expression(arrayAndLength), VarType.INTEGER_ARRAY_TYPE);
			data.emitStoreAt(new Expression(array), 0, length);
			
			return new Expression(array);
		}
		
		public Expression visit(AllocationExpression expr, PigletMethodEmitter data){
			return new Expression(data.emitClassConstruction(
					data.getHierarchy().getClassesByName().get(expr.f1.f0.toString())));
		}
		
		public Expression visit(NotExpression expr, PigletMethodEmitter data){
			Expression bool = visit(expr.f1, data);
			
			return new Expression(data.emitBinOp(bool, BinOp.LESS_THAN, Expression.ONE));
		}
		
		public Expression visit(BracketExpression expr, PigletMethodEmitter data){
			return visit(expr.f1, data);
		}
		
		public Expression visit(AssignmentStatement stmt, PigletMethodEmitter data){
			Expression rvalue = visit(stmt.f2, data);
			
			String identifier = stmt.f0.f0.toString();
			TempVar localVar = data.getLocals().get(identifier);
			
			if (localVar != null){
				data.emitMove(new Expression(localVar), rvalue);
				return null;
			}
			
			TempVar thisTemp = data.getLocals().get("this");
			MiniJavaClass thisClass = thisTemp.getType().getClassType();
			int offset = thisClass.getVtable().getMemberOffsets().get(identifier);
			data.emitStoreAt(new Expression(thisTemp), offset, rvalue);
			return null;
		}
		
		public Expression visit(ArrayAssignmentStatement stmt, PigletMethodEmitter data){
			Expression identifier = visit(stmt.f0, data);
			Expression index = visit(stmt.f2, data);
			Expression rvalue = visit(stmt.f5, data);
			
			data.emitNotNullCheck(identifier);
			data.emitInsideBoundsCheck(identifier, index);
			data.emitArrayStore(identifier, index, rvalue);

			return null;
		}
		
		public Expression visit(IfStatement stmt, PigletMethodEmitter data){
			Expression cond = visit(stmt.f2, data);
			
			String elseLabel = data.newUniqueLabel();
			String endIfLabel = data.newUniqueLabel();
			
			data.emitJumpIfFalse(cond, elseLabel);
			
			visit(stmt.f4, data);
			data.emitJump(endIfLabel);
			
			data.emitLabel(elseLabel);
			visit(stmt.f6, data);
			data.emitLabel(endIfLabel);
			
			return null;
		}
		
		public Expression visit(WhileStatement stmt, PigletMethodEmitter data){
			String loopEnterLabel = data.newUniqueLabel();
			String loopExitLabel = data.newUniqueLabel();
			
			data.emitLabel(loopEnterLabel);
			
			Expression expr = visit(stmt.f2, data);
			data.emitJumpIfFalse(expr, loopExitLabel);
			
			visit(stmt.f4, data);
			data.emitJump(loopEnterLabel);
			
			data.emitLabel(loopExitLabel);
			return null;
		}
		
		public Expression visit(PrintStatement stmt, PigletMethodEmitter data){
			Expression expr = visit(stmt.f2, data);
			data.emitPrint(expr);
			
			return null;
		}
	}
	
	static class ArgumentsExtractor extends GJNoArguDepthFirst<List<syntaxtree.Expression>>{
		public ArgumentsExtractor() {}
		
		public List<syntaxtree.Expression> visit(ExpressionList list){
			List<syntaxtree.Expression> result = new ArrayList<syntaxtree.Expression>();
			result.add(list.f0);
			
			result.addAll(SyntaxTreeUtils.<syntaxtree.Expression>acceptAndCollect(
					list.f1.f0,
					new GJNoArguDepthFirst<syntaxtree.Expression>(){
						public syntaxtree.Expression visit(syntaxtree.ExpressionTerm expr){
							return expr.f1;
						}						
					}));
			return result;
		}
	}

}