package piglet;

import java.util.Map;

import piglet.VTable.MethodInfo;
import program.ClassHierarchy;
import program.Method;
import program.MiniJavaClass;

public class CodeEmitter {
	private CodeEmitter(){}
	
	private static final String CALLOC_DEFINITION =
			"CALLOC[1]\n" +
			"BEGIN\n" +
			"MOVE TEMP 1 HALLOCATE TIMES 4 TEMP 0\n" +
			"MOVE TEMP 2 0\n" +
			"LOOP CJUMP LT TEMP 2 TEMP 0 ENDLOOP\n" +
			"HSTORE PLUS TEMP 1 TIMES 4 TEMP 2 0 0\n" +
			"MOVE TEMP 2 PLUS 1 TEMP 2\n" +
			"JUMP LOOP\n" +
			"ENDLOOP NOOP\n" +
			"RETURN TEMP 1\n" +
			"END\n";
	
	private static String emitConstructor(MiniJavaClass clazz){
		StringBuilder statements = new StringBuilder();
		VTable vtable = clazz.getVtable();
		statements.append(String.format("MOVE TEMP 0 CALL CALLOC(%s)%n",
				Math.max(vtable.getTableSlots(), 1)));
		
		for (Map.Entry<String, MethodInfo> method : vtable.getMethodInfo().entrySet()){
			String labelName = clazz.getVtable().getMethodInfo()
					.get(method.getKey()).getMethodPigletLabel();
			statements.append(String.format("HSTORE TEMP 0 %d %s%n",
					method.getValue().getVtableOffset(),
					labelName));
		}
		
		statements.append("RETURN TEMP 0\n");
		
		return String.format("%s[0]%nBEGIN%n%sEND%n",
				VTable.classConstructorLabel(clazz.toString()),
				statements.toString());
	}
	
	public static String emitPigletCode(ClassHierarchy ch){
		StringBuilder code = new StringBuilder();
		
		code.append(PigletMethodEmitter.emitMain(ch.getMainMethod(), ch)).append("\n");
		code.append(CALLOC_DEFINITION).append("\n");
		
		for (MiniJavaClass clazz : ch.getClassesByName().values()){
			code.append(emitConstructor(clazz)).append("\n");
			for (Method m : clazz.getClassMethods().values()){
				code.append(PigletMethodEmitter.emitMethod(clazz, m, ch)).append("\n");
			}
		}
		
		return code.toString();
	}
}
