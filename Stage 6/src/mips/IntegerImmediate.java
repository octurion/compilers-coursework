package mips;

public final class IntegerImmediate implements Immediate {
	private int value;
	
	public IntegerImmediate(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof IntegerImmediate)){
			return false;
		}
		
		IntegerImmediate other = (IntegerImmediate) o;
		return this.value == other.value;
	}
	
	@Override
	public int hashCode(){
		return value;
	}
	
	public static boolean isIntegerImmediate(RegImm expr){
		return (expr instanceof IntegerImmediate);
	}
	
	@Override
	public String toString(){
		return Integer.toString(value);
	}
}
