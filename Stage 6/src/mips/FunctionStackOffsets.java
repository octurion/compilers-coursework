package mips;

public class FunctionStackOffsets {
	private static final int WORD_SIZE = 4;

	private static final int returnAddressOffset = WORD_SIZE;
	
	private int stackframeSize;
	
	public int offsetOfSpilled(int i){
		return WORD_SIZE * (stackframeSize - i);
	}
	
	public int returnAddressOffset(){
		return returnAddressOffset;
	}
	
	public int offsetOfPassed(int i){
		return -WORD_SIZE * (i - 1);
	}
	
	public int getStackFrameSize(){
		return WORD_SIZE * stackframeSize;
	}
	
	public FunctionStackOffsets(int kangaStackSize, int passedArgs){
		if (kangaStackSize < 0 || passedArgs < 0){
			throw new IllegalArgumentException();
		}
		
		this.stackframeSize = kangaStackSize + 1;
	}
}
