package mips;

import syntaxtree.ALoadStmt;
import syntaxtree.AStoreStmt;
import syntaxtree.BinOp;
import syntaxtree.CJumpStmt;
import syntaxtree.CallStmt;
import syntaxtree.ErrorStmt;
import syntaxtree.HAllocate;
import syntaxtree.HLoadStmt;
import syntaxtree.HStoreStmt;
import syntaxtree.IntegerLiteral;
import syntaxtree.JumpStmt;
import syntaxtree.Label;
import syntaxtree.MoveStmt;
import syntaxtree.Node;
import syntaxtree.NodeToken;
import syntaxtree.PassArgStmt;
import syntaxtree.PrintStmt;
import syntaxtree.Reg;
import syntaxtree.SimpleExp;
import syntaxtree.StmtList;
import visitor.GJNoArguDepthFirst;

public final class MipsRoutineEmitter {
	private StringBuilder builder;
	
	private String functionName;
	
	private FunctionStackOffsets stackOffsets;
	private boolean returnsToCaller;
	
	private StmtList statements;
	
	public static String buildFunction(
			StmtList statements,
			String functionName,
			int kangaStackSize,
			int maxPassedArgs,
			boolean returnsToCaller){
		MipsRoutineEmitter emitter = new MipsRoutineEmitter(
				statements,
				functionName,
				kangaStackSize,
				maxPassedArgs,
				returnsToCaller);
		emitter.buildFunctionCode();
		return emitter.builder.toString();
	}
	
	private MipsRoutineEmitter(
			StmtList statements,
			String functionName,
			int kangaStackSize,
			int maxPassedArgs,
			boolean returnsToCaller){
		if (functionName == null || functionName.isEmpty()){
			throw new IllegalArgumentException();
		}
		
		this.functionName = functionName;
		this.statements = statements;
		
		this.builder = new StringBuilder();
		this.stackOffsets = new FunctionStackOffsets(kangaStackSize, maxPassedArgs);
		this.returnsToCaller = returnsToCaller;
	}
	
	private void buildFunctionCode(){
		emitLabel(functionName);
		
		int stackResizeAmount = stackOffsets.getStackFrameSize();
		
		emitStackMovement(-stackResizeAmount);
		emitSw(Register.RA, stackOffsets.returnAddressOffset(), Register.SP);
		
		new MipsVisitor().visit(statements);
		
		emitLw(Register.RA, stackOffsets.returnAddressOffset(), Register.SP);
		emitStackMovement(stackResizeAmount);
		
		if (returnsToCaller){
			emitJr(Register.RA);
		} else {
			IntegerImmediate exitSuccess = new IntegerImmediate(0);
			emitExitSyscall(exitSuccess);
		}
	}
	
	private void emitJr(Register reg) {
		emitInstruction("jr %s", reg);
	}

	private void emitLabel(String label){
		builder.append(String.format("%s:%n", label));
	}
	
	private void emitInstruction(String fmt, Object... args){
		String result = String.format(fmt, args);
		
		builder.append(String.format("\t%s%n", result));
	}
	
	private void emitLoadImmediate(Register dest, Immediate imm){
		if (imm instanceof LabelImmediate){
			emitInstruction("la %s, %s", dest, (LabelImmediate) imm);
		} else {
			emitInstruction("li %s, %s", dest, (IntegerImmediate) imm);
		}
	}
	
	private void emitMove(Register dest, RegImm src){
		if (src instanceof Immediate){
			emitLoadImmediate(dest, (Immediate) src);
		} else {
			Register regSrc = (Register) src;
			if (regSrc == dest){
				return;
			}
			
			emitInstruction("move %s, %s", dest, regSrc);
		}
	}
	
	private static boolean is16BitImmediate(RegImm expr){
		if (!IntegerImmediate.isIntegerImmediate(expr)){
			return false;
		}
		
		return is16BitImmediate((IntegerImmediate) expr);
	}
	
	private void emitStackMovement(int relativeOffset){
		if (relativeOffset == 0){
			return;
		}
		
		emitBinaryOperation(Register.SP,
				Register.SP,
				BinaryOperator.PLUS,
				new IntegerImmediate(relativeOffset));
	}
	
	private static boolean is16BitImmediate(IntegerImmediate imm){
		int value = imm.getValue();
		int min = (int) Short.MIN_VALUE;
		int max = (int) Short.MAX_VALUE;
		
		return min <= value	&& value <= max;
	}
	
	private void emitSyscall(){
		emitInstruction("syscall");
	}
	
	private void emitExitSyscall(IntegerImmediate errorCode){
		IntegerImmediate exitSyscall = new IntegerImmediate(17);
		
		emitLoadImmediate(Register.A0, errorCode);
		emitLoadImmediate(Register.V0, exitSyscall);
		emitSyscall();
	}
	
	private void emitPrint(RegImm printedValue){
		IntegerImmediate printIntSyscall = new IntegerImmediate(1);
		
		emitMove(Register.A0, printedValue);
		emitLoadImmediate(Register.V0, printIntSyscall);
		emitSyscall();
		
		// This comment mentally separates the two syscalls
		
		IntegerImmediate printCharSyscall = new IntegerImmediate(11);
		IntegerImmediate newLine = new IntegerImmediate((int) '\n');
		
		emitMove(Register.A0, newLine);
		emitLoadImmediate(Register.V0, printCharSyscall);
		emitSyscall();
	}
	
	private void emitJump(String label){
		emitInstruction("j %s", label);
	}
	
	private void emitJal(RegImm address){
		if (address instanceof Register){
			emitInstruction("jalr %s", (Register) address);
		} else {
			emitInstruction("jal %s", (Immediate) address);
		}
	}
	
	private void emitLw(Register dest, int offset, Register ptr){
		emitInstruction("lw %s, %d(%s)", dest, offset, ptr);
	}
	
	private void emitSw(Register src, int offset, Register ptr){
		emitInstruction("sw %s, %d(%s)", src, offset, ptr);
	}
	
	private void emitBeqz(Register cond, String label){
		emitInstruction("beqz %s, %s", cond, label);
	}
	
	private void emitMalloc(Register dest, RegImm size){
		IntegerImmediate mallocSyscall = new IntegerImmediate(9);
		
		emitMove(Register.A0, size);
		emitMove(Register.V0, mallocSyscall);
		emitSyscall();
		
		emitMove(dest, Register.V0);
	}
	
	private void emitBinaryOperation(Register dest,
			Register lhs,
			BinaryOperator oper,
			RegImm rhs){
			String instructionName = canUseImmediateForm(oper, rhs)
					? oper.getInstructionNameWithImmediate()
					: oper.getInstructionName();
					
			if (!is16BitImmediate(rhs) && !(rhs instanceof Register)){
					emitMove(Register.V1, rhs);
					rhs = Register.V1;
			}
			
			emitInstruction("%s %s, %s, %s", instructionName, dest, lhs, rhs);
	}
	
	private boolean canUseImmediateForm(BinaryOperator oper, RegImm rhs){
		if (oper.getInstructionNameWithImmediate() == null){
			return false;
		}
		
		if (!IntegerImmediate.isIntegerImmediate(rhs)){
			return false;
		}
		
		IntegerImmediate imm = (IntegerImmediate) rhs;
		if (!is16BitImmediate(imm)){
			return false;
		}
		
		return true;
	}
	
	private class MipsVisitor extends GJNoArguDepthFirst<Object> {
		public MipsVisitor(){}
		
		@Override
		public Object visit(Label label){
			emitLabel(label.f0.tokenImage);
			return null;
		}
		
		@Override
		public Object visit(ErrorStmt stmt){
			IntegerImmediate exitFailure = new IntegerImmediate(1);
			emitExitSyscall(exitFailure);
			
			return null;
		}
		
		@Override
		public Object visit(CJumpStmt stmt){
			Register cond = fromNode(stmt.f1);
			emitBeqz(cond, stmt.f2.f0.tokenImage);
			return null;
		};
		
		@Override
		public Object visit(JumpStmt stmt){
			emitJump(stmt.f1.f0.tokenImage);
			return null;
		}
		
		@Override
		public Object visit(HStoreStmt stmt){
			Register data = fromReg(stmt.f3);
			Register ptr = fromReg(stmt.f1);
			
			int offset = Integer.parseInt(stmt.f2.f0.tokenImage);
			
			emitSw(data, offset, ptr);
			return null;
		}
		
		@Override
		public Object visit(HLoadStmt stmt){
			Register dest = fromReg(stmt.f1);
			Register ptr = fromReg(stmt.f2);
			
			int offset = Integer.parseInt(stmt.f3.f0.tokenImage);
			
			emitLw(dest, offset, ptr);
			return null;
		}
		
		@Override
		public Object visit(MoveStmt stmt){
			Register dest = fromReg(stmt.f1);
			Node choice = stmt.f2.f0.choice;
			if (choice instanceof SimpleExp){
				RegImm src = fromSimpleExp( (SimpleExp) choice);
				emitMove(dest, src);
				return null;
			}
			
			if (choice instanceof HAllocate){
				RegImm size = fromSimpleExp( ((HAllocate) choice).f1);
				emitMalloc(dest, size);
				return null;
			}

			BinOp operation = (BinOp) choice;
			BinaryOperator oper = BinaryOperator.byName(operation.f0.f0.choice.toString());
			
			Register lhs = fromReg(operation.f1);
			RegImm rhs = fromSimpleExp(operation.f2);
			
			emitBinaryOperation(dest, lhs, oper, rhs);
			return null;
		}
		
		@Override
		public Object visit(PrintStmt stmt){
			emitPrint(fromSimpleExp(stmt.f1));
			return null;
		}

		@Override
		public Object visit(ALoadStmt stmt){
			Register dest = fromReg(stmt.f1);
			int spilledIdx = Integer.parseInt(stmt.f2.f1.f0.tokenImage);
			
			emitLw(dest, stackOffsets.offsetOfSpilled(spilledIdx), Register.SP);
			return null;
		}
		
		@Override
		public Object visit(AStoreStmt stmt){
			Register src = fromReg(stmt.f2);
			int spilledIdx = Integer.parseInt(stmt.f1.f1.f0.tokenImage);
			
			emitSw(src, stackOffsets.offsetOfSpilled(spilledIdx), Register.SP);
			return null;
		}
		
		@Override
		public Object visit(PassArgStmt stmt){
			Register src = fromReg(stmt.f2);
			int argIdx = Integer.parseInt(stmt.f1.f0.tokenImage);
			
			emitSw(src, stackOffsets.offsetOfPassed(argIdx), Register.SP);
			return null;
		}
		
		@Override
		public Object visit(CallStmt stmt){
			emitJal(fromSimpleExp(stmt.f1));
			return null;
		}
		
		private Register fromNode(Reg registerNode){
			return Register.byName(registerNode.f0.choice.toString());
		}
		
		private Register fromReg(Reg reg){
			NodeToken token = (NodeToken) reg.f0.choice;
			return Register.byName(token.tokenImage);
		}
		
		private LabelImmediate fromLabel(Label label){
			return new LabelImmediate(label.f0.tokenImage);
		}
		
		private IntegerImmediate fromIntegerConstant(IntegerLiteral literal){
			return new IntegerImmediate(Integer.parseInt(literal.f0.tokenImage));
		}
		
		private RegImm fromSimpleExp(SimpleExp expr){
			Node node = expr.f0.choice;
			
			if (node instanceof Reg){
				return fromReg((Reg) node);
			}
			
			if (node instanceof Label){
				return fromLabel((Label) node);
			}
			
			return fromIntegerConstant((IntegerLiteral) node);
		}
	}
}
