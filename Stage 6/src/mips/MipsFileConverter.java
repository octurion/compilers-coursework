package mips;

import syntaxtree.Goal;
import syntaxtree.Procedure;
import visitor.GJVoidDepthFirst;
import visitor.GJVoidVisitor;

public class MipsFileConverter {
	private MipsFileConverter(){}
	
	public static String kangaToMipsCode(Goal goal){
		if (goal == null ){
			throw new IllegalArgumentException();
		}
		
		StringBuilder fileBuilder = new StringBuilder();
		
		fileBuilder.append(".text\n");
		FUNCTION_VISITOR.visit(goal, fileBuilder);
		
		return fileBuilder.toString();
	}
	
	private static GJVoidVisitor<StringBuilder> FUNCTION_VISITOR =
			new GJVoidDepthFirst<StringBuilder>(){
		@Override
		public void visit(Goal goal, StringBuilder builder){
			String functionName = "MAIN";
			int kangaStackSize = Integer.parseInt(goal.f5.f0.tokenImage);
			int maxPassedArgs = Integer.parseInt(goal.f8.f0.tokenImage);
			
			String functionCode = MipsRoutineEmitter.buildFunction(
					goal.f10,
					functionName,
					kangaStackSize,
					maxPassedArgs,
					false);
			
			builder.append(functionCode).append('\n');
			
			visit(goal.f12, builder);
		}
		
		@Override
		public void visit(Procedure proc, StringBuilder builder){
			String functionName = proc.f0.f0.tokenImage;
			int kangaStackSize = Integer.parseInt(proc.f5.f0.tokenImage);
			int maxPassedArgs = Integer.parseInt(proc.f8.f0.tokenImage);
			
			String functionCode = MipsRoutineEmitter.buildFunction(
					proc.f10,
					functionName,
					kangaStackSize,
					maxPassedArgs,
					true);
			
			builder.append(functionCode).append('\n');			
		}
	};
}
