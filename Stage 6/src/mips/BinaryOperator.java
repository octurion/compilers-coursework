package mips;

import java.util.HashMap;
import java.util.Map;

public enum BinaryOperator {
	LT("LT", "slt", "slti"),
	PLUS("PLUS", "addu", "addiu"),
	MINUS("MINUS", "subu", null),
	TIMES("TIMES", "mul", null);
	
	private String kangaName;
	private String instructionName;
	private String instructionNameWithImmediate;
	
	public String getKangaName() {
		return kangaName;
	}

	public String getInstructionName() {
		return instructionName;
	}

	public String getInstructionNameWithImmediate() {
		return instructionNameWithImmediate;
	}

	private BinaryOperator(
			String kangaName,
			String instructionName,
			String instructionNameWithImmediate) {
		this.kangaName = kangaName;
		this.instructionName = instructionName;
		this.instructionNameWithImmediate = instructionNameWithImmediate;
	}
	
	private static final Map<String, BinaryOperator> KANGA_OPERATOR_MAPPING;
	static {
		KANGA_OPERATOR_MAPPING = new HashMap<String, BinaryOperator>();
		for (BinaryOperator op : BinaryOperator.values()){
			KANGA_OPERATOR_MAPPING.put(op.kangaName, op);
		}
	}
	
	public static BinaryOperator byName(String name){
		BinaryOperator result = KANGA_OPERATOR_MAPPING.get(name);
		if (result == null){
			throw new IllegalArgumentException();
		}
		
		return result;
	}	
}
