.text
MAIN:
	addiu $sp, $sp, -4
	sw $ra, 4($sp)
	li $a0, 16
	li $v0, 9
	syscall
	move $t0, $v0
	li $a0, 12
	li $v0, 9
	syscall
	move $t1, $v0
	la $t2, QS_Init
	sw $t2, 12($t0)
	la $t2, QS_Print
	sw $t2, 8($t0)
	la $t2, QS_Sort
	sw $t2, 4($t0)
	la $t2, QS_Start
	sw $t2, 0($t0)
	li $t2, 4
L0:
	li $t3, 12
	slt $t4, $t2, $t3
	beqz $t4, L1
	addu $t3, $t1, $t2
	li $t4, 0
	sw $t4, 0($t3)
	addiu $t2, $t2, 4
	j L0
L1:
	sw $t0, 0($t1)
	move $t0, $t1
	lw $t1, 0($t0)
	lw $t2, 0($t1)
	li $t1, 10
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	lw $ra, 4($sp)
	addiu $sp, $sp, 4
	li $a0, 0
	li $v0, 17
	syscall

QS_Start:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	move $a1, $s1
	jalr $t2
	move $t1, $v0
	move $t2, $t1
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t2, $t1
	li $t0, 9999
	move $a0, $t0
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	lw $t0, 8($s0)
	li $t1, 1
	subu $t2, $t0, $t1
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t3, 4($t1)
	li $t1, 0
	move $a0, $t0
	move $a1, $t1
	move $a2, $t2
	jalr $t3
	move $t4, $v0
	move $t2, $t4
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t2, $t1
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

QS_Sort:
	addiu $sp, $sp, -24
	sw $ra, 4($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
	li $t0, 0
	slt $t1, $s1, $s2
	beqz $t1, L2
	lw $t1, 4($s0)
	mul $t2, $s2, 4
	lw $t1, 4($s0)
	lw $t3, 0($t1)
	li $t4, 1
	slt $t5, $t2, $t3
	subu $t3, $t4, $t5
	beqz $t3, L4
	li $a0, 1
	li $v0, 17
	syscall
L4:
	li $t3, 4
	move $t4, $t3
	addu $t3, $t2, $t4
	move $t2, $t3
	addu $t3, $t1, $t2
	lw $t1, 0($t3)
	move $t2, $t1
	subu $s3, $s1, 1
	move $t1, $s2
	li $t3, 1
L5:
	beqz $t3, L6
	li $t4, 1
L7:
	beqz $t4, L8
	addiu $s3, $s3, 1
	lw $t5, 4($s0)
	mul $t6, $s3, 4
	lw $t5, 4($s0)
	lw $t7, 0($t5)
	li $t8, 1
	slt $t9, $t6, $t7
	subu $t7, $t8, $t9
	beqz $t7, L9
	li $a0, 1
	li $v0, 17
	syscall
L9:
	li $t7, 4
	move $t8, $t7
	addu $t7, $t6, $t8
	move $t6, $t7
	addu $t7, $t5, $t6
	lw $t5, 0($t7)
	move $t6, $t5
	li $t5, 1
	slt $t7, $t6, $t2
	subu $t8, $t5, $t7
	beqz $t8, L10
	li $t4, 0
	j L11
L10:
	li $t4, 1
L11:
	j L7
L8:
	li $t4, 1
L12:
	beqz $t4, L13
	subu $t1, $t1, 1
	lw $t5, 4($s0)
	mul $t7, $t1, 4
	lw $t5, 4($s0)
	lw $t8, 0($t5)
	li $t9, 1
	slt $s4, $t7, $t8
	subu $t8, $t9, $s4
	beqz $t8, L14
	li $a0, 1
	li $v0, 17
	syscall
L14:
	li $t8, 4
	move $t9, $t8
	addu $t8, $t7, $t9
	move $t7, $t8
	addu $t8, $t5, $t7
	lw $t5, 0($t8)
	move $t6, $t5
	li $t5, 1
	slt $t7, $t2, $t6
	subu $t6, $t5, $t7
	beqz $t6, L15
	li $t4, 0
	j L16
L15:
	li $t4, 1
L16:
	j L12
L13:
	lw $t4, 4($s0)
	mul $t5, $s3, 4
	lw $t4, 4($s0)
	lw $t6, 0($t4)
	li $t7, 1
	slt $t8, $t5, $t6
	subu $t6, $t7, $t8
	beqz $t6, L17
	li $a0, 1
	li $v0, 17
	syscall
L17:
	li $t6, 4
	move $t7, $t6
	addu $t6, $t5, $t7
	move $t5, $t6
	addu $t6, $t4, $t5
	lw $t4, 0($t6)
	move $t0, $t4
	li $t4, 1
	mul $t5, $t4, 4
	addu $t4, $s0, $t5
	lw $t6, 0($t4)
	mul $t4, $s3, 4
	li $t7, 1
	mul $t5, $t7, 4
	addu $t7, $s0, $t5
	lw $t6, 0($t7)
	lw $t5, 0($t6)
	li $t7, 1
	slt $t8, $t4, $t5
	subu $t5, $t7, $t8
	beqz $t5, L18
	li $a0, 1
	li $v0, 17
	syscall
L18:
	li $t5, 4
	move $t7, $t5
	addu $t5, $t4, $t7
	move $t4, $t5
	addu $t5, $t6, $t4
	lw $t4, 4($s0)
	mul $t6, $t1, 4
	lw $t4, 4($s0)
	lw $t7, 0($t4)
	li $t8, 1
	slt $t9, $t6, $t7
	subu $t7, $t8, $t9
	beqz $t7, L19
	li $a0, 1
	li $v0, 17
	syscall
L19:
	li $t7, 4
	move $t8, $t7
	addu $t7, $t6, $t8
	move $t6, $t7
	addu $t7, $t4, $t6
	lw $t4, 0($t7)
	sw $t4, 0($t5)
	li $t4, 1
	mul $t5, $t4, 4
	addu $t4, $s0, $t5
	lw $t6, 0($t4)
	mul $t4, $t1, 4
	li $t7, 1
	mul $t5, $t7, 4
	addu $t7, $s0, $t5
	lw $t6, 0($t7)
	lw $t5, 0($t6)
	li $t7, 1
	slt $t8, $t4, $t5
	subu $t5, $t7, $t8
	beqz $t5, L20
	li $a0, 1
	li $v0, 17
	syscall
L20:
	li $t5, 4
	move $t7, $t5
	addu $t5, $t4, $t7
	move $t4, $t5
	addu $t5, $t6, $t4
	sw $t0, 0($t5)
	li $t4, 1
	addu $t5, $s3, $t4
	slt $t4, $t1, $t5
	beqz $t4, L21
	li $t3, 0
	j L22
L21:
	li $t3, 1
L22:
	j L5
L6:
	li $t2, 1
	mul $t3, $t2, 4
	addu $t2, $s0, $t3
	lw $t4, 0($t2)
	mul $t2, $t1, 4
	li $t1, 1
	mul $t3, $t1, 4
	addu $t1, $s0, $t3
	lw $t4, 0($t1)
	lw $t1, 0($t4)
	li $t3, 1
	slt $t5, $t2, $t1
	subu $t1, $t3, $t5
	beqz $t1, L23
	li $a0, 1
	li $v0, 17
	syscall
L23:
	li $t1, 4
	move $t3, $t1
	addu $t1, $t2, $t3
	move $t2, $t1
	addu $t1, $t4, $t2
	lw $t2, 4($s0)
	mul $t3, $s3, 4
	lw $t2, 4($s0)
	lw $t4, 0($t2)
	li $t5, 1
	slt $t6, $t3, $t4
	subu $t4, $t5, $t6
	beqz $t4, L24
	li $a0, 1
	li $v0, 17
	syscall
L24:
	li $t4, 4
	move $t5, $t4
	addu $t4, $t3, $t5
	move $t3, $t4
	addu $t4, $t2, $t3
	lw $t2, 0($t4)
	sw $t2, 0($t1)
	li $t1, 1
	mul $t2, $t1, 4
	addu $t1, $s0, $t2
	lw $t3, 0($t1)
	mul $t1, $s3, 4
	li $t4, 1
	mul $t2, $t4, 4
	addu $t4, $s0, $t2
	lw $t3, 0($t4)
	lw $t2, 0($t3)
	li $t4, 1
	slt $t5, $t1, $t2
	subu $t2, $t4, $t5
	beqz $t2, L25
	li $a0, 1
	li $v0, 17
	syscall
L25:
	li $t2, 4
	move $t4, $t2
	addu $t2, $t1, $t4
	move $t1, $t2
	addu $t2, $t3, $t1
	lw $t1, 4($s0)
	mul $t3, $s2, 4
	lw $t1, 4($s0)
	lw $t4, 0($t1)
	li $t5, 1
	slt $t6, $t3, $t4
	subu $t4, $t5, $t6
	beqz $t4, L26
	li $a0, 1
	li $v0, 17
	syscall
L26:
	li $t4, 4
	move $t5, $t4
	addu $t4, $t3, $t5
	move $t3, $t4
	addu $t4, $t1, $t3
	lw $t1, 0($t4)
	sw $t1, 0($t2)
	li $t1, 1
	mul $t2, $t1, 4
	addu $t1, $s0, $t2
	lw $t3, 0($t1)
	mul $t1, $s2, 4
	li $t4, 1
	mul $t2, $t4, 4
	addu $t4, $s0, $t2
	lw $t3, 0($t4)
	lw $t2, 0($t3)
	li $t4, 1
	slt $t5, $t1, $t2
	subu $t2, $t4, $t5
	beqz $t2, L27
	li $a0, 1
	li $v0, 17
	syscall
L27:
	li $t2, 4
	move $t4, $t2
	addu $t2, $t1, $t4
	move $t1, $t2
	addu $t2, $t3, $t1
	sw $t0, 0($t2)
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 4($t1)
	li $t1, 1
	subu $t3, $s3, $t1
	move $a0, $t0
	move $a1, $s1
	move $a2, $t3
	jalr $t2
	move $t1, $v0
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 4($t1)
	li $t1, 1
	addu $t3, $s3, $t1
	move $a0, $t0
	move $a1, $t3
	move $a2, $s2
	jalr $t2
	move $t1, $v0
	j L3
L2:
L3:
	li $t0, 0
	move $v0, $t0
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 24
	jr $ra

QS_Print:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	li $t0, 0
L28:
	lw $t1, 8($s0)
	slt $t2, $t0, $t1
	beqz $t2, L29
	lw $t1, 4($s0)
	mul $t2, $t0, 4
	lw $t1, 4($s0)
	lw $t3, 0($t1)
	li $t4, 1
	slt $t5, $t2, $t3
	subu $t3, $t4, $t5
	beqz $t3, L30
	li $a0, 1
	li $v0, 17
	syscall
L30:
	li $t3, 4
	move $t4, $t3
	addu $t3, $t2, $t4
	move $t2, $t3
	addu $t3, $t1, $t2
	lw $t1, 0($t3)
	move $a0, $t1
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	addiu $t0, $t0, 1
	j L28
L29:
	li $t0, 0
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

QS_Init:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 8($s0)
	addiu $t0, $s1, 1
	li $t1, 4
	mul $t2, $t0, $t1
	move $a0, $t2
	li $v0, 9
	syscall
	move $t0, $v0
	li $t1, 4
L31:
	li $t2, 1
	addu $t3, $s1, $t2
	li $t2, 4
	move $t4, $t2
	mul $t2, $t3, $t4
	slt $t3, $t1, $t2
	beqz $t3, L32
	addu $t2, $t0, $t1
	li $t3, 0
	sw $t3, 0($t2)
	addiu $t1, $t1, 4
	j L31
L32:
	li $t1, 4
	mul $t2, $s1, $t1
	sw $t2, 0($t0)
	sw $t0, 4($s0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 0
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L33
	li $a0, 1
	li $v0, 17
	syscall
L33:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 20
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 1
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L34
	li $a0, 1
	li $v0, 17
	syscall
L34:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 7
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 2
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L35
	li $a0, 1
	li $v0, 17
	syscall
L35:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 12
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 3
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L36
	li $a0, 1
	li $v0, 17
	syscall
L36:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 18
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 4
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L37
	li $a0, 1
	li $v0, 17
	syscall
L37:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 2
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 5
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L38
	li $a0, 1
	li $v0, 17
	syscall
L38:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 11
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 6
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L39
	li $a0, 1
	li $v0, 17
	syscall
L39:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 6
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 7
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L40
	li $a0, 1
	li $v0, 17
	syscall
L40:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 9
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 8
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L41
	li $a0, 1
	li $v0, 17
	syscall
L41:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 19
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 9
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L42
	li $a0, 1
	li $v0, 17
	syscall
L42:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 5
	sw $t1, 0($t0)
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

