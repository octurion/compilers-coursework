.text
MAIN:
	addiu $sp, $sp, -4
	sw $ra, 4($sp)
	li $a0, 16
	li $v0, 9
	syscall
	move $t0, $v0
	li $a0, 12
	li $v0, 9
	syscall
	move $t1, $v0
	la $t2, LS_Init
	sw $t2, 12($t0)
	la $t2, LS_Search
	sw $t2, 8($t0)
	la $t2, LS_Print
	sw $t2, 4($t0)
	la $t2, LS_Start
	sw $t2, 0($t0)
	li $t2, 4
L0:
	li $t3, 12
	slt $t4, $t2, $t3
	beqz $t4, L1
	addu $t3, $t1, $t2
	li $t4, 0
	sw $t4, 0($t3)
	addiu $t2, $t2, 4
	j L0
L1:
	sw $t0, 0($t1)
	move $t0, $t1
	lw $t1, 0($t0)
	lw $t2, 0($t1)
	li $t1, 10
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	lw $ra, 4($sp)
	addiu $sp, $sp, 4
	li $a0, 0
	li $v0, 17
	syscall

LS_Start:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	move $a1, $s1
	jalr $t2
	move $t1, $v0
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 4($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	li $t0, 9999
	move $a0, $t0
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	li $t1, 8
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	li $t1, 12
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	li $t1, 17
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	li $t1, 50
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	li $t0, 55
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

LS_Print:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	li $t0, 1
L2:
	lw $t1, 8($s0)
	slt $t2, $t0, $t1
	beqz $t2, L3
	lw $t1, 4($s0)
	mul $t2, $t0, 4
	lw $t1, 4($s0)
	lw $t3, 0($t1)
	li $t4, 1
	slt $t5, $t2, $t3
	subu $t3, $t4, $t5
	beqz $t3, L4
	li $a0, 1
	li $v0, 17
	syscall
L4:
	li $t3, 4
	move $t4, $t3
	addu $t3, $t2, $t4
	move $t2, $t3
	addu $t3, $t1, $t2
	lw $t1, 0($t3)
	move $a0, $t1
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	addiu $t0, $t0, 1
	j L2
L3:
	li $t0, 0
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

LS_Search:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	li $t0, 1
	li $t1, 0
L5:
	lw $t2, 8($s0)
	slt $t3, $t0, $t2
	beqz $t3, L6
	lw $t2, 4($s0)
	mul $t3, $t0, 4
	lw $t2, 4($s0)
	lw $t4, 0($t2)
	li $t5, 1
	slt $t6, $t3, $t4
	subu $t4, $t5, $t6
	beqz $t4, L7
	li $a0, 1
	li $v0, 17
	syscall
L7:
	li $t4, 4
	move $t5, $t4
	addu $t4, $t3, $t5
	move $t3, $t4
	addu $t4, $t2, $t3
	lw $t2, 0($t4)
	move $t3, $t2
	addiu $t2, $s1, 1
	slt $t4, $t3, $s1
	beqz $t4, L8
	j L9
L8:
	li $t4, 1
	slt $t5, $t3, $t2
	subu $t2, $t4, $t5
	beqz $t2, L10
	j L11
L10:
	li $t1, 1
	lw $t2, 8($s0)
	move $t0, $t2
L11:
L9:
	addiu $t0, $t0, 1
	j L5
L6:
	move $v0, $t1
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

LS_Init:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 8($s0)
	addiu $t0, $s1, 1
	li $t1, 4
	mul $t2, $t0, $t1
	move $a0, $t2
	li $v0, 9
	syscall
	move $t0, $v0
	li $t1, 4
L12:
	li $t2, 1
	addu $t3, $s1, $t2
	li $t2, 4
	move $t4, $t2
	mul $t2, $t3, $t4
	slt $t3, $t1, $t2
	beqz $t3, L13
	addu $t2, $t0, $t1
	li $t3, 0
	sw $t3, 0($t2)
	addiu $t1, $t1, 4
	j L12
L13:
	li $t1, 4
	mul $t2, $s1, $t1
	sw $t2, 0($t0)
	sw $t0, 4($s0)
	li $t0, 1
	lw $t1, 8($s0)
	li $t2, 1
	addu $t3, $t1, $t2
L14:
	lw $t1, 8($s0)
	slt $t2, $t0, $t1
	beqz $t2, L15
	li $t1, 2
	mul $t2, $t1, $t0
	subu $t1, $t3, 3
	li $t4, 1
	mul $t5, $t4, 4
	addu $t4, $s0, $t5
	lw $t6, 0($t4)
	mul $t4, $t0, 4
	li $t7, 1
	mul $t5, $t7, 4
	addu $t7, $s0, $t5
	lw $t6, 0($t7)
	lw $t5, 0($t6)
	li $t7, 1
	slt $t8, $t4, $t5
	subu $t5, $t7, $t8
	beqz $t5, L16
	li $a0, 1
	li $v0, 17
	syscall
L16:
	li $t5, 4
	move $t7, $t5
	addu $t5, $t4, $t7
	move $t4, $t5
	addu $t5, $t6, $t4
	addu $t4, $t2, $t1
	sw $t4, 0($t5)
	addiu $t0, $t0, 1
	subu $t3, $t3, 1
	j L14
L15:
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

