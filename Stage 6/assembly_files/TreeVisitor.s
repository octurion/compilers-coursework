.text
MAIN:
	addiu $sp, $sp, -4
	sw $ra, 4($sp)
	li $a0, 4
	li $v0, 9
	syscall
	move $t0, $v0
	li $a0, 4
	li $v0, 9
	syscall
	move $t1, $v0
	la $t2, TV_Start
	sw $t2, 0($t0)
	sw $t0, 0($t1)
	move $t0, $t1
	lw $t1, 0($t0)
	lw $t2, 0($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $a0, $t1
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	lw $ra, 4($sp)
	addiu $sp, $sp, 4
	li $a0, 0
	li $v0, 17
	syscall

TV_Start:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	li $a0, 84
	li $v0, 9
	syscall
	move $t0, $v0
	li $a0, 28
	li $v0, 9
	syscall
	move $t1, $v0
	la $t2, Tree_accept
	sw $t2, 80($t0)
	la $t2, Tree_RecPrint
	sw $t2, 76($t0)
	la $t2, Tree_Print
	sw $t2, 72($t0)
	la $t2, Tree_Search
	sw $t2, 68($t0)
	la $t2, Tree_RemoveLeft
	sw $t2, 64($t0)
	la $t2, Tree_RemoveRight
	sw $t2, 60($t0)
	la $t2, Tree_Remove
	sw $t2, 56($t0)
	la $t2, Tree_Delete
	sw $t2, 52($t0)
	la $t2, Tree_Insert
	sw $t2, 48($t0)
	la $t2, Tree_Compare
	sw $t2, 44($t0)
	la $t2, Tree_SetHas_Right
	sw $t2, 40($t0)
	la $t2, Tree_SetHas_Left
	sw $t2, 36($t0)
	la $t2, Tree_GetHas_Left
	sw $t2, 32($t0)
	la $t2, Tree_GetHas_Right
	sw $t2, 28($t0)
	la $t2, Tree_SetKey
	sw $t2, 24($t0)
	la $t2, Tree_GetKey
	sw $t2, 20($t0)
	la $t2, Tree_GetLeft
	sw $t2, 16($t0)
	la $t2, Tree_GetRight
	sw $t2, 12($t0)
	la $t2, Tree_SetLeft
	sw $t2, 8($t0)
	la $t2, Tree_SetRight
	sw $t2, 4($t0)
	la $t2, Tree_Init
	sw $t2, 0($t0)
	li $t2, 4
L2:
	li $t3, 28
	slt $t4, $t2, $t3
	beqz $t4, L3
	addu $t3, $t1, $t2
	li $t4, 0
	sw $t4, 0($t3)
	addiu $t2, $t2, 4
	j L2
L3:
	sw $t0, 0($t1)
	move $s1, $t1
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 0($t1)
	li $t1, 16
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 72($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	li $t0, 100000000
	move $a0, $t0
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 48($t1)
	li $t1, 8
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 48($t1)
	li $t1, 24
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 48($t1)
	li $t1, 4
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 48($t1)
	li $t1, 12
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 48($t1)
	li $t1, 20
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 48($t1)
	li $t1, 28
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 48($t1)
	li $t1, 14
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 72($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	li $t0, 100000000
	move $a0, $t0
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	li $a0, 4
	li $v0, 9
	syscall
	move $t0, $v0
	li $a0, 12
	li $v0, 9
	syscall
	move $t1, $v0
	la $t2, MyVisitor_visit
	sw $t2, 0($t0)
	li $t2, 4
L4:
	li $t3, 12
	slt $t4, $t2, $t3
	beqz $t4, L5
	addu $t3, $t1, $t2
	li $t4, 0
	sw $t4, 0($t3)
	addiu $t2, $t2, 4
	j L4
L5:
	sw $t0, 0($t1)
	move $t0, $t1
	li $t1, 50000000
	move $a0, $t1
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t1, $s1
	lw $t2, 0($t1)
	lw $t3, 80($t2)
	move $a0, $t1
	move $a1, $t0
	jalr $t3
	move $t2, $v0
	li $t0, 100000000
	move $a0, $t0
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 68($t1)
	li $t1, 24
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 68($t1)
	li $t1, 12
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 68($t1)
	li $t1, 16
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 68($t1)
	li $t1, 50
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 68($t1)
	li $t1, 12
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 52($t1)
	li $t1, 12
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 72($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 68($t1)
	li $t1, 12
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Tree_Init:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 12($s0)
	li $t0, 0
	sw $t0, 16($s0)
	li $t0, 0
	sw $t0, 20($s0)
	li $t0, 1
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Tree_SetRight:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 8($s0)
	li $t0, 1
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Tree_SetLeft:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 4($s0)
	li $t0, 1
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Tree_GetRight:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	lw $t0, 8($s0)
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

Tree_GetLeft:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	lw $t0, 4($s0)
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

Tree_GetKey:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	lw $t0, 12($s0)
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

Tree_SetKey:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 12($s0)
	li $t0, 1
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Tree_GetHas_Right:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	lw $t0, 20($s0)
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

Tree_GetHas_Left:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	lw $t0, 16($s0)
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

Tree_SetHas_Left:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 16($s0)
	li $t0, 1
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Tree_SetHas_Right:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 20($s0)
	li $t0, 1
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Tree_Compare:
	addiu $sp, $sp, -16
	sw $ra, 4($sp)
	sw $s0, 16($sp)
	sw $s1, 12($sp)
	sw $s2, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
	li $t1, 0
	addiu $t0, $s2, 1
	slt $t1, $s1, $s2
	beqz $t1, L6
	li $t1, 0
	j L7
L6:
	li $t2, 1
	slt $t3, $s1, $t0
	subu $t0, $t2, $t3
	beqz $t0, L8
	li $t1, 0
	j L9
L8:
	li $t1, 1
L9:
L7:
	move $v0, $t1
	lw $s0, 16($sp)
	lw $s1, 12($sp)
	lw $s2, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 16
	jr $ra

Tree_Insert:
	addiu $sp, $sp, -24
	sw $ra, 4($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	move $s0, $a0
	move $s1, $a1
	li $a0, 84
	li $v0, 9
	syscall
	move $t0, $v0
	li $a0, 28
	li $v0, 9
	syscall
	move $t1, $v0
	la $t2, Tree_accept
	sw $t2, 80($t0)
	la $t2, Tree_RecPrint
	sw $t2, 76($t0)
	la $t2, Tree_Print
	sw $t2, 72($t0)
	la $t2, Tree_Search
	sw $t2, 68($t0)
	la $t2, Tree_RemoveLeft
	sw $t2, 64($t0)
	la $t2, Tree_RemoveRight
	sw $t2, 60($t0)
	la $t2, Tree_Remove
	sw $t2, 56($t0)
	la $t2, Tree_Delete
	sw $t2, 52($t0)
	la $t2, Tree_Insert
	sw $t2, 48($t0)
	la $t2, Tree_Compare
	sw $t2, 44($t0)
	la $t2, Tree_SetHas_Right
	sw $t2, 40($t0)
	la $t2, Tree_SetHas_Left
	sw $t2, 36($t0)
	la $t2, Tree_GetHas_Left
	sw $t2, 32($t0)
	la $t2, Tree_GetHas_Right
	sw $t2, 28($t0)
	la $t2, Tree_SetKey
	sw $t2, 24($t0)
	la $t2, Tree_GetKey
	sw $t2, 20($t0)
	la $t2, Tree_GetLeft
	sw $t2, 16($t0)
	la $t2, Tree_GetRight
	sw $t2, 12($t0)
	la $t2, Tree_SetLeft
	sw $t2, 8($t0)
	la $t2, Tree_SetRight
	sw $t2, 4($t0)
	la $t2, Tree_Init
	sw $t2, 0($t0)
	li $t2, 4
L10:
	li $t3, 28
	slt $t4, $t2, $t3
	beqz $t4, L11
	addu $t3, $t1, $t2
	li $t4, 0
	sw $t4, 0($t3)
	addiu $t2, $t2, 4
	j L10
L11:
	sw $t0, 0($t1)
	move $s2, $t1
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 0($t1)
	move $a0, $t0
	move $a1, $s1
	jalr $t2
	move $t1, $v0
	move $s3, $s0
	li $s4, 1
L12:
	beqz $s4, L13
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t0, $t1
	slt $t1, $s1, $t0
	beqz $t1, L14
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L16
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s3, $t1
	j L17
L16:
	li $s4, 0
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 36($t1)
	li $t1, 1
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	move $a0, $t0
	move $a1, $s2
	jalr $t2
	move $t1, $v0
L17:
	j L15
L14:
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L18
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s3, $t1
	j L19
L18:
	li $s4, 0
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 40($t1)
	li $t1, 1
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s3
	lw $t1, 0($t0)
	lw $t2, 4($t1)
	move $a0, $t0
	move $a1, $s2
	jalr $t2
	move $t1, $v0
L19:
L15:
	j L12
L13:
	li $t0, 1
	move $v0, $t0
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 24
	jr $ra

Tree_Delete:
	addiu $sp, $sp, -44
	sw $ra, 4($sp)
	sw $s0, 44($sp)
	sw $s1, 40($sp)
	sw $s2, 36($sp)
	sw $s3, 32($sp)
	sw $s4, 28($sp)
	sw $s6, 20($sp)
	sw $s7, 16($sp)
	sw $s5, 12($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $s0
	move $s3, $s0
	li $s4, 1
	li $v1, 0
	sw $v1, 24($sp)
	li $s6, 1
L20:
	beqz $s4, L21
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s7, $t1
	slt $t0, $s1, $s7
	beqz $t0, L22
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L24
	move $s3, $s2
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s2, $t1
	j L25
L24:
	li $s4, 0
L25:
	j L23
L22:
	slt $t0, $s7, $s1
	beqz $t0, L26
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L28
	move $s3, $s2
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s2, $t1
	j L29
L28:
	li $s4, 0
L29:
	j L27
L26:
	beqz $s6, L30
	li $s7, 0
	li $s5, 1
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	subu $t0, $s5, $t1
	beqz $t0, L34
	li $s5, 1
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	subu $t0, $s5, $t1
	beqz $t0, L34
	li $s7, 1
L34:
	beqz $s7, L32
	j L33
L32:
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 56($t1)
	move $a0, $t0
	move $a1, $s3
	move $a2, $s2
	jalr $t2
	move $t1, $v0
L33:
	j L31
L30:
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 56($t1)
	move $a0, $t0
	move $a1, $s3
	move $a2, $s2
	jalr $t2
	move $t1, $v0
L31:
	lw $v1, 24($sp)
	li $v1, 1
	sw $v1, 8($sp)
	li $s4, 0
L27:
L23:
	li $s6, 0
	j L20
L21:
	lw $v1, 8($sp)
	move $v0, $v1
	lw $s0, 44($sp)
	lw $s1, 40($sp)
	lw $s2, 36($sp)
	lw $s3, 32($sp)
	lw $s4, 28($sp)
	lw $s5, 12($sp)
	lw $s6, 20($sp)
	lw $s7, 16($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 44
	jr $ra

Tree_Remove:
	addiu $sp, $sp, -20
	sw $ra, 4($sp)
	sw $s0, 20($sp)
	sw $s1, 16($sp)
	sw $s2, 12($sp)
	sw $s3, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L35
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 64($t1)
	move $a0, $t0
	move $a1, $s1
	move $a2, $s2
	jalr $t2
	move $t1, $v0
	j L36
L35:
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L37
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 60($t1)
	move $a0, $t0
	move $a1, $s1
	move $a2, $s2
	jalr $t2
	move $t1, $v0
	j L38
L37:
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s3, $t1
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t0, $t1
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t0, $t1
	move $t1, $s0
	lw $t2, 0($t1)
	lw $t3, 44($t2)
	move $a0, $t1
	move $a1, $s3
	move $a2, $t0
	jalr $t3
	move $t2, $v0
	beqz $t2, L39
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	lw $t1, 24($s0)
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 36($t1)
	li $t1, 0
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	j L40
L39:
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 4($t1)
	lw $t1, 24($s0)
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 40($t1)
	li $t1, 0
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
L40:
L38:
L36:
	li $t0, 1
	move $v0, $t0
	lw $s0, 20($sp)
	lw $s1, 16($sp)
	lw $s2, 12($sp)
	lw $s3, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 20
	jr $ra

Tree_RemoveRight:
	addiu $sp, $sp, -24
	sw $ra, 4($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
L41:
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L42
	move $s3, $s2
	lw $t0, 0($s3)
	lw $s4, 24($t0)
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t0, $t1
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $a0, $s3
	move $a1, $t1
	jalr $s4
	move $t0, $v0
	move $s1, $s2
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s2, $t1
	j L41
L42:
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 4($t1)
	lw $t1, 24($s0)
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 40($t1)
	li $t1, 0
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	li $t0, 1
	move $v0, $t0
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 24
	jr $ra

Tree_RemoveLeft:
	addiu $sp, $sp, -24
	sw $ra, 4($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $a2
L43:
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L44
	move $s3, $s2
	lw $t0, 0($s3)
	lw $s4, 24($t0)
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t0, $t1
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $a0, $s3
	move $a1, $t1
	jalr $s4
	move $t0, $v0
	move $s1, $s2
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s2, $t1
	j L43
L44:
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	lw $t1, 24($s0)
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 36($t1)
	li $t1, 0
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	li $t0, 1
	move $v0, $t0
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 24
	jr $ra

Tree_Search:
	addiu $sp, $sp, -28
	sw $ra, 4($sp)
	sw $s0, 28($sp)
	sw $s1, 24($sp)
	sw $s2, 20($sp)
	sw $s3, 16($sp)
	sw $s4, 12($sp)
	sw $s5, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $s2, $s0
	li $s3, 1
	li $s4, 0
L45:
	beqz $s3, L46
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s5, $t1
	slt $t0, $s1, $s5
	beqz $t0, L47
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L49
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s2, $t1
	j L50
L49:
	li $s3, 0
L50:
	j L48
L47:
	slt $t0, $s5, $s1
	beqz $t0, L51
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L53
	move $t0, $s2
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $s2, $t1
	j L54
L53:
	li $s3, 0
L54:
	j L52
L51:
	li $s4, 1
	li $s3, 0
L52:
L48:
	j L45
L46:
	move $v0, $s4
	lw $s0, 28($sp)
	lw $s1, 24($sp)
	lw $s2, 20($sp)
	lw $s3, 16($sp)
	lw $s4, 12($sp)
	lw $s5, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 28
	jr $ra

Tree_Print:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	move $t0, $s0
	move $t1, $s0
	lw $t2, 0($t1)
	lw $t3, 76($t2)
	move $a0, $t1
	move $a1, $t0
	jalr $t3
	move $t2, $v0
	li $t0, 1
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

Tree_RecPrint:
	addiu $sp, $sp, -20
	sw $ra, 4($sp)
	sw $s0, 20($sp)
	sw $s1, 16($sp)
	sw $s2, 12($sp)
	sw $s3, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L55
	move $s2, $s0
	lw $t0, 0($s2)
	lw $s3, 76($t0)
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $a0, $s2
	move $a1, $t1
	jalr $s3
	move $t0, $v0
	j L56
L55:
L56:
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $a0, $t1
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L57
	move $s2, $s0
	lw $t0, 0($s2)
	lw $s3, 76($t0)
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $a0, $s2
	move $a1, $t1
	jalr $s3
	move $t0, $v0
	j L58
L57:
L58:
	li $t0, 1
	move $v0, $t0
	lw $s0, 20($sp)
	lw $s1, 16($sp)
	lw $s2, 12($sp)
	lw $s3, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 20
	jr $ra

Tree_accept:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	li $t0, 333
	move $a0, $t0
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 0($t1)
	move $a0, $t0
	move $a1, $s0
	jalr $t2
	move $t1, $v0
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

Visitor_visit:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L59
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	sw $t1, 8($s0)
	lw $t0, 8($s0)
	move $t1, $t0
	lw $t0, 0($t1)
	lw $t2, 80($t0)
	move $a0, $t1
	move $a1, $s0
	jalr $t2
	move $t0, $v0
	j L60
L59:
L60:
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L61
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	sw $t1, 4($s0)
	lw $t0, 4($s0)
	move $t1, $t0
	lw $t0, 0($t1)
	lw $t2, 80($t0)
	move $a0, $t1
	move $a1, $s0
	jalr $t2
	move $t0, $v0
	j L62
L61:
L62:
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

MyVisitor_visit:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 28($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L63
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	sw $t1, 8($s0)
	lw $t0, 8($s0)
	move $t1, $t0
	lw $t0, 0($t1)
	lw $t2, 80($t0)
	move $a0, $t1
	move $a1, $s0
	jalr $t2
	move $t0, $v0
	j L64
L63:
L64:
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 20($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $a0, $t1
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 32($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	beqz $t1, L65
	move $t0, $s1
	lw $t1, 0($t0)
	lw $t2, 16($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	sw $t1, 4($s0)
	lw $t0, 4($s0)
	move $t1, $t0
	lw $t0, 0($t1)
	lw $t2, 80($t0)
	move $a0, $t1
	move $a1, $s0
	jalr $t2
	move $t0, $v0
	j L66
L65:
L66:
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

