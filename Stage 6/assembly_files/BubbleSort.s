.text
MAIN:
	addiu $sp, $sp, -4
	sw $ra, 4($sp)
	li $a0, 16
	li $v0, 9
	syscall
	move $t0, $v0
	li $a0, 12
	li $v0, 9
	syscall
	move $t1, $v0
	la $t2, BBS_Init
	sw $t2, 12($t0)
	la $t2, BBS_Print
	sw $t2, 8($t0)
	la $t2, BBS_Sort
	sw $t2, 4($t0)
	la $t2, BBS_Start
	sw $t2, 0($t0)
	li $t2, 4
L0:
	li $t3, 12
	slt $t4, $t2, $t3
	beqz $t4, L1
	addu $t3, $t1, $t2
	li $t4, 0
	sw $t4, 0($t3)
	addiu $t2, $t2, 4
	j L0
L1:
	sw $t0, 0($t1)
	move $t0, $t1
	lw $t1, 0($t0)
	lw $t2, 0($t1)
	li $t1, 10
	move $a0, $t0
	move $a1, $t1
	jalr $t2
	move $t3, $v0
	move $a0, $t3
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	lw $ra, 4($sp)
	addiu $sp, $sp, 4
	li $a0, 0
	li $v0, 17
	syscall

BBS_Start:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 12($t1)
	move $a0, $t0
	move $a1, $s1
	jalr $t2
	move $t1, $v0
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	li $t0, 99999
	move $a0, $t0
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 4($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	move $t0, $s0
	lw $t1, 0($t0)
	lw $t2, 8($t1)
	move $a0, $t0
	jalr $t2
	move $t1, $v0
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

BBS_Sort:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	lw $t0, 8($s0)
	li $t1, 1
	subu $t2, $t0, $t1
	li $t0, 0
	subu $t1, $t0, 1
L2:
	slt $t0, $t1, $t2
	beqz $t0, L3
	li $t0, 1
L4:
	li $t3, 1
	addu $t4, $t2, $t3
	slt $t3, $t0, $t4
	beqz $t3, L5
	subu $t3, $t0, 1
	lw $t4, 4($s0)
	mul $t5, $t3, 4
	lw $t4, 4($s0)
	lw $t3, 0($t4)
	li $t6, 1
	slt $t7, $t5, $t3
	subu $t3, $t6, $t7
	beqz $t3, L6
	li $a0, 1
	li $v0, 17
	syscall
L6:
	li $t3, 4
	move $t6, $t3
	addu $t3, $t5, $t6
	move $t5, $t3
	addu $t3, $t4, $t5
	lw $t4, 0($t3)
	move $t3, $t4
	lw $t4, 4($s0)
	mul $t5, $t0, 4
	lw $t4, 4($s0)
	lw $t6, 0($t4)
	li $t7, 1
	slt $t8, $t5, $t6
	subu $t6, $t7, $t8
	beqz $t6, L7
	li $a0, 1
	li $v0, 17
	syscall
L7:
	li $t6, 4
	move $t7, $t6
	addu $t6, $t5, $t7
	move $t5, $t6
	addu $t6, $t4, $t5
	lw $t4, 0($t6)
	move $t5, $t4
	slt $t4, $t5, $t3
	beqz $t4, L8
	subu $t3, $t0, 1
	lw $t4, 4($s0)
	mul $t5, $t3, 4
	lw $t4, 4($s0)
	lw $t6, 0($t4)
	li $t7, 1
	slt $t8, $t5, $t6
	subu $t6, $t7, $t8
	beqz $t6, L10
	li $a0, 1
	li $v0, 17
	syscall
L10:
	li $t6, 4
	move $t7, $t6
	addu $t6, $t5, $t7
	move $t5, $t6
	addu $t6, $t4, $t5
	lw $t4, 0($t6)
	move $t5, $t4
	li $t4, 1
	mul $t6, $t4, 4
	addu $t4, $s0, $t6
	lw $t7, 0($t4)
	mul $t4, $t3, 4
	li $t3, 1
	mul $t6, $t3, 4
	addu $t3, $s0, $t6
	lw $t7, 0($t3)
	lw $t3, 0($t7)
	li $t6, 1
	slt $t8, $t4, $t3
	subu $t3, $t6, $t8
	beqz $t3, L11
	li $a0, 1
	li $v0, 17
	syscall
L11:
	li $t3, 4
	move $t6, $t3
	addu $t3, $t4, $t6
	move $t4, $t3
	addu $t3, $t7, $t4
	lw $t4, 4($s0)
	mul $t6, $t0, 4
	lw $t4, 4($s0)
	lw $t7, 0($t4)
	li $t8, 1
	slt $t9, $t6, $t7
	subu $t7, $t8, $t9
	beqz $t7, L12
	li $a0, 1
	li $v0, 17
	syscall
L12:
	li $t7, 4
	move $t8, $t7
	addu $t7, $t6, $t8
	move $t6, $t7
	addu $t7, $t4, $t6
	lw $t4, 0($t7)
	sw $t4, 0($t3)
	li $t3, 1
	mul $t4, $t3, 4
	addu $t3, $s0, $t4
	lw $t6, 0($t3)
	mul $t3, $t0, 4
	li $t7, 1
	mul $t4, $t7, 4
	addu $t7, $s0, $t4
	lw $t6, 0($t7)
	lw $t4, 0($t6)
	li $t7, 1
	slt $t8, $t3, $t4
	subu $t4, $t7, $t8
	beqz $t4, L13
	li $a0, 1
	li $v0, 17
	syscall
L13:
	li $t4, 4
	move $t7, $t4
	addu $t4, $t3, $t7
	move $t3, $t4
	addu $t4, $t6, $t3
	sw $t5, 0($t4)
	j L9
L8:
L9:
	addiu $t0, $t0, 1
	j L4
L5:
	subu $t2, $t2, 1
	j L2
L3:
	li $t0, 0
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

BBS_Print:
	addiu $sp, $sp, -8
	sw $ra, 4($sp)
	sw $s0, 8($sp)
	move $s0, $a0
	li $t0, 0
L14:
	lw $t1, 8($s0)
	slt $t2, $t0, $t1
	beqz $t2, L15
	lw $t1, 4($s0)
	mul $t2, $t0, 4
	lw $t1, 4($s0)
	lw $t3, 0($t1)
	li $t4, 1
	slt $t5, $t2, $t3
	subu $t3, $t4, $t5
	beqz $t3, L16
	li $a0, 1
	li $v0, 17
	syscall
L16:
	li $t3, 4
	move $t4, $t3
	addu $t3, $t2, $t4
	move $t2, $t3
	addu $t3, $t1, $t2
	lw $t1, 0($t3)
	move $a0, $t1
	li $v0, 1
	syscall
	li $a0, 10
	li $v0, 11
	syscall
	addiu $t0, $t0, 1
	j L14
L15:
	li $t0, 0
	move $v0, $t0
	lw $s0, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 8
	jr $ra

BBS_Init:
	addiu $sp, $sp, -12
	sw $ra, 4($sp)
	sw $s0, 12($sp)
	sw $s1, 8($sp)
	move $s0, $a0
	move $s1, $a1
	sw $s1, 8($s0)
	addiu $t0, $s1, 1
	li $t1, 4
	mul $t2, $t0, $t1
	move $a0, $t2
	li $v0, 9
	syscall
	move $t0, $v0
	li $t1, 4
L17:
	li $t2, 1
	addu $t3, $s1, $t2
	li $t2, 4
	move $t4, $t2
	mul $t2, $t3, $t4
	slt $t3, $t1, $t2
	beqz $t3, L18
	addu $t2, $t0, $t1
	li $t3, 0
	sw $t3, 0($t2)
	addiu $t1, $t1, 4
	j L17
L18:
	li $t1, 4
	mul $t2, $s1, $t1
	sw $t2, 0($t0)
	sw $t0, 4($s0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 0
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L19
	li $a0, 1
	li $v0, 17
	syscall
L19:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 20
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 1
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L20
	li $a0, 1
	li $v0, 17
	syscall
L20:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 7
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 2
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L21
	li $a0, 1
	li $v0, 17
	syscall
L21:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 12
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 3
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L22
	li $a0, 1
	li $v0, 17
	syscall
L22:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 18
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 4
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L23
	li $a0, 1
	li $v0, 17
	syscall
L23:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 2
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 5
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L24
	li $a0, 1
	li $v0, 17
	syscall
L24:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 11
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 6
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L25
	li $a0, 1
	li $v0, 17
	syscall
L25:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 6
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 7
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L26
	li $a0, 1
	li $v0, 17
	syscall
L26:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 9
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 8
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L27
	li $a0, 1
	li $v0, 17
	syscall
L27:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 19
	sw $t1, 0($t0)
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	li $t0, 9
	mul $t3, $t0, 4
	li $t0, 1
	mul $t1, $t0, 4
	addu $t0, $s0, $t1
	lw $t2, 0($t0)
	lw $t0, 0($t2)
	li $t1, 1
	slt $t4, $t3, $t0
	subu $t0, $t1, $t4
	beqz $t0, L28
	li $a0, 1
	li $v0, 17
	syscall
L28:
	li $t0, 4
	move $t1, $t0
	addu $t0, $t3, $t1
	move $t1, $t0
	addu $t0, $t2, $t1
	li $t1, 5
	sw $t1, 0($t0)
	li $t0, 0
	move $v0, $t0
	lw $s0, 12($sp)
	lw $s1, 8($sp)
	lw $ra, 4($sp)
	addiu $sp, $sp, 12
	jr $ra

