Compilers - Project 6
---------------------

Alexandros Tasos (sdi1100085)

Compilation instructions:
1) Unzip & descend into the src directory
2) Run the following command:
   $ find -name "*.java" -print | xargs javac

Alternatively, you can create a new project in Eclipse and add the
source files.

Running instructions:
1) While inside the src directory, run:
   $ java Main /path/to/kanga/files/*.kg

If you are using Eclipse, you will have to set the command line arguments
by going to Run > Run Configurations... > Arguments.
If for some reason or another the Kanga files cannot be found by Eclipse,
just cd into the Kanga folder and run the following, then copy & paste
the output:
$ find `pwd` -name "*.kg" | sort

Each *.s file generated will be located in the same directory its
corresponding Kanga source file lives in.

Notice
------

The MIPS files generated have been tested and are known to print the
correct output, character by character, on the MIPS MARS 4.4 simulator
(http://courses.missouristate.edu/KenVollmar/MARS/). The generated assembly
code will most likely work with the latest version of QtSpim, however, no
such guarantee is made.
