Compilers - Project 5
---------------------

Alexandros Tasos (sdi1100085)

Compilation instructions:
1) Unzip & descend into the src directory
2) Run the following command:
   $ find -name "*.java" -print | xargs javac

Alternatively, you can create a new project in Eclipse and add the
source files.

Running instructions:
1) While inside the src directory, run:
   $ java Main /path/to/spiglet/files/*.spg

If you are using Eclipse, you will have to set the command line arguments
by going to Run > Run Configurations... > Arguments.
If for some reason or another the Spiglet files cannot be found by Eclipse,
just cd into the Piglet folder and run the following, then copy & paste
the output:
$ find `pwd` -name "*.spg" | sort

Each *.kg file generated will be located in the same directory its
corresponding Piglet source file lives in.

Project Structure
-----------------
- Package ir contains all the necessary classes to represent a Spiglet
  function into basic blocks.
  Class ir.BasicBlocksConverter converts
  a Spiglet function parse tree into a basic block graph.
- Package kanga contains all the necessary functions to convert a Spiglet
  function into its Kanga equivalent, provided that we have performed the
  liveness analysis pass and applied the register allocation algorithm
  on the graph.
  Package kanga.StackLayout provides the stack layout for
  each function. The ordering of arguments, locals etc. is given in the
  source code comments of the file.
- Package reg_alloc contains two important classes: BlockLivenessAnalyzer,
  which performs the liveness analysis pass and constructs the live-in
  and live-out sets for each statement and for each block.
  The liveness analysis algorithm used is the one we have been taught in
  class.
  Package reg.LinearScanAllocator implements the register allocation
  algorithm. The algorithm follows the original linear scan register
  allocation algorithm we have been taught in class. The only difference
  compared to our algorithm is that we attempt to assign as many
  alive-between-function-calls variables to sX registers as possible and
  as many dead-between-function-calls variables to tX registers as possible.
