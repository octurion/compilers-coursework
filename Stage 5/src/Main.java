import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import kanga.CodeEmitter;
import syntaxtree.Goal;

public class Main {
	public static void main(String[] args) {
		String spigletExtension = ".spg";
		String kangaExtension = ".kg";
		
		for (String filename : args) {
			if (!filename.endsWith(spigletExtension)){
				System.out.printf(
					"Skipping file %s because it does not have a %s extension%n",
					filename, spigletExtension);
				continue;
			}
			String outFilename = replaceSuffix(filename, spigletExtension, kangaExtension);
			System.out.printf("Now parsing file %s...%n", filename);
			parseFile(filename, outFilename);
		}
	}
	
	private static void parseFile(String filename, String outFilename){
		InputStream in = null;
		FileWriter out = null;
		try {
			in = new FileInputStream(filename);
			out = new FileWriter(outFilename);
			
			Goal goal = new SpigletParser(in).Goal();
			out.write(CodeEmitter.spigletToKangaCode(goal));

			System.out.println("File parsed successfully!");
		}
		catch (IOException e) {
			System.err.printf("An I/O error occurred while parsing file \"%s\": %s%n",
				filename, e.getMessage());
		}
		catch (ParseException e){
			System.err.printf("Parse error while parsing file \"%s\": %s%n",
					filename, e.getMessage());
		}
		finally {
			try {
				if (in != null) in.close();
			}
			catch(IOException e) {
				System.err.println(e.getMessage());
			}
			
			try {
				if (out != null) out.close();
			}
			catch(IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
	private static String replaceSuffix(String target, String suffix, String replacement) {
	    if (!target.endsWith(suffix)) {
	        return target;
	    }

	    String prefix = target.substring(0, target.length() - suffix.length());
	    return prefix + replacement;
	}
}
