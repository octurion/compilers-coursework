package ir;

import ir.BasicBlock.ConditionalJump;
import ir.statements.BinaryOperation;
import ir.statements.ErrorStatement;
import ir.statements.FunctionCall;
import ir.statements.MemoryAllocate;
import ir.statements.MemoryLoad;
import ir.statements.MemoryStore;
import ir.statements.PrintStatement;
import ir.statements.ReturnStatement;
import ir.statements.UnaryAssignment;

/**
 * A visitor used to traverse the statements of a basic block. To use a visitor,
 * you only have to pass your visitor object into the statement's
 * {@link BlockStatement#accept} method.
 */
public interface StatementVisitor {
	public void visit(ConditionalJump jump);
	public void visit(ErrorStatement stmt);
	public void visit(MemoryStore store);
	public void visit(MemoryLoad load);
	public void visit(PrintStatement stmt);
	public void visit(ReturnStatement stmt);

	public void visit(BinaryOperation oper);
	public void visit(FunctionCall call);
	public void visit(MemoryAllocate alloc);
	public void visit(UnaryAssignment assignment);
}