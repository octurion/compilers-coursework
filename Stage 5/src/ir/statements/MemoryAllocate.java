package ir.statements;

import java.util.Collections;
import java.util.Set;

import ir.StatementVisitor;
import ir.definitions.Assignment;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;

public final class MemoryAllocate implements Assignment {
	private Variable ptr;
	private SimpleExpression size;
	
	private Set<Variable> usedVariables;
	
	public MemoryAllocate(Variable ptr, SimpleExpression size){
		if (ptr == null || size == null){
			throw new IllegalArgumentException();
		}
		
		this.ptr = ptr;
		this.size = size;
		
		setUsedVariablesSet();
	}
	
	@Override
	public Variable getAssignedVariable() {
		return ptr;
	}

	public SimpleExpression getSizeExpression(){
		return size;
	}

	@Override
	public Set<Variable> getUsedVariables() {
		return usedVariables;
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
	
	private void setUsedVariablesSet(){
		usedVariables = Variable.isVariable(size)
				? Collections.singleton((Variable) size)
				: Collections.<Variable>emptySet();
	}
	
	@Override
	public String toString(){
		return String.format("MOVE %s HALLOCATE %s", ptr, size);
	}
}