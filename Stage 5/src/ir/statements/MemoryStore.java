package ir.statements;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ir.StatementVisitor;
import ir.definitions.BlockStatement;
import ir.definitions.Variable;

public final class MemoryStore implements BlockStatement {
	private Variable ptr;
	private Variable storedData;
	
	private int offset;

	private Set<Variable> usedVariables;
	
	public Variable getPointer() {
		return ptr;
	}

	public Variable getStoredData() {
		return storedData;
	}

	public int getOffset() {
		return offset;
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
	
	public MemoryStore(Variable ptr, int offset, Variable data){
		if (ptr == null || data == null || offset < 0){
			throw new IllegalArgumentException();
		}
		
		this.ptr = ptr;
		this.storedData = data;
		this.offset = offset;
		
		makeUsedVariablesSet();
	}
	
	@Override
	public Set<Variable> getUsedVariables() {
		return usedVariables;
	}
	
	private void makeUsedVariablesSet(){
		Set<Variable> vars = new HashSet<Variable>(Arrays.asList(ptr, storedData));
		
		usedVariables = Collections.unmodifiableSet(vars);
	}
	
	@Override
	public String toString(){
		return String.format("HSTORE %s %d %s", ptr, offset, storedData);
	}
}