package ir.statements;

import java.util.Collections;
import java.util.Set;

import ir.StatementVisitor;
import ir.definitions.BlockStatement;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;

public final class PrintStatement implements BlockStatement {
	private SimpleExpression expressionPrinted;
	private Set<Variable> usedVariables;
	
	public PrintStatement(SimpleExpression expressionPrinted){
		if (expressionPrinted == null){
			throw new NullPointerException();
		}
		
		this.expressionPrinted = expressionPrinted;
		
		makeUsedVariablesSet();
	}
	
	public SimpleExpression getExpressionPrinted(){
		return expressionPrinted;
	}

	@Override
	public Set<Variable> getUsedVariables() {
		return usedVariables;
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
	
	private void makeUsedVariablesSet(){
		usedVariables = Variable.isVariable(expressionPrinted)
				? Collections.singleton((Variable) expressionPrinted)
				: Collections.<Variable>emptySet();
	}
	
	@Override
	public String toString(){
		return String.format("PRINT %s", expressionPrinted);
	}
}
