package ir.statements;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ir.StatementVisitor;
import ir.definitions.Assignment;
import ir.definitions.BinaryOperator;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;

/**
 * Represents an assignment where the result of a binary operation
 * (add, subtract, multiply, less than) is assigned to a variable.
 */
public final class BinaryOperation implements Assignment {
	private Variable dest;
	
	private Variable lhs;
	private BinaryOperator operator;
	private SimpleExpression rhs;
	
	private Set<Variable> usedVariables;
	
	public BinaryOperation(Variable dest,
			Variable lhs,
			BinaryOperator operator,
			SimpleExpression rhs){
		if (dest == null || lhs == null || operator == null || rhs == null){
			throw new IllegalArgumentException();
		}
		
		this.dest = dest;
		this.lhs = lhs;
		this.operator = operator;
		this.rhs = rhs;
		
		makeUsedVariablesSet();
	}
	
	@Override
	public Variable getAssignedVariable() {
		return dest;
	}
	
	@Override public Set<Variable> getUsedVariables(){
		return usedVariables;
	}
	
	public Variable getLHS(){
		return lhs;
	}

	public SimpleExpression getRHS(){
		return rhs;
	}
	
	public BinaryOperator getOperator(){
		return operator;
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
	
	private void makeUsedVariablesSet(){
		Set<Variable> variables = new HashSet<Variable>(2);
		variables.add(lhs);
		
		if (Variable.isVariable(rhs)){
			variables.add((Variable) rhs);
		}
		
		usedVariables = Collections.unmodifiableSet(variables);
	}

	@Override
	public String toString(){
		return String.format("MOVE %s %s %s %s", dest, operator, lhs, rhs);
	}
}
