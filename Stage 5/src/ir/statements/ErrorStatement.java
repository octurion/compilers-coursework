package ir.statements;

import java.util.Collections;
import java.util.Set;

import syntaxtree.ErrorStmt;

import ir.StatementVisitor;
import ir.definitions.BlockStatement;
import ir.definitions.Variable;

public final class ErrorStatement implements BlockStatement {
	private ErrorStatement(){}
	
	public static final ErrorStatement ERROR_STATEMENT = new ErrorStatement();

	@Override
	public Set<Variable> getUsedVariables() {
		return Collections.emptySet();
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public String toString(){
		return "ERROR";
	}
	
	public static ErrorStatement fromErrorStmt(ErrorStmt unused){
		return ERROR_STATEMENT;
	}
}
