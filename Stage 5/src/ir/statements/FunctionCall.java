package ir.statements;

import ir.StatementVisitor;
import ir.definitions.Assignment;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class FunctionCall implements Assignment {
	private Variable dest;
	
	private SimpleExpression function;
	private List<Variable> parameters;
	
	private Set<Variable> usedVariables;
	
	@Override
	public Variable getAssignedVariable() {
		return dest;
	}
	
	public FunctionCall(Variable dest, SimpleExpression function, List<Variable> parameters){
		if (dest == null || function == null || parameters == null){
			throw new IllegalArgumentException();
		}
		
		this.dest = dest;
		this.function = function;
		this.parameters = new ArrayList<Variable>(parameters);
		
		makeUsedVariablesSet();
	}
	
	public SimpleExpression getFunction(){
		return function;
	}
	
	@Override
	public Set<Variable> getUsedVariables() {
		return usedVariables;
	}
	
	private void makeUsedVariablesSet(){
		Set<Variable> variables = new HashSet<Variable>(parameters);
		
		if (Variable.isVariable(function)) {
			variables.add((Variable) function);
		}
		
		usedVariables = Collections.unmodifiableSet(variables);
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		Iterator<Variable> iter = parameters.iterator();
		if (iter.hasNext()){
			builder.append(iter.next());
		}
		while(iter.hasNext()){
			builder.append(' ').append(iter.next());
		}
		
		return String.format("MOVE %s CALL %s (%s)", dest, function, builder);
	}

	public List<Variable> getParameters() {
		return Collections.unmodifiableList(parameters);
	}	
}
