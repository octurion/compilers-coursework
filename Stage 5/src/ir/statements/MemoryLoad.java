package ir.statements;

import java.util.Collections;
import java.util.Set;

import ir.StatementVisitor;
import ir.definitions.Assignment;
import ir.definitions.Variable;

public final class MemoryLoad implements Assignment {
	private Variable dest;

	private Variable ptr;
	private int offset;
	
	private Set<Variable> usedVariables;
	
	public MemoryLoad(Variable dest, Variable ptr, int offset){
		if (dest == null || ptr == null || offset < 0){
			throw new IllegalArgumentException();
		}
		
		this.dest = dest;
		this.ptr = ptr;
		this.offset = offset;
		
		makeUsedVariablesSet();
	}
	
	public int getOffset(){
		return offset;
	}
	
	public Variable getPointer(){
		return ptr;
	}

	@Override
	public Set<Variable> getUsedVariables() {
		return usedVariables;
	}

	@Override
	public Variable getAssignedVariable() {
		return dest;
	}
	
	private void makeUsedVariablesSet(){
		usedVariables = Collections.singleton(ptr);
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public String toString(){
		return String.format("HLOAD %s %s %d", dest, ptr, offset);
	}
}
