package ir.statements;

import java.util.Collections;
import java.util.Set;

import ir.StatementVisitor;
import ir.definitions.BlockStatement;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;

public final class ReturnStatement implements BlockStatement {
	private SimpleExpression returnExpr;
	private Set<Variable> usedVariables;
	
	public ReturnStatement(SimpleExpression returnExpr){
		if (returnExpr == null){
			throw new IllegalArgumentException();
		}
		
		this.returnExpr = returnExpr;
		makeUsedVariablesSet();
	}
	
	public SimpleExpression getReturnExpression(){
		return returnExpr;
	}

	@Override
	public Set<Variable> getUsedVariables() {
		return usedVariables;
	}
	
	private void makeUsedVariablesSet(){
		usedVariables = Variable.isVariable(returnExpr)
				? Collections.singleton((Variable) returnExpr)
				: Collections.<Variable>emptySet();
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public String toString(){
		return String.format("RETURN %s", returnExpr);
	}
}