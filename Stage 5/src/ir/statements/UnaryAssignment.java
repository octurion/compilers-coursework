package ir.statements;

import java.util.Collections;
import java.util.Set;

import ir.StatementVisitor;
import ir.definitions.Assignment;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;

/**
 * Represents an assignment where the source can be either a variable or a
 * constant.
 */
public final class UnaryAssignment implements Assignment {
	private Variable dest;
	private SimpleExpression src;
	
	private Set<Variable> usedVariables;

	public UnaryAssignment(Variable dest, SimpleExpression src){
		this.dest = dest;
		this.src = src;
		
		makeUsedVariablesSet();
	}
	
	public SimpleExpression getAssignmentExpression(){
		return src;
	}

	@Override
	public Variable getAssignedVariable() {
		return dest;
	}

	@Override
	public Set<Variable> getUsedVariables() {
		return usedVariables;
	}
	
	@Override
	public void accept(StatementVisitor visitor) {
		visitor.visit(this);
	}

	private void makeUsedVariablesSet() {
		usedVariables = Variable.isVariable(src)
				? Collections.singleton((Variable) src)
				: Collections.<Variable>emptySet();
	}
	
	@Override
	public String toString(){
		return String.format("MOVE %s %s", dest, src);
	}
}