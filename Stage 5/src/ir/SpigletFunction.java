package ir;

import ir.definitions.BlockStatement;
import ir.statements.FunctionCall;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Programmatic representation of any Spiglet function (including its number of
 * arguments, basic blocks, etc).
 */
public final class SpigletFunction {
	private String name;

	private int numParameters;
	private BasicBlock entryPoint;
	private List<BasicBlock> blocks;
	
	public SpigletFunction(String name, int numParameters,
			BasicBlock entryPoint, List<BasicBlock> blockList) {
		this.name = name;
		this.numParameters = numParameters;
		this.entryPoint = entryPoint;
		
		blocks = new ArrayList<BasicBlock>(blockList);
	}

	public String getName() {
		return name;
	}

	public int getNumParameters() {
		return numParameters;
	}
	
	public List<BasicBlock> getBlocks(){
		return Collections.unmodifiableList(blocks);
	}

	public BasicBlock getEntryPoint() {
		return entryPoint;
	}
	
	/**
	 * Calculates the maximum number of arguments that have to be passed to a
	 * function which this function calls.
	 */
	public int maxNumParametersPassedToCallees(){
		int maxParams = 0;
		for (BasicBlock block : blocks){
			for (BlockStatement stmt : block.getStatements()){
				if (!(stmt instanceof FunctionCall)){
					continue;
				}
				
				FunctionCall call = (FunctionCall) stmt;
				maxParams = Math.max(call.getParameters().size(), maxParams);
			}
		}
		
		return maxParams;
	}
}