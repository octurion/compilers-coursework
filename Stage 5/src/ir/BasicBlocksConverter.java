package ir;

import ir.definitions.Assignment;
import ir.definitions.BinaryOperator;
import ir.definitions.BlockStatement;
import ir.definitions.IntegerConstant;
import ir.definitions.LabelConstant;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;
import ir.statements.BinaryOperation;
import ir.statements.ErrorStatement;
import ir.statements.FunctionCall;
import ir.statements.MemoryAllocate;
import ir.statements.MemoryLoad;
import ir.statements.MemoryStore;
import ir.statements.PrintStatement;
import ir.statements.ReturnStatement;
import ir.statements.UnaryAssignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import syntaxtree.*;

import visitor.*;

/**
 * A class that converts a Spiglet function parse tree into a graph of basic
 * blocks.
 */
public final class BasicBlocksConverter {
	private static class BlockGraphBuilder {
		private Map<String, BasicBlock> labeledBlocks;
		private List<BasicBlock> blockList;
		
		private BasicBlock initialBlock;
		private BasicBlock currentBlock;
		
		public BlockGraphBuilder(Set<String> blockLabels){
			initialBlock = new BasicBlock();
			currentBlock = initialBlock;
			blockList = new ArrayList<BasicBlock>();
			
			initLabeledBlocks(blockLabels);
		}
		
		private void initLabeledBlocks(Set<String> blockLabels){
			labeledBlocks = new HashMap<String, BasicBlock>();
			for (String label: blockLabels){
				labeledBlocks.put(label, new BasicBlock());
			}
		}

		public void addStatement(BlockStatement stmt){
			currentBlock.addStatement(stmt);
		}
		
		public void emitJumpToLabel(String labelName){
			BasicBlock labelBlock = labeledBlocks.get(labelName);
			assert(labelBlock != null);
			
			currentBlock.setSuccessor(labelBlock);
			blockList.add(currentBlock);
			currentBlock = new BasicBlock();
		}
		
		public void emitConditionalJumpToLabel(String labelName,
				Variable conditionVariable){
			BasicBlock labelBlock = labeledBlocks.get(labelName);
			assert(labelBlock != null);

			currentBlock.putConditionalJump(conditionVariable, labelBlock);
			
			BasicBlock nextBlock = new BasicBlock();
			currentBlock.setSuccessor(nextBlock);
			blockList.add(currentBlock);
			
			currentBlock = nextBlock;
		}
		
		public void switchToLabeledBlock(String labelName){
			BasicBlock labeledBlock = labeledBlocks.get(labelName);
			assert(labeledBlock != null);
			currentBlock.setSuccessor(labeledBlock);
			blockList.add(currentBlock);
			
			currentBlock = labeledBlock;
		}
		
		public void finalizeBlockGraph(){
			blockList.add(currentBlock);
			currentBlock = null;
		}
		
		public BasicBlock getBlockGraphHead(){
			return initialBlock;
		}
	}
	
	/**
	 * Converts any other Spiglet function other than main() into a graph of
	 * basic blocks.
	 */
	public static SpigletFunction spigletFunctionToBlockGraph(Procedure func){
		int numArguments = Integer.parseInt(func.f2.f0.tokenImage);
		return makeSpigletFunction(numArguments,
				func.f0.f0.tokenImage,
				func.f4);
	}
	
	/**
	 * Converts the main function into a graph of basic blocks.
	 */
	public static SpigletFunction spigletMainToBlockGraph(StmtList mainFunc){
		return makeSpigletFunction(0, "MAIN", mainFunc);
	}
	
	private static SpigletFunction makeSpigletFunction(
			int numArguments,
			String functionName,
			Node functionStatements){
		BlockGraphBuilder builder = new BlockGraphBuilder(
				extractControlFlowLabels(functionStatements));
		
		functionStatements.accept(BASIC_BLOCK_CONSTRUCTION_VISITOR, builder);
		builder.finalizeBlockGraph();
		return new SpigletFunction(functionName, numArguments, builder.getBlockGraphHead(),
				builder.blockList);
	}

	private static Set<String> extractControlFlowLabels(Node functionNode) {
		Set<String> labels = new HashSet<String>();
		functionNode.accept(LABEL_VISITOR, labels);
		return labels;
	}

	/**
	 * A visitor that finds all labels that affect control flow and adds them
	 * to a set
	 */
	private static final GJVoidVisitor<Set<String>> LABEL_VISITOR =
			new GJVoidDepthFirst<Set<String>>(){
		public void visit(Label label, Set<String> labels){
			labels.add(label.f0.tokenImage);
		}
		
		public void visit(CJumpStmt stmt, Set<String> labels){
			visit(stmt.f2, labels);
		}
		
		public void visit(JumpStmt stmt, Set<String> labels){
			visit(stmt.f1, labels);
		}
		
		// All these statements will only have labels to other functions
		// (or are doing some dodgy business, like calling labels inside the
		// function), ignore them
		public void visit(HStoreStmt stmt, Set<String> labels){}
		public void visit(HLoadStmt stmt, Set<String> labels){}
		public void visit(MoveStmt stmt, Set<String> labels){}
		public void visit(PrintStmt stmt, Set<String> labels){}
		public void visit(SimpleExp stmt, Set<String> labels){}
	};
	
	/**
	 * A visitor that constructs the basic blocks from the Spiglet parse tree
	 */
	private static final GJVoidVisitor<BlockGraphBuilder> BASIC_BLOCK_CONSTRUCTION_VISITOR =
			new GJVoidDepthFirst<BlockGraphBuilder>(){
		@Override
		public void visit(Label label, BlockGraphBuilder builder){
			builder.switchToLabeledBlock(label.f0.tokenImage);
		}
		
		@Override
		public void visit(CJumpStmt stmt, BlockGraphBuilder builder){
			builder.emitConditionalJumpToLabel(stmt.f2.f0.tokenImage,
					Variable.fromTemp(stmt.f1));
		}
		
		@Override
		public void visit(JumpStmt stmt, BlockGraphBuilder builder){
			builder.emitJumpToLabel(stmt.f1.f0.tokenImage);
		}
		
		@Override
		public void visit(HStoreStmt stmt, BlockGraphBuilder builder){
			builder.addStatement(new MemoryStore(
					Variable.fromTemp(stmt.f1), 
					Integer.parseInt(stmt.f2.f0.tokenImage),
					Variable.fromTemp(stmt.f3)
				));
		}
		
		@Override
		public void visit(ErrorStmt stmt, BlockGraphBuilder builder){
			builder.addStatement(ErrorStatement.fromErrorStmt(stmt));
		}
		
		@Override
		public void visit(HLoadStmt stmt, BlockGraphBuilder builder){
			builder.addStatement(new MemoryLoad(
					Variable.fromTemp(stmt.f1), 
					Variable.fromTemp(stmt.f2),
					Integer.parseInt(stmt.f3.f0.tokenImage)
				));
		}
		
		@Override
		public void visit(StmtExp expr, BlockGraphBuilder builder){
			visit(expr.f1, builder);
			
			builder.addStatement(new ReturnStatement(
					expr.f3.accept(SIMPLE_EXPRESSION_VISITOR)
				));
		}
		
		@Override
		public void visit(MoveStmt stmt, BlockGraphBuilder builder){
			GJVisitor<Assignment, Variable> ASSIGNMENT_VISITOR =
					new GJDepthFirst<Assignment, Variable>(){
				@Override
				public Assignment visit(BinOp expr, Variable dest){
					BinaryOperator operator = BinaryOperator.valueOf
							(expr.f0.f0.choice.toString());
					
					return new BinaryOperation(dest,
							Variable.fromTemp(expr.f1),
							operator,
							SIMPLE_EXPRESSION_VISITOR.visit(expr.f2));
				}
				
				@Override
				public Assignment visit(SimpleExp expr, Variable dest){
					return new UnaryAssignment(dest,
							SIMPLE_EXPRESSION_VISITOR.visit(expr));
				}
				
				@Override
				public Assignment visit(HAllocate expr, Variable dest){
					return new MemoryAllocate(dest,
							SIMPLE_EXPRESSION_VISITOR.visit(expr.f1));
				}
				
				@Override
				public Assignment visit(Call expr, Variable dest){
					List<Variable> parameters = new ArrayList<Variable>();
					for (Node n: Collections.list(expr.f3.elements())){
						parameters.add(Variable.fromTemp((Temp) n));
					}
					
					return new FunctionCall(dest,
							expr.f1.accept(SIMPLE_EXPRESSION_VISITOR),
							parameters);
				}
			};
			
			builder.addStatement(ASSIGNMENT_VISITOR.visit(stmt.f2,
					Variable.fromTemp(stmt.f1)));
		}
		
		@Override
		public void visit(PrintStmt stmt, BlockGraphBuilder builder){
			builder.addStatement(new PrintStatement(
					stmt.f1.accept(SIMPLE_EXPRESSION_VISITOR)
				));
		}
		
		@Override
		public void visit(NoOpStmt stmt, BlockGraphBuilder builder){}
	};
	
	private static final GJNoArguVisitor<SimpleExpression> SIMPLE_EXPRESSION_VISITOR =
			new GJNoArguDepthFirst<SimpleExpression>(){
		
		@Override
		public SimpleExpression visit(Temp temp){
			return Variable.fromTemp(temp);
		}
		
		@Override
		public SimpleExpression visit(IntegerLiteral literal){
			return new IntegerConstant(Integer.parseInt(literal.f0.tokenImage));
		}

		@Override
		public SimpleExpression visit(Label labelName){
			return new LabelConstant(labelName.f0.tokenImage);
		}
	};
}