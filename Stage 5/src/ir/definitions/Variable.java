package ir.definitions;

import syntaxtree.Temp;

/**
 * A class that represents a Spiglet variable
 */
public final class Variable implements SimpleExpression, Comparable<Variable> {
	int variableID;

	/**
	 * @return The ID of the variable
	 */
	public int getVariableID(){
		return variableID;
	}

	/**
	 * Construct a new variable with the specified ID
	 * @param variableID
	 */
	public Variable(int variableID) {
		if (variableID < 0){
			throw new IllegalArgumentException();
		}
		
		this.variableID = variableID;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof Variable)){
			return false;
		}
		
		Variable other = (Variable) o;
		return variableID == other.variableID;
	}
	
	@Override
	public int hashCode(){
		return variableID;
	}
	
	public static boolean isVariable(SimpleExpression expr){
		return (expr instanceof Variable);
	}
	
	@Override
	public String toString(){
		return String.format("TEMP %d", variableID);
	}
	
	
	public static Variable fromTemp(Temp tempVar){
		return new Variable(Integer.parseInt(tempVar.f1.f0.tokenImage));
	}

	@Override
	public int compareTo(Variable other) {
		if (other == null){
			throw new NullPointerException();
		}
		
		return Integer.compare(variableID, other.variableID);
	}
}