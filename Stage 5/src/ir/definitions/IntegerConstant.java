package ir.definitions;

public final class IntegerConstant implements SimpleExpression {
	int value;
	
	public IntegerConstant(int value){
		if (value < 0){
			throw new IllegalArgumentException();
		}
		
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof IntegerConstant)){
			return false;
		}
		
		IntegerConstant other = (IntegerConstant) o;
		return this.value == other.value;
	}
	
	@Override
	public int hashCode(){
		return value;
	}
	
	public static boolean isIntegerConstant(SimpleExpression expr){
		return (expr instanceof IntegerConstant);
	}
	
	@Override
	public String toString(){
		return Integer.toString(value);
	}
}
