package ir.definitions;

/**
 * Represents a variable or a constant. All instances of {@link Variable},
 * {@link IntegerConstant}, {@link LabelConstant} implement this interface.
 */
public interface SimpleExpression {}
