package ir.definitions;

public final class LabelConstant implements SimpleExpression {
	private String labelName;

	public String getLabelName() {
		return labelName;
	}
	
	public LabelConstant(String labelName){
		if (labelName == null || labelName.isEmpty()){
			throw new IllegalArgumentException();
		}
		
		this.labelName = labelName;
	}

	@Override
	public int hashCode() {
		return labelName.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof LabelConstant)) {
			return false;
		}
		
		LabelConstant other = (LabelConstant) obj;
		return labelName.equals(other.labelName);
	}
	
	@Override
	public String toString(){
		return labelName;
	}
}
