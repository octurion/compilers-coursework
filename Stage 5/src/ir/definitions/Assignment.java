package ir.definitions;

/**
 * An interface implemented by all statements that assign to exactly one variable
 */
public interface Assignment extends BlockStatement {
	public Variable getAssignedVariable();
}
