package ir.definitions;

/**
 * An enum that represents the 4 possible Spiglet binary operators
 */
public enum BinaryOperator {
	LT("LT", "<"), PLUS("PLUS", "+"), MINUS("MINUS", "-"), TIMES("TIMES", "*");
	
	private String operatorName;
	private String operatorSymbol;
	
	public String getOperatorName() {
		return operatorName;
	}

	public String getOperatorSymbol() {
		return operatorSymbol;
	}

	private BinaryOperator(String operatorName, String operatorSymbol){
		this.operatorName = operatorName;
		this.operatorSymbol = operatorSymbol;
	}
	
	@Override
	public String toString(){
		return operatorName;
	}
}