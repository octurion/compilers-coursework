package ir.definitions;

import ir.StatementVisitor;

import java.util.Set;

public interface BlockStatement {
	/**
	 * @return The variables used (or accessed) by this statement
	 */
	public Set<Variable> getUsedVariables();
	
	/**
	 * Function to be called in order to use a visitor
	 */
	public void accept(StatementVisitor visitor);
}