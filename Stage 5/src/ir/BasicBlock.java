package ir;

import ir.definitions.BlockStatement;
import ir.definitions.Variable;
import ir.statements.ErrorStatement;
import ir.statements.ReturnStatement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A representation of a basic block
 */
public final class BasicBlock {
	/**
	 * A class that represents the conditional variable which affects the
	 * execution path, as well as the "other" successor of the basic block.
	 * 
	 * The conditional jump, unlike the unconditional one, is treated as a
	 * statement, which makes a lot of algorithms much easier to simplify.
	 */
	public static final class ConditionalJump implements BlockStatement {
		private Variable cond;
		private BasicBlock successor;
		private Set<Variable> usedVariables;
		
		public ConditionalJump(Variable cond, BasicBlock successor){
			if(cond == null || successor == null){
				throw new IllegalArgumentException();
			}
			
			this.cond = cond;
			this.successor = successor;
			
			this.usedVariables = Collections.singleton(cond);
		}

		public Variable getConditionVariable() {
			return cond;
		}

		public BasicBlock getConditionalSuccessor() {
			return successor;
		}

		@Override
		public Set<Variable> getUsedVariables() {
			return usedVariables;
		}
		
		@Override
		public String toString(){
			return String.format("CJUMP %s", cond);
		}

		@Override
		public void accept(StatementVisitor visitor) {
			visitor.visit(this);
		}
	}

	private List<BlockStatement> statements;
	
	private BasicBlock successor;
	private ConditionalJump condJump;

	public List<BlockStatement> getStatements() {
		List<BlockStatement> statementsWithCondJump =
				new ArrayList<BlockStatement>(statements);
		if (condJump != null){
			statementsWithCondJump.add(condJump);
		}
		
		return Collections.unmodifiableList(statementsWithCondJump);
	}

	public ConditionalJump getCondJump() {
		return condJump;
	}
	
	BasicBlock(){
		statements = new ArrayList<BlockStatement>();
		
		successor = null;
		condJump = null;
	}
	
	public BasicBlock getNonconditionalSuccessor() {
		return successor;
	}
	
	public Set<BasicBlock> getSuccessors(){
		if (successor == null){
			return Collections.<BasicBlock>emptySet(); 
		}
		
		if (condJump == null){
			return Collections.<BasicBlock>singleton(successor);
		}
		
		return new HashSet<BasicBlock>(Arrays.asList(successor, condJump.successor));
	}
	
	private static boolean isEndOfFunction(BlockStatement stmt){
		return stmt instanceof ErrorStatement || stmt instanceof ReturnStatement;
	}
	
	private boolean isLastStatementEndOfFunction(){
		return !statements.isEmpty() && isEndOfFunction(statements.get(statements.size() - 1));
	}
	
	void setSuccessor(BasicBlock successor){
		if (successor == null){
			throw new IllegalArgumentException();
		}
		
		if (this.successor != null){
			throw new IllegalStateException();
		}
		
		if (isLastStatementEndOfFunction()){
			return;
		}
		
		this.successor = successor;
	}
	
	void putConditionalJump(Variable conditionalVariable,
			BasicBlock conditionalSuccessorBlock){
		if (conditionalVariable == null || conditionalSuccessorBlock == null){
			throw new IllegalArgumentException();
		}
		
		if (condJump != null){
			throw new IllegalStateException();
		}
		
		if (isLastStatementEndOfFunction()){
			return;
		}
		
		this.condJump = new ConditionalJump(conditionalVariable,
				conditionalSuccessorBlock);
	}

	void addStatement(BlockStatement statement){
		if (statement == null){
			throw new IllegalArgumentException();
		}
		
		if (isLastStatementEndOfFunction()){
			return;
		}
		
		statements.add(statement);
	}
	
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		
		for (BlockStatement statement: getStatements()){
			builder.append(statement).append('\n');
		}
		
		return builder.toString();
	}
}