package reg_alloc;

import ir.BasicBlock;
import ir.definitions.Assignment;
import ir.definitions.BlockStatement;
import ir.definitions.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * Represents the live-in and live-out sets of all statements of a
 * {@link BasicBlock}, along with the live-in and live-out sets of the block
 * itself.
 */
public final class LivenessData {
	/**
	 * Represents the live-in set of variables of a statement, along with the
	 * statement itself.
	 */
	public static class LiveInData {
		private Set<Variable> liveInStmt;
		private BlockStatement stmt;
		
		public Set<Variable> getLiveInStmt() {
			return Collections.unmodifiableSet(liveInStmt);
		}

		public BlockStatement getStmt() {
			return stmt;
		}

		public LiveInData(BlockStatement stmt) {
			liveInStmt = new HashSet<Variable>();
			this.stmt = stmt;
		}
		
		@Override
		public String toString(){
			return String.format("%s => %s%n", stmt, liveInStmt);
		}
	}
	
	private Set<Variable> liveOutBlock;
	private List<LiveInData> liveInStatements;
	
	public LivenessData(BasicBlock block) {
		liveInStatements = new ArrayList<LiveInData>(block.getStatements().size());
		liveOutBlock = new HashSet<Variable>();
		
		for (BlockStatement stmt : block.getStatements()){
			liveInStatements.add(new LiveInData(stmt));
		}
	}

	public Set<Variable> getLiveOutBlock(){
		return Collections.unmodifiableSet(liveOutBlock);
	}
	
	public List<LiveInData> getLiveInPerStatement(){
		return Collections.unmodifiableList(liveInStatements);
	}
	
	public Set<Variable> getLiveInAtStatement(int idx){
		if (idx > liveInStatements.size()){
			throw new IllegalArgumentException();
		}
		
		if (idx == liveInStatements.size()){
			return liveOutBlock;
		}
		
		return liveInStatements.get(idx).liveInStmt;
	}
	
	public Set<Variable> getLiveOutAtStatement(int idx){
		if (idx > liveInStatements.size() - 1){
			throw new IllegalArgumentException();
		}
		
		if (idx == liveInStatements.size() - 1){
			return liveOutBlock;
		}
		
		return liveInStatements.get(idx + 1).liveInStmt;
	}
	
	public Set<Variable> getLiveInBlock(){
		if (liveInStatements.isEmpty()){
			return Collections.unmodifiableSet(liveOutBlock);
		}
		
		return Collections.unmodifiableSet(liveInStatements.get(0).liveInStmt);
	}
	
	/**
	 * @return Performs the "propagate upward" operation and returns
	 * {@code true} if the live-in set changed during the operation
	 */
	boolean propagateLivenessUpward(){
		boolean changed = false;
		ListIterator<LiveInData> iter =
				liveInStatements.listIterator(liveInStatements.size());
		Set<Variable> liveOut = liveOutBlock;
		while(iter.hasPrevious()){
			LiveInData data = iter.previous();
			Set<Variable> newLiveIn = new HashSet<Variable>(liveOut);
			
			if (data.stmt instanceof Assignment){
				Assignment assgn = (Assignment) data.stmt;
				newLiveIn.remove(assgn.getAssignedVariable());
			}
			newLiveIn.addAll(data.stmt.getUsedVariables());
			
			if (!data.liveInStmt.equals(newLiveIn)){
				changed = true;
			}

			liveOut = newLiveIn;
			data.liveInStmt = newLiveIn;
		}
		
		return changed;
	}
	
	boolean successorsUnion(List<Set<Variable>> successorsLiveIn){
		Set<Variable> newLiveOut = new HashSet<Variable>();
		for (Set<Variable> successorLiveIn : successorsLiveIn){
			newLiveOut.addAll(successorLiveIn);
		}
		
		boolean changed = !liveOutBlock.equals(newLiveOut);
		liveOutBlock = newLiveOut;
		
		return changed;
	}
	
	@Override
	public String toString(){
		return liveInStatements.toString() + liveOutBlock.toString();
	}
}