package reg_alloc;

import ir.BasicBlock;
import ir.SpigletFunction;
import ir.definitions.Variable;
import ir.statements.FunctionCall;

import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import reg_alloc.LivenessData.LiveInData;
import reg_alloc.Register.RegisterType;

/**
 * A linear scan register allocator. The algorithm follows the original linear
 * scan register allocation algorithm. The only difference compared to our
 * algorithm is that we attempt to assign as many alive-between-function-calls
 * variables to sX registers as possible and as many dead-between-function-calls
 * variables to tX registers as possible.
 */
public final class LinearScanAllocator {
	public static class LivenessInterval implements Comparable <LivenessInterval> {
		int liveStart;
		int liveEnd;
		Variable variable;
		
		public int getLiveStart() {
			return liveStart;
		}
		
		public int getLiveEnd() {
			return liveEnd;
		}
		
		public LivenessInterval(Variable var, int liveStart, int liveEnd) {
			this.variable = var;
			this.liveStart = liveStart;
			this.liveEnd = liveEnd;
		}
		
		public Variable getVariable(){
			return variable;
		}
		
		@Override
		public int compareTo(LivenessInterval other){
			if (other == null){
				throw new NullPointerException();
			}

			int liveStartComparison = Integer.compare(liveStart, other.liveStart);
			if (liveStartComparison != 0){
				return liveStartComparison;
			}
			
			int liveEndComparison = Integer.compare(liveEnd, other.liveEnd);
			if (liveEndComparison != 0){
				return liveEndComparison;
			}
			
			return variable.compareTo(other.variable);
		}
		
		@Override
		public boolean equals(Object o){
			if (!(o instanceof LivenessInterval)){
				return false;
			}
			
			LivenessInterval other = (LivenessInterval) o;
			return liveStart == other.liveStart
					&& liveEnd == other.liveEnd
					&& variable.equals(other.variable); 
		}
		
		@Override
		public int hashCode() {
			int prime = 31;
			int result = 17;
			
			result = prime * result + liveEnd;
			result = prime * result + liveStart;
			result = prime * result + variable.hashCode();
			
			return result;
		}
		
		@Override
		public String toString(){
			return String.format("%s => [%d, %d]", variable, liveStart, liveEnd);
		}
		
		private static LivenessInterval extendEnding(LivenessInterval interval, int upTo){
			if (interval == null){
				throw new IllegalArgumentException();
			}
			
			if (upTo <= interval.liveEnd){
				return interval;
			}
			
			return new LivenessInterval(interval.variable, interval.liveStart, upTo);
		}
		
		public static final Comparator<LivenessInterval> END_TO_START_COMPARATOR =
				new Comparator<LivenessInterval>(){
			@Override
			public int compare(LivenessInterval lhs, LivenessInterval rhs){
				int liveEndComparison = Integer.compare(lhs.liveEnd, rhs.liveEnd);
				if (liveEndComparison != 0){
					return liveEndComparison;
				}
				
				int liveStartComparison = Integer.compare(lhs.liveStart, rhs.liveStart);
				if (liveStartComparison != 0){
					return liveStartComparison;
				}
				
				return lhs.variable.compareTo(rhs.variable);
			}
		};
	}
	
	private SpigletFunction func;
	private BlockLivenessAnalyzer analyzer;
	
	// This is the data structure we use to traverse the liveness intervals in
	// increasing interval ending order and choose which intervals to assign to
	// registers and which to spill to the stack.
	private SortedSet<LivenessInterval> linearScanIntervals;
	
	private Set<Variable> calleeSaveables;
	private Map<Variable, Register> registerMapping;
	
	private Map<Variable, LivenessInterval> unsortedIntervals;
	
	public Map<Variable, Register> getRegisterMapping() {
		return registerMapping;
	}
	
	public Map<Variable,LivenessInterval> getIntervals(){
		return Collections.unmodifiableMap(unsortedIntervals);
	}

	public Set<Variable> getAlwaysSpilled() {
		return alwaysSpilled;
	}

	public Set<Variable> getSpilledOnFunctionCall() {
		return spilledOnFunctionCall;
	}
	
	public BlockLivenessAnalyzer getAnalyzer(){
		return analyzer;
	}

	private Set<Variable> alwaysSpilled;
	private Set<Variable> spilledOnFunctionCall;

	public LinearScanAllocator(SpigletFunction func){
		this.func = func;
		analyzer = new BlockLivenessAnalyzer(func);
		linearScanIntervals = constructVariableIntervals(analyzer);
		calleeSaveables = aliveAfterCall();
		registerMapping = performRegisterAllocation();
		
		alwaysSpilled = findAlwaysSpilled();
		spilledOnFunctionCall = findSpilledOnlyOnFunctionCall();
	}
	
	private SortedSet<LivenessInterval> constructVariableIntervals(
			BlockLivenessAnalyzer analyzer){
		
		Map<Variable, LivenessInterval> variableInterval = 
				new HashMap<Variable, LivenessInterval>();
		unsortedIntervals = variableInterval;
		
		int idx = 0;
		for (BasicBlock block : func.getBlocks()){
			for (LiveInData data : analyzer.getLiveness(block).getLiveInPerStatement()){
				updateLivenessInterval(variableInterval, idx, data);
				idx++;
			}
		}
		
		return Collections.unmodifiableSortedSet(
				new TreeSet<LivenessInterval>(variableInterval.values()));
	}

	private static void updateLivenessInterval(
			Map<Variable, LivenessInterval> variableInterval, int idx,
			LiveInData data) {
		for (Variable var : data.getLiveInStmt()){
			LivenessInterval interval = variableInterval.get(var);
			if (interval == null){
				variableInterval.put(var, new LivenessInterval(var, idx, idx));
			} else {
				variableInterval.put(var, LivenessInterval.extendEnding(interval, idx));
			}
		}
	}
	
	/**
	 * Returns the set of variables that are alive after ANY function call
	 * and, thus, are candidates for sX registers.
	 */
	private Set<Variable> aliveAfterCall(){
		Set<Variable> candidates = new HashSet<Variable>();
		for (BasicBlock block : func.getBlocks()){
			LivenessData data = analyzer.getLiveness(block);
			List<LiveInData> statements = data.getLiveInPerStatement();
			
			ListIterator<LiveInData> iter =	statements.listIterator();
			while(iter.hasNext()){
				LiveInData liveIndata = iter.next();
				if (!(liveIndata.getStmt() instanceof FunctionCall)){
					continue;
				}
				
				FunctionCall call = (FunctionCall) liveIndata.getStmt();
				Set<Variable> liveOut = new HashSet<Variable>(
						data.getLiveInAtStatement(iter.nextIndex()));
				liveOut.remove(call.getAssignedVariable());
				
				candidates.addAll(liveOut);
			}
		}
		
		return candidates;
	}
	
	private Map<Variable, Register> performRegisterAllocation(){
		EnumSet<Register> availableRegisters =
				EnumSet.copyOf(Register.calleeSavedRegisters());
		availableRegisters.addAll(Register.callerSavedRegisters());
		
		Map<Variable, Register> registerAllocation =
				new HashMap<Variable, Register>();
		
		SortedSet<LivenessInterval> pickedIntervals =
				new TreeSet<LivenessInterval>(LivenessInterval.END_TO_START_COMPARATOR);
		for (LivenessInterval interval : linearScanIntervals){
			
			Variable var = interval.variable;

			pickedIntervals.add(interval);
			removeAllExpired(interval,
					registerAllocation,
					availableRegisters,
					pickedIntervals);
			
			if (availableRegisters.isEmpty()){
				LivenessInterval spilledInterval = pickedIntervals.last();
				pickedIntervals.remove(spilledInterval);
				Register usedRegister = registerAllocation.put(
						spilledInterval.variable, null);
				if (usedRegister != null){
					availableRegisters.add(usedRegister);
				}
			}
			
			Register allocatedRegister = pickRegister(
					var,
					calleeSaveables,
					availableRegisters);
			availableRegisters.remove(allocatedRegister);
			registerAllocation.put(var, allocatedRegister);
		}

		return registerAllocation;
	}
	
	/**
	 * Remove all intervals that will expire before our interval is added to the
	 * sorted set.
	 * 
	 * @param interval
	 * @param registerAllocation
	 * @param availableRegisters
	 * @param pickedIntervals
	 */
	private static void removeAllExpired(
			LivenessInterval interval,
			Map<Variable, Register> registerAllocation,
			EnumSet<Register> availableRegisters,
			SortedSet<LivenessInterval> pickedIntervals){
		for (Iterator<LivenessInterval> iter = pickedIntervals.iterator(); iter.hasNext(); ){
			LivenessInterval elem = iter.next();
			if (elem.liveEnd >= interval.liveStart){
				return;
			}
			
			iter.remove();
			availableRegisters.add(registerAllocation.get(elem.variable));
		}
	}
	
	private static Register pickRegister(Variable var,
			Set<Variable> aliveAfterCall,
			EnumSet<Register> availableRegisters){
		if (availableRegisters.isEmpty()){
			return null;
		}
		
		// If the variable is alive during a subroutine call, attempt to assign
		// it to an sX register, so that we avoid the overhead of saving and
		// restoring the variable to and from the stack, respectively, before
		// and after a function call.
		//
		// Otherwise, pick a tX register.
		if (aliveAfterCall.contains(var)){
			EnumSet<Register> availableCalleeSaved =
					EnumSet.copyOf(availableRegisters);
			availableCalleeSaved.retainAll(Register.calleeSavedRegisters());
			if (!availableCalleeSaved.isEmpty()){
				return availableCalleeSaved.iterator().next();
			}
		} else {
			// If the variable does not have to be preserved during a subroutine
			// call, try to pick a tX register so that we don't burden our
			// function with extraneous stack operations.
			//
			// Otherwise, pick a sX register.
			EnumSet<Register> availableTemp =
					EnumSet.copyOf(availableRegisters);
			availableTemp.retainAll(Register.callerSavedRegisters());
			if (!availableTemp.isEmpty()){
				return availableTemp.iterator().next();
			}
		}
		
		return availableRegisters.iterator().next();
	}
	
	/**
	 * Returns the set of variables that will be always spilled to the stack
	 * while the function is being executed.
	 */
	private Set<Variable> findAlwaysSpilled(){
		Set<Variable> alwaysSpilled = new HashSet<Variable>();
		for (Map.Entry<Variable, Register> entry : registerMapping.entrySet()){
			if (entry.getValue() == null){
				alwaysSpilled.add(entry.getKey());
			}
		}
		
		return alwaysSpilled;
	}

	/**
	 * Returns the set of variables that must be restored from the stack
	 * (if necessary) after a function is called by our function. Because
	 * sX registers are callee-saved, they are, thankfully, never spilled.
	 */
	private Set<Variable> findSpilledOnlyOnFunctionCall(){
		Set<Variable> funcCallSpilled = new HashSet<Variable>();
		for (Variable var : calleeSaveables){
			Register reg = registerMapping.get(var);
			if (reg != null && reg.getType() != RegisterType.CALLEE_SAVE){
				funcCallSpilled.add(var);
			}
		}
		
		return funcCallSpilled;
	}
}
