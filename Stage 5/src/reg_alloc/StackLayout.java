package reg_alloc;

import ir.SpigletFunction;
import ir.definitions.Variable;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import reg_alloc.Register.RegisterType;

/**
 * Represents the stack layout of a Spiglet function. The stack layout is as
 * follows:
 * 
 * <li> If there are more than 4 arguments, the first4 are placed in the a0-a3
 * registers and the rest are placed on the stack in a left-to-right order.
 * <li> All callee-saved registers that are used (s0-s7) are stored after the
 * arguments (if any).
 * <li> Local variables, as well as a0-a3 registers (if required) that may have
 * to be spilled.
 * <li> All but the first 4 arguments of each called subroutine in left to
 * right order.
 */
public class StackLayout {
	private EnumSet<Register> calleeSavedRegisters;
	private int calleeSavedOffset;

	private Map<Variable, Integer> localsIndices;
	private int stackFrameSize;

	public Map<Variable, Integer> getLocalsIndices() {
		return localsIndices;
	}

	public int getCalleeSavedOffset() {
		return calleeSavedOffset;
	}

	public int getStackFrameSize() {
		return stackFrameSize;
	}

	public EnumSet<Register> getCalleeSavedRegisters() {
		return calleeSavedRegisters;
	}
	
	private StackLayout(
			EnumSet<Register> calleeSavedRegisters,
			int calleeSavedOffset,
			Map<Variable, Integer> localsIndices,
			int stackFrameSize) {
		this.calleeSavedRegisters = calleeSavedRegisters;
		this.calleeSavedOffset = calleeSavedOffset;
		this.localsIndices = localsIndices;
		this.stackFrameSize = stackFrameSize;
	}

	public static StackLayout generateLayout(SpigletFunction func,
			LinearScanAllocator allocator){
		Map<Variable, Integer> locals = new HashMap<Variable, Integer>();
		int idx = 0;
		final int REGISTER_PARAMS = Register.parameterPassingRegisters().size();
		for (int i = REGISTER_PARAMS; i < func.getNumParameters(); i++){
			locals.put(new Variable(i), idx++);
		}
		
		int calleeSavedOffset = idx;
		EnumSet<Register> usedCalleeSave = EnumSet.noneOf(Register.class);
		for (Register reg : allocator.getRegisterMapping().values()){
			if (reg != null && reg.getType() == RegisterType.CALLEE_SAVE){
				usedCalleeSave.add(reg);
			}
		}
		idx += usedCalleeSave.size();
		
		Set<Variable> onStack = new HashSet<Variable>(allocator.getAlwaysSpilled());
		onStack.addAll(allocator.getSpilledOnFunctionCall());
		
		for (Variable var : onStack){
			if (locals.containsKey(var)){
				continue;
			}
			locals.put(var, idx++);
		}
		
		int frameSize = idx;
		
		return new StackLayout(usedCalleeSave,
				calleeSavedOffset,
				locals,
				frameSize);
	}
	
}
