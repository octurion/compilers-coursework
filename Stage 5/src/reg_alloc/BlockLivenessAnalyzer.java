package reg_alloc;

import ir.BasicBlock;
import ir.SpigletFunction;
import ir.definitions.Variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Runs the liveness analysis for a function and stores all live-in and
 * live-out information required for the register allocation algorithm.
 */
public final class BlockLivenessAnalyzer {
	private Map<BasicBlock, LivenessData> blockLiveness;
	
	public Map<BasicBlock, LivenessData> getBlocksLiveness() {
		return Collections.unmodifiableMap(blockLiveness);
	}

	public BlockLivenessAnalyzer(SpigletFunction func){
		generateDummyLivenessData(func);
		runLivenessAlgorithm(func);
	}
	
	public LivenessData getLiveness(BasicBlock block){
		return blockLiveness.get(block);
	}
	
	private void generateDummyLivenessData(SpigletFunction func){
		blockLiveness = new HashMap<BasicBlock, LivenessData>(func.getBlocks().size());
		for (BasicBlock block : func.getBlocks()){
			blockLiveness.put(block, new LivenessData(block));
		}
	}
	
	private boolean performLivenessAlgorithmLoop(SpigletFunction func){
		boolean changed = false;
		for (BasicBlock block : func.getBlocks()){
			LivenessData data = blockLiveness.get(block);
			
			boolean liveInChanged = data.propagateLivenessUpward();
			Set<BasicBlock> successors = block.getSuccessors();
			List<Set<Variable>> successorsLiveIn =
					new ArrayList<Set<Variable>>(successors.size());
			for (BasicBlock successor : successors){
				successorsLiveIn.add(blockLiveness.get(successor).getLiveInBlock());
			}
			
			boolean liveOutChanged = data.successorsUnion(successorsLiveIn);
			
			if (liveInChanged || liveOutChanged){
				changed = true;
			}
		}
		
		return changed;
	}
	
	private void runLivenessAlgorithm(SpigletFunction func){
		boolean changed;
		do {
			changed = performLivenessAlgorithmLoop(func);
		} while (changed);
	}
}
