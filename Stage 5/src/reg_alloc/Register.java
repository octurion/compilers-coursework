package reg_alloc;

import java.util.EnumSet;

import kanga.RegisterOrImmediate;

/**
 * An enum type that represents each register supported by Kanga.
 */
public enum Register implements RegisterOrImmediate {
	S0(RegisterType.CALLEE_SAVE, "s0"),
	S1(RegisterType.CALLEE_SAVE, "s1"),
	S2(RegisterType.CALLEE_SAVE, "s2"),
	S3(RegisterType.CALLEE_SAVE, "s3"),
	S4(RegisterType.CALLEE_SAVE, "s4"),
	S5(RegisterType.CALLEE_SAVE, "s5"),
	S6(RegisterType.CALLEE_SAVE, "s6"),
	S7(RegisterType.CALLEE_SAVE, "s7"),

	T0(RegisterType.CALLER_SAVE, "t0"),
	T1(RegisterType.CALLER_SAVE, "t1"),
	T2(RegisterType.CALLER_SAVE, "t2"),
	T3(RegisterType.CALLER_SAVE, "t3"),
	T4(RegisterType.CALLER_SAVE, "t4"),
	T5(RegisterType.CALLER_SAVE, "t5"),
	T6(RegisterType.CALLER_SAVE, "t6"),
	T7(RegisterType.CALLER_SAVE, "t7"),
	
	V0(RegisterType.LIVE_ONCE, "v0"),
	V1(RegisterType.LIVE_ONCE, "v1"),
	
	A0(RegisterType.PARAMETER_PASSING, "a0"),
	A1(RegisterType.PARAMETER_PASSING, "a1"),
	A2(RegisterType.PARAMETER_PASSING, "a2"),
	A3(RegisterType.PARAMETER_PASSING, "a3");

	public enum RegisterType {
		CALLEE_SAVE, CALLER_SAVE, LIVE_ONCE, PARAMETER_PASSING
	}
	
	private RegisterType type;
	private String registerName;
	
	public RegisterType getType() {
		return type;
	}

	public String getRegisterName() {
		return registerName;
	}
	
	@Override
	public String toString(){
		return registerName;
	}

	private Register(RegisterType type, String registerName){
		this.type = type;
		this.registerName = registerName;
	}
	
	public static EnumSet<Register> calleeSavedRegisters(){
		return EnumSet.range(S0, S7);
	}
	
	public static EnumSet<Register> callerSavedRegisters(){
		return EnumSet.range(T0, T7);
	}
	
	public static EnumSet<Register> liveOnceRegisters(){
		return EnumSet.of(V0);
	}
	
	public static EnumSet<Register> parameterPassingRegisters(){
		return EnumSet.range(A0, A3);
	}
}
