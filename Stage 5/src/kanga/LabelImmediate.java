package kanga;

import ir.definitions.LabelConstant;

public final class LabelImmediate implements RegisterOrImmediate {
	private String labelName;

	public String getLabelName() {
		return labelName;
	}
	
	public LabelImmediate(String labelName){
		if (labelName == null || labelName.isEmpty()){
			throw new IllegalArgumentException();
		}
		
		this.labelName = labelName;
	}

	@Override
	public int hashCode() {
		return labelName.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof LabelImmediate)) {
			return false;
		}
		
		LabelImmediate other = (LabelImmediate) obj;
		return labelName.equals(other.labelName);
	}
	
	@Override
	public String toString(){
		return labelName;
	}
	
	public static LabelImmediate fromLabelConstant(LabelConstant constant){
		if (constant == null){
			throw new IllegalArgumentException();
		}
		
		return new LabelImmediate(constant.getLabelName());
	}
}
