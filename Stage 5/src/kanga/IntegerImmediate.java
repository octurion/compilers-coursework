package kanga;

import ir.definitions.IntegerConstant;

public final class IntegerImmediate implements RegisterOrImmediate {
	int value;
	
	public IntegerImmediate(int value){
		if (value < 0){
			throw new IllegalArgumentException();
		}
		
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	@Override
	public boolean equals(Object o){
		if (!(o instanceof IntegerImmediate)){
			return false;
		}
		
		IntegerImmediate other = (IntegerImmediate) o;
		return this.value == other.value;
	}
	
	@Override
	public int hashCode(){
		return value;
	}
	
	public static boolean isIntegerImmediate(RegisterOrImmediate expr){
		return (expr instanceof IntegerImmediate);
	}
	
	@Override
	public String toString(){
		return Integer.toString(value);
	}
	
	public static IntegerImmediate fromIntegerConstant(IntegerConstant constant){
		if (constant == null){
			throw new IllegalArgumentException();
		}
		
		return new IntegerImmediate(constant.getValue());
	}
}
