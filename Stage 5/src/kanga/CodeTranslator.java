package kanga;

import ir.BasicBlock;
import ir.BasicBlock.ConditionalJump;
import ir.SpigletFunction;
import ir.StatementVisitor;
import ir.definitions.BinaryOperator;
import ir.definitions.BlockStatement;
import ir.definitions.IntegerConstant;
import ir.definitions.LabelConstant;
import ir.definitions.SimpleExpression;
import ir.definitions.Variable;
import ir.statements.BinaryOperation;
import ir.statements.ErrorStatement;
import ir.statements.FunctionCall;
import ir.statements.MemoryAllocate;
import ir.statements.MemoryLoad;
import ir.statements.MemoryStore;
import ir.statements.PrintStatement;
import ir.statements.ReturnStatement;
import ir.statements.UnaryAssignment;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import reg_alloc.LinearScanAllocator;
import reg_alloc.LivenessData;
import reg_alloc.Register;
import reg_alloc.Register.RegisterType;
import reg_alloc.StackLayout;

/**
 * The class which deals with translating the entire Spiglet function into
 * Kanga code
 */
final class CodeTranslator {
	private StringBuilder builder;
	private Map<BasicBlock, String> blockName;
	private SpigletFunction func;
	private LinearScanAllocator allocator;
	private StackLayout layout;
	
	private int currentStatementIndex;
	private BasicBlock currentBlock;
	
	private class RegisterOrStackOffset {
		boolean onRegister;
		Register reg;
		int offset;
		
		public RegisterOrStackOffset(Register reg) {
			onRegister = true;
			this.reg = reg;
		}
		
		public RegisterOrStackOffset(int offset) {
			onRegister = false;
			this.offset = offset;
		}
	}
	
	/**
	 * Converts a Spiglet function into its equivalent Kanga source code.
	 */
	public static String toKanga(SpigletFunction func,
			LinearScanAllocator allocator,
			StackLayout layout){
		CodeTranslator translator = new CodeTranslator(func, allocator, layout);
		return translator.emitCode();
	}
	
	private CodeTranslator(SpigletFunction func,
			LinearScanAllocator allocator,
			StackLayout layout){
		this.func = func;
		this.allocator = allocator;
		this.layout = layout;
		
		builder = new StringBuilder();
		blockName = makeBlockNames(func);
	}
	
	private String emitCode(){
		generatePrologue();
		
		for (BasicBlock block : func.getBlocks()){
			emitBlockLabel(block);
			currentBlock = block;

			currentStatementIndex = 0;
			for (BlockStatement stmt : block.getStatements()){
				stmt.accept(new CodeEmissionVisitor());
				currentStatementIndex++;
			}
			
			emitJump(block.getNonconditionalSuccessor());
		}
		
		generateEpilogue();
		
		return builder.toString();
	}
	
	private boolean isDeadStore(Variable var){
		LivenessData data = allocator.getAnalyzer().getBlocksLiveness().get(currentBlock);
		Set<Variable> liveOutVars = data.getLiveOutAtStatement(currentStatementIndex);
		
		return !liveOutVars.contains(var);
	}
	
	private void generatePrologue(){
		int idx = layout.getCalleeSavedOffset();
		for (Register reg : layout.getCalleeSavedRegisters()){
			emitAstore(idx++, reg);
		}
		
		int i;
		EnumSet<Register> paramRegisters = Register.parameterPassingRegisters();
		Iterator<Register> iter = paramRegisters.iterator();
		for (i = 0; i < func.getNumParameters() && iter.hasNext(); i++){
			RegisterOrStackOffset loc = getLocation(new Variable(i));
			if (loc == null){
				iter.next();
				continue;
			}
			moveToLocation(loc, iter.next());
		}
		
		for (; i < func.getNumParameters(); i++){
			Variable var = new Variable(i);
			Register dest = allocator.getRegisterMapping().get(var);
			if (dest == null){
				continue;
			}
			
			emitAload(dest, layout.getLocalsIndices().get(var));
		}
	}
	
	private void generateEpilogue(){
		int idx = layout.getCalleeSavedOffset();
		for (Register reg : layout.getCalleeSavedRegisters()){
			emitAload(reg, idx++);
		}
	}

	
	private static void appendFmtLn(StringBuilder builder, String fmt, Object... args){
		builder.append(String.format(fmt, args)).append('\n');
	}
	
	private static Map<BasicBlock, String> makeBlockNames(SpigletFunction func){
		int labelID = 0;
		Map<BasicBlock, String> blockNames = new HashMap<BasicBlock, String>();
		for (BasicBlock block : func.getBlocks()){
			blockNames.put(block,
					String.format("%s_L%d", func.getName(), labelID++));
		}
		
		return blockNames;
	}
	
	private void emitBlockLabel(BasicBlock block){
		appendFmtLn(builder, "%s NOOP", blockName.get(block));
	}
	
	private void emitError(){
		appendFmtLn(builder, "ERROR");
	}	
	
	private void emitAload(Register dest, int spilledArg){
		appendFmtLn(builder, "ALOAD %s SPILLEDARG %d", dest, spilledArg);
	}
	
	private void emitAstore(int spilledArg, Register src){
		appendFmtLn(builder, "ASTORE SPILLEDARG %d %s", spilledArg, src);
	}
	
	private void emitPassArg(int passedArgument, Register src){
		appendFmtLn(builder, "PASSARG %d %s", passedArgument, src);
	}
	
	private void emitOperation(
			Register dest,
			BinaryOperator oper,
			Register lhs,
			RegisterOrImmediate rhs){
		appendFmtLn(builder, "MOVE %s %s %s %s", dest, oper, lhs, rhs);
	}
	
	private void emitHload(Register dest, Register src, int offset){
		appendFmtLn(builder, "HLOAD %s %s %d", dest, src, offset);
	}
	
	private void emitHstore(Register ptr, int offset, Register data){
		appendFmtLn(builder, "HSTORE %s %d %s", ptr, offset, data);
	}
	
	private void emitPrint(RegisterOrImmediate expr){
		appendFmtLn(builder, "PRINT %s", expr);
	}
	
	private void emitCJump(Register reg, BasicBlock successor){
		appendFmtLn(builder, "CJUMP %s %s", reg, blockName.get(successor));
	}
	
	private void emitJump(BasicBlock successor){
		if (successor == null){
			return;
		}
		appendFmtLn(builder, "JUMP %s", blockName.get(successor));
	}
	
	private void emitMove(Register dest, RegisterOrImmediate src){
		if (dest.equals(src)){
			return;
		}
		
		appendFmtLn(builder, "MOVE %s %s", dest, src);
	}
	
	private void emitHallocate(Register dest, RegisterOrImmediate size){
		appendFmtLn(builder, "MOVE %s HALLOCATE %s", dest, size);
	}
	
	private void emitCall(RegisterOrImmediate func){
		appendFmtLn(builder, "CALL %s", func);
	}
	
	private Register loadToRegister(Variable var){
		Register register = allocator.getRegisterMapping().get(var);
		if (register != null){
			return register;
		}
		
		int stackOffset = layout.getLocalsIndices().get(var);
		Register src = Register.V1;
		emitAload(src, stackOffset);
		return src;
	}

	private Register loadSecondToRegister(Variable var, Register otherOperand){
		Register register = allocator.getRegisterMapping().get(var);
		if (register != null){
			return register;
		}
		
		int stackOffset = layout.getLocalsIndices().get(var);
		Register src = otherOperand == Register.V0 
				? Register.V1
				: Register.V0;
		emitAload(src, stackOffset);
		return src;
	}
	
	private RegisterOrImmediate loadRegImmToRegister(SimpleExpression expr){
		if (expr instanceof IntegerConstant){
			return IntegerImmediate.fromIntegerConstant((IntegerConstant) expr);
		}
		
		if (expr instanceof LabelConstant){
			return LabelImmediate.fromLabelConstant((LabelConstant) expr);
		}
		
		return loadToRegister((Variable) expr);
	}
	
	private RegisterOrImmediate convertToImmediate(SimpleExpression expr){
		if (expr instanceof IntegerConstant){
			return IntegerImmediate.fromIntegerConstant((IntegerConstant) expr);
		}
		
		if (expr instanceof LabelConstant){
			return LabelImmediate.fromLabelConstant((LabelConstant) expr);
		}

		throw new AssertionError();
	}
	
	private RegisterOrImmediate loadSecondToRegister(SimpleExpression expr,
			Register otherOperand){
		if (expr instanceof Variable){
			return loadSecondToRegister((Variable) expr, otherOperand);
		}
		
		return convertToImmediate(expr);
	}
	
	private void moveToLocation(RegisterOrStackOffset loc, Register src){
		if (loc == null){
			emitMove(Register.V0, src);
		} else if (loc.onRegister){
			emitMove(loc.reg, src);
		} else {
			emitAstore(loc.offset, src);
		}
	}
	
	private RegisterOrStackOffset getLocation(Variable var){
		if (!allocator.getRegisterMapping().containsKey(var)){
			return null;
		}
		
		Register reg = allocator.getRegisterMapping().get(var);
		if (reg != null){
			return new RegisterOrStackOffset(reg);
		}
		
		return new RegisterOrStackOffset(layout.getLocalsIndices().get(var));
	}
	
	private class CodeEmissionVisitor implements StatementVisitor{
		public CodeEmissionVisitor(){}

		@Override
		public void visit(ConditionalJump jump) {
			Register cond = loadToRegister(jump.getConditionVariable());
			emitCJump(cond, jump.getConditionalSuccessor());
		}

		@Override
		public void visit(ErrorStatement stmt) {
			emitError();
		}

		@Override
		public void visit(MemoryStore store) {
			Register data = loadToRegister(store.getStoredData());
			Register ptr = loadSecondToRegister(store.getPointer(), data);
			
			emitHstore(ptr, store.getOffset(), data);
		}

		@Override
		public void visit(MemoryLoad load) {
			if (isDeadStore(load.getAssignedVariable())){
				return;
			}
			
			Register ptr = loadToRegister(load.getPointer());
			RegisterOrStackOffset location = getLocation(load.getAssignedVariable());
			
			if (location == null){
				return;
			}

			if (!location.onRegister){
				emitHload(Register.V1, ptr, load.getOffset());
				moveToLocation(location, Register.V1);
			} else {
				emitHload(location.reg, ptr, load.getOffset());
			}
		}

		@Override
		public void visit(PrintStatement stmt) {
			RegisterOrImmediate expr = loadRegImmToRegister(stmt.getExpressionPrinted());
			emitPrint(expr);
		}

		@Override
		public void visit(ReturnStatement stmt) {
			emitMove(Register.V0, loadRegImmToRegister(stmt.getReturnExpression()));
		}

		@Override
		public void visit(BinaryOperation oper) {
			if (isDeadStore(oper.getAssignedVariable())){
				return;
			}
			Register lhs = loadToRegister(oper.getLHS());
			RegisterOrImmediate rhs = loadSecondToRegister(oper.getRHS(), lhs);
			
			RegisterOrStackOffset location = getLocation(oper.getAssignedVariable());
			if (location == null){
				return;
			}

			if (!location.onRegister){
				emitOperation(Register.V1, oper.getOperator(), lhs, rhs);
				moveToLocation(location, Register.V1);
			} else {
				emitOperation(location.reg, oper.getOperator(), lhs, rhs);
			}
		}

		@Override
		public void visit(FunctionCall call) {
			Set<Variable> spilledVariables =
					new HashSet<Variable>(allocator.getSpilledOnFunctionCall());
			Iterator<Variable> iter = spilledVariables.iterator();
			while (iter.hasNext()){
				Variable var = iter.next();
				Register reg = allocator.getRegisterMapping().get(var);
				if (reg == null || reg.getType() != RegisterType.CALLER_SAVE){
					iter.remove();
				}
				
				int stackOffset = layout.getLocalsIndices().get(var);
				emitAstore(stackOffset, reg);
			}

			int idx = 1;
			EnumSet<Register> parameterRegs = Register.parameterPassingRegisters();
			Iterator<Register> regIter = parameterRegs.iterator();
			for (Variable argument : call.getParameters()){
				Register reg = loadToRegister(argument);
				if (regIter.hasNext()){
					Register dest = regIter.next();
					emitMove(dest, reg);
				} else {
					emitPassArg(idx++, reg);
				}
			}
			
			RegisterOrImmediate func = loadRegImmToRegister(call.getFunction());
			emitCall(func);
			
			for (Variable var : spilledVariables){
				Register reg = allocator.getRegisterMapping().get(var);
				int stackOffset = layout.getLocalsIndices().get(var);
				emitAload(reg, stackOffset);
			}
			
			RegisterOrStackOffset resultLoc = getLocation(call.getAssignedVariable());
			moveToLocation(resultLoc, Register.V0);
		}

		@Override
		public void visit(MemoryAllocate alloc) {
			if (isDeadStore(alloc.getAssignedVariable())){
				return;
			}
			
			RegisterOrImmediate arg = loadRegImmToRegister(
					alloc.getSizeExpression());
			RegisterOrStackOffset location = getLocation(alloc.getAssignedVariable());
			
			if (location == null){
				return;
			}

			if (!(arg instanceof Register) && !location.onRegister){
				emitHallocate(Register.V1, arg);
				moveToLocation(location, Register.V1);
			} else {
				emitHallocate(location.reg, arg);
			}
		}

		@Override
		public void visit(UnaryAssignment assignment) {
			if (isDeadStore(assignment.getAssignedVariable())){
				return;
			}
			
			RegisterOrImmediate arg = loadRegImmToRegister(
					assignment.getAssignmentExpression());
			RegisterOrStackOffset location = getLocation(assignment.getAssignedVariable());
			
			if (location == null){
				return;
			}

			if (!(arg instanceof Register) && !location.onRegister){
				emitMove(Register.V1, arg);
				moveToLocation(location, Register.V1);
			} else {
				emitMove(location.reg, arg);
			}
		}
	};
}
