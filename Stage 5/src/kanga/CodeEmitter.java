package kanga;

import ir.BasicBlocksConverter;
import ir.SpigletFunction;

import java.util.ArrayList;
import java.util.List;

import reg_alloc.LinearScanAllocator;
import reg_alloc.StackLayout;
import syntaxtree.Goal;
import syntaxtree.Procedure;
import syntaxtree.StmtList;
import visitor.GJVoidDepthFirst;
import visitor.GJVoidVisitor;

public final class CodeEmitter {
	/**
	 * Converts a Spiglet into a Kanga source string
	 * 
	 * @param goal The parse tree of the Spiglet file
	 * @return The source string of the Spiglet function translated into Kanga.
	 */
	public static String spigletToKangaCode(Goal goal){
		List<SpigletFunction> functions = new ArrayList<SpigletFunction>();
		FUNCTION_COLLECTOR.visit(goal, functions);

		StringBuilder builder = new StringBuilder();
		for (SpigletFunction function : functions){
			builder.append(convertFunctionToKanga(function)).append('\n');
		}
		
		return builder.toString();
	}
	
	private static String convertFunctionToKanga(SpigletFunction function){
		LinearScanAllocator allocator = new LinearScanAllocator(function);
		StackLayout layout = StackLayout.generateLayout(function, allocator);

		return String.format("%s[%d][%d][%d]%n%sEND%n",
				function.getName(),
				function.getNumParameters(),
				layout.getStackFrameSize(),
				function.maxNumParametersPassedToCallees(),
				CodeTranslator.toKanga(function, allocator, layout));
	}
	
	private static final GJVoidVisitor<List<SpigletFunction>> FUNCTION_COLLECTOR =
			new GJVoidDepthFirst<List<SpigletFunction>>(){
		@Override
		public void visit(StmtList mainFunc, List<SpigletFunction> functions){
			functions.add(BasicBlocksConverter.spigletMainToBlockGraph(mainFunc));
		}
		
		@Override
		public void visit(Procedure proc, List<SpigletFunction> functions){
			functions.add(BasicBlocksConverter.spigletFunctionToBlockGraph(proc));
		}
	};
}
