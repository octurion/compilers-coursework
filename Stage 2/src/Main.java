import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import program.ClassHierarchy;
import program.SemanticException;
import syntaxtree.Goal;

public class Main {
	public static void main(String[] args) {
		for (String filename : args) {
			System.out.printf("Now parsing file %s...%n", filename);
			parseFile(filename);
		}
	}
	
	public static void parseFile(String filename){
		InputStream in = null;
		try {
			in = new FileInputStream(filename);
			MiniJavaParser parser = new MiniJavaParser(in);
			Goal goal = parser.Goal();
			
			ClassHierarchy.generateDeclarations(goal);
			System.out.println("File parsed successfully!");
		}
		catch (IOException e) {
			System.err.printf("An I/O error occurred while parsing file \"%s\": %s%n",
				filename, e.getMessage());
		}
		catch (ParseException e) {
			System.err.println(e.getMessage());
		}
		catch (SemanticException e){
			System.err.println(e.getMessage());
		}
		finally {
			try {
				if (in != null) in.close();
			}
			catch(IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
}
