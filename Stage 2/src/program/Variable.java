package program;
import syntaxtree.FormalParameter;
import syntaxtree.FormalParameterTerm;
import syntaxtree.VarDeclaration;
import visitor.*;

public final class Variable {
	private String name;
	private VarType type;
	
	public String getName() {
		return name;
	}

	public VarType getType() {
		return type;
	}

	private Variable(String name, VarType type){
		this.name = name;
		this.type = type;
	}
	
	@Override
	public String toString(){
		return String.format("%s %s", type.toString(), name);
	}

	static Variable extractVariable(VarDeclaration d, ClassHierarchy ch){
		return new VariableExtractor().visit(d, ch);
	}
	
	static Variable extractVariable(FormalParameter p, ClassHierarchy ch){
		return new VariableExtractor().visit(p, ch);
	}

	static class VariableExtractor extends GJDepthFirst<Variable, ClassHierarchy>{
		public VariableExtractor(){}
		
		public Variable visit(VarDeclaration d, ClassHierarchy ch){
			return new Variable(d.f1.f0.toString(), VarType.extractType(d.f0, ch));
		}
		
		public Variable visit(FormalParameter p, ClassHierarchy ch){
			return new Variable(p.f1.f0.toString(), VarType.extractType(p.f0, ch));
		}
		
		public Variable visit(FormalParameterTerm p, ClassHierarchy ch){
			return visit(p.f1, ch);
		}
	}
}
