package program;

public class SemanticException extends RuntimeException {
	private static final long serialVersionUID = 3477112397188646815L;
	
	public SemanticException(){
		super();
	}
	
	public SemanticException(String message){
		super(message);
	}
}
