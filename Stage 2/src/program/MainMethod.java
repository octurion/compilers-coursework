package program;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import syntaxtree.MainClass;
import syntaxtree.NodeListOptional;

public final class MainMethod {
	private Map<String, Variable> locals;
	private NodeListOptional statements;
	
	Map<String, Variable> getLocals() {
		return locals;
	}

	NodeListOptional getStatements() {
		return statements;
	}

	MainMethod(ClassHierarchy ch, MainClass mainClass) {
		this.locals = checkNonDuplicate(SyntaxTreeUtils.acceptAndCollect(mainClass.f14,
				new Variable.VariableExtractor(), ch));
		String argsParameter = mainClass.f1.f0.toString();
		if (this.locals.containsKey(argsParameter)){
			throw new SemanticException(
					String.format("Parameter %s redefined as local",
							argsParameter));
		}
		
		this.statements = mainClass.f15;
	}
	
	private static Map<String, Variable> checkNonDuplicate(List<Variable> paramsList) {
		Map<String, Variable> paramsMap = new HashMap<String, Variable>();
		for (Variable v : paramsList){
			if (paramsMap.put(v.getName(), v) != null){
				throw new SemanticException(
						String.format("Local %s redefined twice",
								v.getName()));
			}
		}
		return paramsMap;
	}
}