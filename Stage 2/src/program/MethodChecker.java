package program;

import static program.VarType.BOOLEAN_TYPE;
import static program.VarType.INTEGER_ARRAY_TYPE;
import static program.VarType.INTEGER_TYPE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import syntaxtree.AllocationExpression;
import syntaxtree.AndExpression;
import syntaxtree.ArrayAllocationExpression;
import syntaxtree.ArrayAssignmentStatement;
import syntaxtree.ArrayLength;
import syntaxtree.ArrayLookup;
import syntaxtree.AssignmentStatement;
import syntaxtree.BracketExpression;
import syntaxtree.CompareExpression;
import syntaxtree.Expression;
import syntaxtree.ExpressionList;
import syntaxtree.ExpressionTerm;
import syntaxtree.FalseLiteral;
import syntaxtree.Identifier;
import syntaxtree.IfStatement;
import syntaxtree.IntegerLiteral;
import syntaxtree.MessageSend;
import syntaxtree.MinusExpression;
import syntaxtree.Node;
import syntaxtree.NodeListOptional;
import syntaxtree.NotExpression;
import syntaxtree.PlusExpression;
import syntaxtree.PrintStatement;
import syntaxtree.ThisExpression;
import syntaxtree.TimesExpression;
import syntaxtree.TrueLiteral;
import syntaxtree.WhileStatement;
import visitor.GJDepthFirst;

final class MethodChecker {
	private static class MethodContext {
		private MiniJavaClass underlyingClass;
		private ClassHierarchy hierarchy;
		
		private VarType returnType;
		private Map<String, Variable> paramsAndLocals;
		
		private NodeListOptional statements;
		private Expression returnExpression;
		
		public Map<String, Variable> getParamsAndLocals() {
			return Collections.unmodifiableMap(paramsAndLocals);
		}
		public MiniJavaClass getUnderlyingClass() {
			return underlyingClass;
		}
		public ClassHierarchy getHierarchy() {
			return hierarchy;
		}
		public VarType getReturnType() {
			return returnType;
		}
		public NodeListOptional getStatements() {
			return statements;
		}
		public Expression getReturnExpression() {
			return returnExpression;
		}
		public MethodContext(
				ClassHierarchy hierarchy,
				MiniJavaClass underlyingClass,
				VarType returnType,
				Map<String, Variable> parameters,
				Map<String, Variable> locals,
				NodeListOptional statements,
				Expression returnExpression) {
			this.hierarchy = hierarchy;
			this.underlyingClass = underlyingClass;
			this.returnType = returnType;

			this.statements = statements;
			this.returnExpression = returnExpression;
			
			this.paramsAndLocals = new HashMap<String, Variable>(locals);
			this.paramsAndLocals.putAll(parameters);
		}
	}
	
	private MethodChecker(){}
	
	public static void checkMethod(Method method, ClassHierarchy ch,
			MiniJavaClass clazz){
		checkAnyMethod(new MethodContext(
				ch,
				clazz,
				method.getReturnType(),
				method.getParameters(),
				method.getLocals(),
				method.getMethodNode().f8,
				method.getMethodNode().f10));
	}
	
	public static void checkMainMethod(ClassHierarchy ch){
		checkAnyMethod(new MethodContext(
				ch,
				null,
				null,
				Collections.<String, Variable>emptyMap(),
				ch.getMainMethod().getLocals(),
				ch.getMainMethod().getStatements(),
				null));
	}
	
	private static void checkAnyMethod(MethodContext ctx){
		new StatementVisitor().visit(ctx.getStatements(), ctx);
		
		if(ctx.getReturnExpression() == null){
			return;
		}
		VarType returnExpressionType =
				new StatementVisitor().visit(ctx.getReturnExpression(), ctx);
		if(!returnExpressionType.equals(ctx.getReturnType())){
			throw new SemanticException(String.format(
					"Expected return type of %s rather than %s",
					ctx.getReturnType(), returnExpressionType));
		}
	}
	
	private static class StatementVisitor extends GJDepthFirst<VarType, MethodContext> {
		private StatementVisitor(){}
		
		public VarType visit(AndExpression expr, MethodContext ctx){
			expect(expr.f0, BOOLEAN_TYPE, ctx);
			expect(expr.f2, BOOLEAN_TYPE, ctx);

			return BOOLEAN_TYPE;
		}
		
		public VarType visit(CompareExpression expr, MethodContext ctx){
			expect(expr.f0, INTEGER_TYPE, ctx);
			expect(expr.f2, INTEGER_TYPE, ctx);

			return BOOLEAN_TYPE;
		}
		
		public VarType visit(PlusExpression expr, MethodContext ctx){
			expect(expr.f0, INTEGER_TYPE, ctx);
			expect(expr.f2, INTEGER_TYPE, ctx);

			return INTEGER_TYPE;
		}
		
		public VarType visit(MinusExpression expr, MethodContext ctx){
			expect(expr.f0, INTEGER_TYPE, ctx);
			expect(expr.f2, INTEGER_TYPE, ctx);

			return INTEGER_TYPE;
		}
		
		public VarType visit(TimesExpression expr, MethodContext ctx){
			expect(expr.f0, INTEGER_TYPE, ctx);
			expect(expr.f2, INTEGER_TYPE, ctx);

			return INTEGER_TYPE;
		}
		
		public VarType visit(ArrayLookup expr, MethodContext ctx){
			expect(expr.f0, INTEGER_ARRAY_TYPE, ctx);
			expect(expr.f2, INTEGER_TYPE, ctx);

			return INTEGER_TYPE;
		}
		
		public VarType visit(ArrayLength expr, MethodContext ctx){
			expect(expr.f0, INTEGER_ARRAY_TYPE, ctx);

			return INTEGER_TYPE;
		}
		
		public VarType visit(MessageSend expr, MethodContext ctx){
			VarType msgReceiverType = visit(expr.f0, ctx);
			String methodName = expr.f2.f0.toString();
			if (msgReceiverType.getWhich() != VarType.VarKind.CLASS){
				throw new SemanticException(
						"Cannot call function for non-class type");
			}
			
			MiniJavaClass classType = msgReceiverType.getClassType();
			Method method = StatementVisitor.findMethod(methodName, classType);
			
			List<VarType> args = new ArgumentsExtractor().visit(expr.f4, ctx);
			if (args == null){
				args = Collections.<VarType>emptyList();
			}
			
			Collection<Variable> params = method.getParameters().values();
			if (args.size() != params.size()){
				throw new SemanticException("Incorrect number of arguments passed");
			}
			
			Iterator<Variable> paramsIter = params.iterator();
			Iterator<VarType> argsIter = args.iterator();
			
			while(paramsIter.hasNext() && argsIter.hasNext()){
				VarType expectedType = paramsIter.next().getType();
				VarType argType = argsIter.next();
				
				if (!argType.isConvertibleTo(expectedType)){
					throw new SemanticException(String.format(
							"Argument type %s cannot be converted into parameter type %s",
							argType,
							expectedType));
				}
			}
			
			return method.getReturnType();
		}
		
		public VarType visit(IntegerLiteral expr, MethodContext ctx){
			return INTEGER_TYPE;
		}
		
		public VarType visit(TrueLiteral expr, MethodContext ctx){
			return BOOLEAN_TYPE;
		}
		
		public VarType visit(FalseLiteral expr, MethodContext ctx){
			return BOOLEAN_TYPE;
		}
		
		public VarType visit(Identifier expr, MethodContext ctx){
			return StatementVisitor.findFieldOrLocalType(
					expr.f0.toString(), ctx);
		}
		
		public VarType visit(ThisExpression expr, MethodContext ctx){
			if (ctx.getUnderlyingClass() == null){
				throw new SemanticException(
						"Cannot use the keyword this in a static function");
			}
			return new VarType(ctx.getUnderlyingClass());
		}
		
		public VarType visit(ArrayAllocationExpression expr, MethodContext ctx){
			expect(expr.f3, INTEGER_TYPE, ctx);

			return INTEGER_ARRAY_TYPE;
		}
		
		public VarType visit(AllocationExpression expr, MethodContext ctx){
			String className = expr.f1.f0.toString();
			MiniJavaClass type = ctx.getHierarchy().getClassesByName()
					.get(className);
			if (type == null){
				throw new SemanticException(
						String.format("Class %s has not been defined",
								className));
			}

			return new VarType(type);
		}
		
		public VarType visit(NotExpression expr, MethodContext ctx){
			expect(expr.f1, BOOLEAN_TYPE, ctx);

			return BOOLEAN_TYPE;
		}
		
		public VarType visit(BracketExpression expr, MethodContext ctx){
			return visit(expr.f1, ctx);
		}
		
		public VarType visit(AssignmentStatement stmt, MethodContext ctx){
			VarType lvalueType = visit(stmt.f0, ctx);
			VarType rvalueType = visit(stmt.f2, ctx);
			
			if (!rvalueType.isConvertibleTo(lvalueType)){
				throw new SemanticException(String.format(
						"rvalue type %s cannot be converted to lvalue type %s",
						rvalueType,
						lvalueType));
			}
			
			return null;
		}
		
		public VarType visit(ArrayAssignmentStatement stmt, MethodContext ctx){
			expect(stmt.f0, INTEGER_ARRAY_TYPE, ctx);
			expect(stmt.f2, INTEGER_TYPE, ctx);
			expect(stmt.f5, INTEGER_TYPE, ctx);
			
			return null;
		}

		public VarType visit(IfStatement stmt, MethodContext ctx){
			expect(stmt.f2, BOOLEAN_TYPE, ctx);
			visit(stmt.f4, ctx);
			visit(stmt.f6, ctx);

			return null;
		}
		
		public VarType visit(WhileStatement stmt, MethodContext ctx){
			expect(stmt.f2, BOOLEAN_TYPE, ctx);
			visit(stmt.f4, ctx);

			return null;
		}
		
		public VarType visit(PrintStatement stmt, MethodContext ctx){
			VarType exprType = visit(stmt.f2, ctx);
			if (!exprType.equals(VarType.INTEGER_TYPE)
					&& !exprType.equals(VarType.BOOLEAN_TYPE)){
				throw new SemanticException(
						"Only ints and booleans can be printed");
			}

			return null;
		}
		
		public VarType visit(ExpressionTerm expr, MethodContext ctx){
			return visit(expr.f1, ctx);
		}
		
		private void expect(Node node, VarType type, MethodContext ctx){
			VarType nodeType = node.accept(this, ctx);
			if (!nodeType.equals(type)){
				throw new SemanticException(String.format(
						"Expected type %s but got %s",
						type, nodeType));
			}
		}
		
		private static VarType findFieldType(String fieldName, MiniJavaClass clazz){
			for (MiniJavaClass iter = clazz; iter != null; iter = iter.getSuperClass()){
				Variable classField = iter.getClassMembers().get(fieldName);
				if (classField != null){
					return classField.getType();
				}
			}
			return null;
		}
		
		private static VarType findFieldOrLocalType(
				String identifierName, MethodContext ctx){
			Variable local = ctx.getParamsAndLocals().get(identifierName);
			if (local != null){
				return local.getType();
			}
			
			VarType fieldType = StatementVisitor.findFieldType(
					identifierName, ctx.getUnderlyingClass());
			if (fieldType != null){
				return fieldType;				
			}
			
			throw new SemanticException(String.format(
					"No field or local variable named %s was found",
					identifierName));
		}

		private static Method findMethod(String methodName, MiniJavaClass clazz){
			for (MiniJavaClass iter = clazz; iter != null; iter = iter.getSuperClass()){
				Method method = iter.getClassMethods().get(methodName);
				if (method != null){
					return method;
				}
			}
			throw new SemanticException(String.format(
					"No method named %s was found in class %s",
					methodName, clazz));
		}
	}
	
	static class ArgumentsExtractor extends GJDepthFirst<List<VarType>, MethodContext>{
		public ArgumentsExtractor() {}
		
		public List<VarType> visit(ExpressionList list, MethodContext ctx){
			List<VarType> result = new ArrayList<VarType>();
			result.add(new StatementVisitor().visit(list.f0, ctx));
			
			result.addAll(SyntaxTreeUtils.<VarType, MethodContext>acceptAndCollect(
					list.f1.f0,
					new StatementVisitor(), ctx));
			return result;
		}
	}
}