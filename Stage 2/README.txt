Compilers - Project 2
---------------------

Alexandros Tasos (sdi1100085)

Compilation instructions:
1) Unzip & descend into the src directory
2) Run the following command:
   $ find -name "*.java" -print | xargs javac

Alternatively, you can create a new project in Eclipse and add the
source files.

Running instructions:
1) While inside the src directory, run:
   $ java Main /path/to/MiniJava/files/*.java

If you are using Eclipse, you will have to set the comman line arguments
by going to Run > Run Configurations... > Arguments.
If for some reason or another the MiniJava files cannot be found by Eclipse,
just cd into the MiniJava folder and run the following:
$ find `pwd` -name "*.java" | sort

Also, the Eclipse console does not interleave stdout and stderr the way a
terminal does, so prepare to see improperly printed or out of order
statements.
